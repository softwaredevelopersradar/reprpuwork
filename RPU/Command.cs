﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace RPU
{
    class Command
    {
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct EmptyCommand
        {
            public byte first; //0x03
            public byte second; // 0x0A
            public byte third; // 0x00

            public EmptyCommand(byte first, byte second, byte third)
            {
                this.first = first;
                this.second = second;
                this.third = third;
            }
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SaveData
        {
            public byte first; //0x03
            public byte xx; //number URP
            public byte third; //0x36
            public SaveData(byte xx)
            {
                this.first = 0x03;
                this.xx = xx;
                this.third = 0x36;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CurrentStateURP //запрос текущего состояния УРП
        {
            public byte lengthCommand; // 0x07
            public byte yy;
            public byte constValue;// = 0x00
            public byte xx; // 0x00, а в ответ номер полосы приема шириной 30МГц(0..99)
            public byte zz; //0x00, а в ответ значение аттенюатора в полосе приема
            public byte vv; //0x00, а в ответ значение усилителя в полосе приема
            public byte tt;//0х00, а в ответ байт состояния

            public CurrentStateURP(byte yy)
            {
                this.lengthCommand = 0x07;
                this.yy = yy;
                this.constValue = 0;
                this.xx = 0;
                this.zz = 0;
                this.vv = 0;
                this.tt = 0;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FreqRadioControl// Установка частоты приема РПУ в режиме радиоконтроля для канала
        {
            public byte lengthCommand; // 0x05
            public byte yy; // уу - 00 режим пеленгования для УРП-1..УРП-5(ответ только от УРП-1)/ уу = 01..06- номер УРП
            public byte constValue; // = 0x01
            public byte xx1;
            public byte xx2;//xx xx - hex- представление частоты в мегагерцах от 25 до 3025

            public FreqRadioControl( byte yy, byte xx1, byte xx2)
            {
                this.lengthCommand = 0x05;
                this.yy = yy;
                this.constValue = 0x01;
                this.xx1 = xx1;
                this.xx2 = xx2;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StepFreq //установка шага перестройки частоты приема УРП
        {
            public byte lengthCommand; // 0x05
            public byte yy;// уу - 00 режим пеленгования для УРП-1..УРП-5(ответ только от УРП-1)/ уу = 01..06- номер УРП
            public byte constValue; 

            public byte xx1;
            public byte xx2; //xx xx - hex- представление частоты в мегагерцах 

            public StepFreq( byte yy, byte xx1, byte xx2)
            {
                this.lengthCommand = 0x05;
                this.yy = yy;
                this.constValue = 0x10;
                this.xx1 = xx1;
                this.xx2 = xx2;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StepFreqUpDown //установка шага перестройки частоты приема УРП
        {
            public byte lengthCommand; // 0x05
            public byte yy;// уу - 00 режим пеленгования для УРП-1..УРП-5(ответ только от УРП-1)/ уу = 01..06- номер УРП
            public byte constValue;
            // = 0x11 - перестройка частоты приема на заданный шаг ВВЕРХ для УРП
            // = 0x12 - перестройка частоты приема на заданный шаг ВНИЗ для УРП

            public byte xx1;
            public byte xx2; //xx xx - hex- представление частоты в мегагерцах 

            public StepFreqUpDown(byte yy, byte constValue)
            {
                this.lengthCommand = 0x05;
                this.yy = yy;
                this.constValue = constValue;
                this.xx1 = 0x00;
                this.xx2 = 0x00;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SetBandNumber // Установка номера полосы приема УРП
        {
            public byte lengthCommand; // 0x04
            public byte yy;
            // уу - 00 режим пеленгования для УРП-1..УРП-5(ответ только от УРП-1)/ уу = 01..06- номер УРП 
            // yy = 0F-последовательный режим установки УРП1...УРП5
            public byte constValue; // = 0x0F
            public byte xx; // xx - номер полосы приема шириной 30МГц
            
            public SetBandNumber( byte yy, byte xx)
            {
                this.lengthCommand = 0x04;
                this.yy = yy;
                this.constValue = 0x0F;
                this.xx = xx;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SetBandNumberParam 
        {
            public byte lengthCommand; // 0x07
            public byte yy;
            // уу - 00 режим пеленгования для УРП-1..УРП-5(ответ только от УРП-1)/ уу = 01..06- номер УРП 
            // yy = 0F-последовательный режим установки УРП1...УРП5
            
            public byte constValue;
            // = 0x0F - Установка номера полосы приема УРП с параметрами
            // = 0x13 - Перестройка на частотную полосу на шаг ВВЕРХ
            // = 0x14-Перестройка на частотную полосу на шаг ВНИЗ

            public byte xx; // 0x00, а в ответ номер полосы приема шириной 30МГц(0..99)
            public byte zz; //0x00, а в ответ значение аттенюатора в полосе приема
            public byte vv; //0x00, а в ответ значение усилителя в полосе приема
            public byte tt;//0х00, а в ответ байт состояния

            public SetBandNumberParam( byte yy, byte constValue, byte xx)
            {
                this.lengthCommand = 0x07;
                this.yy = yy;
                this.constValue = constValue;
                this.xx = xx;
                this.zz = 0;
                this.vv = 0;
                this.tt = 0;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ManageATTLO1// управление аттенюатором на выходе 1-го гетеродина 0..30дБ с шагом 0.5дБ
        {
            public byte lengthCommand; // = 0x04
            public byte yy; // yy = 01...05 - номер УРП
            public byte constValue;
            public byte xx; //хх = 00//3F, запрос состояния АТТ - хх>= 40(обычно FF)

            public ManageATTLO1(byte yy, byte xx)
            {
                this.lengthCommand = 0x04;
                this.yy = yy;
                this.constValue = 0x05;
                this.xx = xx;
            }
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ManageATT // управление аттенюатором в канале приема 0..30дБ с шагом 0.5дБ
        {
            public byte lengthCommand; // = 0x04
            public byte yy; // yy = 01...05 - номер УРП / yy = 00 -режим пеленгования для УРП1...УРП5 (ответ только от УРП1)
            public byte constValue;//constValue = 0x07
            public byte xx; //хх = 00//3F, запрос состояния АТТ - хх>= 40(обычно FF)

            public ManageATT(byte yy, byte xx)
            {
                this.lengthCommand = 0x04;
                this.yy = yy;
                this.constValue = 0x07;
                this.xx = xx;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Manage10dB// Управление АТТ -10дБ
        {
            public byte lengthCommand; // = 0x04
            public byte yy; // yy = 01...05 - номер УРП /yy = 00 -режим пеленгования для УРП1...УРП5 (ответ только от УРП1)
            public byte constValue;// = 0x06
            public byte xx;// xx = 00 ВЫКЛ, хх = 01 ВКЛ, xx = FF - проверить состояние (в ответ придет 00 или 01)
            public Manage10dB( byte yy, byte xx)
            {
                this.lengthCommand = 0x04;
                this.yy = yy;
                this.constValue = 0x06;
                this.xx = xx;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CalibAlaris // включение/выключение режима калибровки в коммутаторе антенн Alaris(Poiting)
        {
            public byte lengthCommand; // = 0x04
            public byte constValue1; // = 0x09
            public byte constValue2;// = 0x08
            public byte xx;
            // xx = 00 - ВЫКЛ калибровки
            // xx = 01 - ВКЛ встроенного генератора шума для калибровки
            // xx = 02 - ВКЛ внешнего источника сигнала для калибровки
            public CalibAlaris( byte xx)
            {
                this.lengthCommand = 0x04;
                this.constValue1 = 0x09;
                this.constValue2 = 0x08;
                this.xx = xx;
                
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct OverallGain // Регулировка общего усиления -12..20дБ
        {
            public byte lengthCommand; // = 0x04
            public byte yy; // yy = 01...05 - номер УРП /yy = 00 -режим пеленгования для УРП1...УРП5 (ответ только от УРП1)
            public byte constValue;// = 0x0A
            public byte xx;// xx = 00..FF

            public OverallGain(byte yy, byte xx)
            {
                this.lengthCommand = 0x04;
                this.yy = yy;
                this.constValue = 0x0A;
                this.xx = xx;
            }
            
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct StateAttGain //Запрос текущего состояния аттенюатора и общего усиления
        {
            public byte lengthCommand; // = 0x05
            public byte yy; // yy = 01...05 - номер УРП /yy = 00 -режим пеленгования для УРП1...УРП5 (ответ только от УРП1)
            public byte constValue;// = 0x0B
            public byte xx;// = 0x00, а в ответ xx = 00..3F - аттенюатор
            public byte zz;// = 0х00, а в ответ zz = 00..FF - усилитель на ПЧ-выходе модуля УРП

            public StateAttGain(byte yy)
            {
                this.lengthCommand = 0x05;
                this.yy = yy;
                this.constValue = 0x0B;
                this.xx = 0x00;
                this.zz = 0x00;
            }

        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SwitchLO
        {
            public byte lengthCommand; // = 0x04
            public byte yy; // = 01..06 - номер УРП
            
            public byte constValue;
            // = 0x0С - переключение ПЕРВОГО гетерордина на верхнюю или нижнюю полосу 
            // = 0x0D - переключение ВТОРОГО гетерордина на верхнюю или нижнюю полосу

            public byte xx;
            // xx = 00 - нижняя полоса,
            // xx = 01 - верхняя полоса,
            // xx = 0F - опрос состояние.
            public SwitchLO(byte yy, byte constValue, byte xx)
            {
                this.lengthCommand = 0x04;
                this.yy = yy;
                this.constValue = constValue;
                this.xx = xx;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ConditionFiltLO// запрос состояния фильтров гетеродинов( толлько для автономного управления модулем Урп)
        {
            public byte lengthCommand; // = 0x05
            public byte yy; // yy = 01...05 - номер УРП
            public byte constValue; // = 0x09

            public byte xx;// = 0x00, а в ответ придет состояние фильтра ПЕРВОГО гетеродина (00- 950-2200; 03- 2000-2700; 02 - 2700-3500; 01- 3300-3900)

            public byte zz;// = 0x00, а в ответ придет состояние фильтра ВТОРОГО гетеродина(00- 1425-1575; 01 - 470-880)

            public ConditionFiltLO(byte yy)
            {
                this.lengthCommand = 0x05;
                this.yy = yy;
                this.constValue = 0x09;
                this.xx = 0x00;
                this.zz = 0x00;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ConditionFiltPresel // запрос состояния фильтров преселектора
        {
            public byte lengthCommand; // = 0x04
            public byte yy; // yy = 01...05 - номер УРП
            public byte constValue; // = 0x29

            public byte xx;// = 0x00, а в ответ придет номер фильтра преселектора (1...10)

            public ConditionFiltPresel(byte yy)
            {
                this.lengthCommand = 0x04;
                this.yy = yy;
                this.constValue = 0x29;
                this.xx = 0x00; 
            }
            
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct RangeLO
        {
            public byte lengthCommand; // = 0x04
            public byte yy; // yy = 01...05 - номер УРП
            public byte constValue; 
            // = 0x30 - переключение диапазона  работы ПЕРВОГО гетеродина
            // = 0x31 - переключение диапазона  работы ВТОРОГО гетеродина

            public byte xx;
            // = 0x00 - Нижний диапазон
            // = 0x01 - Верхний диаппазон

            public RangeLO(byte yy, byte constValue, byte xx)
            {
                this.lengthCommand = 0x04;
                this.yy = yy;
                this.constValue = constValue;
                this.xx = xx;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CalibLO1 //калибровка ПЕРВОГО гетеродина на текущей полосе приема
        {
            public byte lengthCommand; // = 0x04
            public byte yy; // yy = 01...05 - номер УРП
            public byte constValue;// = 0x32
            public byte xx;
            // = 0x00 - Калибровка прошла успешно
            // = 0x01 - В результате калибровки уровень гетеродина не соответствует ожидаемому
            public CalibLO1(byte yy)
            {
                this.lengthCommand = 0x04;
                this.yy = yy;
                this.constValue = 0x32;
                this.xx = 0x00;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SynchPeriod // Установка периода цикловой синхронизации (в мс)
        {
            public byte lengthCommand; // = 0x05
            public byte constValue1; //= 0x35
            public byte constValue2;// = 0x35
            public byte xx1;//хх хх = 00 00 - производится запрос текущего значения периода,
            public byte xx2;//хх хх = FF FF - отключение цикловой синхронизации
            // хх хх -  период в пределах 1...100 мс включает цикловую синхронизацию с указанным периодом.
            public SynchPeriod(byte xx1, byte xx2)
            {
                this.lengthCommand = 0x05;
                this.constValue1 = 0x0A;
                this.constValue2 = 0x35;
                this.xx1 = xx1;
                this.xx2 = xx2;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct SignalLevel
        {
            public byte lengthCommand; // = 0x05
            public byte yy; // yy = 01...05 - номер УРП
            public byte constValue; 
            // = 0x38 - запрос уровня сигнала на ПЧ-выходе УРП
            // = 0х39 - запрос уровня сигнала ПЕРВОГО гетеродина
            // = 0х3А - запрос уровня сигнала ВТОРОГО гетеродина

            public byte xx1;
            public byte xx2;// xx xx = 0x0000..0x0FFF - уровень сигнала, измеренный 12-битной АЦП

            public SignalLevel(byte yy, byte constValue)
            {
                this.lengthCommand = 0x05;
                this.yy = yy;
                this.constValue = constValue;
                this.xx1 = 0x00;
                this.xx2 = 0x00;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ModuleTemper // Запрос темературы модуля УРП
        {
            public byte lengthCommand; // = 0x05
            public byte yy; // yy = 01...05 - номер УРП
            public byte constValue; // 0x2C

            public byte xx1;
            public byte xx2;// xx xx = 0x0000..0x0FFF - уровень сигнала, измеренный 12-битной АЦП

            public ModuleTemper(byte yy)
            {
                this.lengthCommand = 0x05;
                this.yy = yy;
                this.constValue = 0x2C;
                this.xx1 = 0x00;
                this.xx2 = 0x00;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ShiftFront // Сдвиг фронта циклового сигнала относительно тактового сигнала
        {
            public byte lengthCommand; // = 0x05
            public byte constValue1; // = 0x0C
            public byte constValue2; // = 0x41

            public byte xx1;
            public byte xx2;// xx xx = 5..522, если хх хх за пределами указанного выше диапазона, в ответ будет выдано текущее значение сдвига
        }

    }

}
