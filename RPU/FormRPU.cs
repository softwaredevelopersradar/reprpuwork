﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ini;
using SDS;
using System.Windows.Forms;
using System.IO.Ports;
using System.Net.Configuration;
using System.Threading;
using Microsoft.VisualBasic;
//using System.IO;
using HardwareLibrary;
using HardwareLibrary.Receivers;
using DspDataModel.Hardware;
using HardwareLibrary.FpgaDevices;

namespace RPU
{
    public partial class FormRPU : Form
    {
        //delegate void DelPollLO();
        //event DelPollLO EvPollLO;
        delegate void DelegLOG(byte[] answer);
        event DelegLOG EvEnLOG;
        private SerialPort portCOM;
        private string PortName;
        private bool flag = true;
        private bool heter1 = true, heter2 = true;
        IniFile ini;
        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        private int numberURP = -1;
        private bool flagOpen, flagSeven;
        public Label[] LabelPanel;
        public TextBox[] Textbox;
        public Button[] BuTTon;
        delegate void LevelSignalScheme();
        event LevelSignalScheme EventLevelSignal;
        bool FlagEnabled = true;
        IReceiverManager portSPI;
        int DelayCalib, DelayRead, DelayOther;
        public FormRPU()
        {
            InitializeComponent();
            EventLevelSignal += new LevelSignalScheme(ShowLevelSignalSheme);
            //EvPollLO += new DelPollLO(PollLO);
            EvEnLOG += new DelegLOG(WriteToFile);
        }

        public void WriteToFile(byte[] mess) //Запись в LOG
        {
            DateTime Time = DateTime.Now;
            IniFile ini = new IniFile(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Message.ini");
            string pathFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\\LOG"; // путь  в папку LOG
            Directory.CreateDirectory(pathFolder);
            string name = Time.ToString(@"dd\_MM\_yyyy") + ".txt";
            StreamWriter WrStream = new StreamWriter(pathFolder + @"\" + name, true, System.Text.Encoding.Default);

            string Line = "\n" + Time.ToString("HH:mm:ss") + " ";

            for (int i = 0; i < 7; i++)
            {
                try
                {
                    Line += mess[i].ToString("X2");
                }
                catch { Line += "  "; }
                Line += " ";
            }
            Line += "  ";

            try
            {
                switch (mess[0])
                {
                    case 0x03:
                        Line += ini.IniReadValue("Length3", "Mess" + mess[2].ToString("X2"));
                        WrStream.WriteLine(Line);
                        break;
                    case 0x04:
                        Line += String.Format(ini.IniReadValue("Length4", "Mess" + mess[2].ToString("X2")), mess[1].ToString("X2"), mess[3].ToString("X2"));
                        WrStream.WriteLine(Line);
                        break;
                    case 0x05:
                        Line += String.Format(ini.IniReadValue("Length5", "Mess" + mess[2].ToString("X2")), mess[1].ToString("X2"), mess[3].ToString("X2"), mess[4].ToString("X2"));
                        WrStream.WriteLine(Line);
                        break;
                    case 0x07:
                        Line += String.Format(ini.IniReadValue("Length7", "Mess" + mess[2].ToString("X2")), mess[1].ToString("X2"), mess[3].ToString("X2"), mess[4].ToString("X2"), mess[5].ToString("X2"), mess[6].ToString("X2"));
                        WrStream.WriteLine(Line);
                        break;
                    default:
                        Line += "В ответ пришла неверная кодограмма";
                        WrStream.WriteLine(Line);
                        break;

                }
                WrStream.Close();
            }
            catch
            {
                Line += "В ответ пришла неверная кодограмма";
                WrStream.Close();
            }

        }
        int Gain(bool up, int varible)
        {
            if (up == true)
            {
                int G = varible;
                int W = (int)Math.Round(((G + 13.75) / 0.147075f));
                return W;
            }
            else
            {
                int W = varible;
                int G = (int)Math.Round((W * 0.147075f - 13.75));
                return G;
            }
        }

        void EnabledComponents(bool flagEnabled)
        {
            ATT1.Enabled = flagEnabled;
            ATT2.Enabled = flagEnabled;
            ATT3.Enabled = flagEnabled;
            ATT4.Enabled = flagEnabled;
            panelATT1.Enabled = flagEnabled;
            panelATT2.Enabled = flagEnabled;
            panelUNN.Enabled = flagEnabled;
            LOHeter1.Enabled = flagEnabled;
            LOHeter2.Enabled = flagEnabled;
            panelATTLo1.Enabled = flagEnabled;
            LO1_Peleng.Enabled = flagEnabled;
            ButCalibCoh.Enabled = flagEnabled;
            ButCalibInd.Enabled = flagEnabled;
            LoadButton.Enabled = flagEnabled;
        }
        void PollLO()
        {
            Command.ManageATTLO1 commandATT = new Command.ManageATTLO1(Convert.ToByte(numberURP), 0xFF);
            byte[] messAtt = SdS.Serialization(commandATT);
            byte[] answerAtt = MessPort(messAtt, false);// опрос сосотояния аттенюатора на  выходе 1-го гетеродина
            LO1_ATT.Text = (answerAtt[3] / 2).ToString();
            LO1_ATT.BackColor = Color.White;
        }

        void SwitchFilter() //переделать для переключения фильтров
        {
            if (numberURP >= 0)
            {
                Command.ConditionFiltPresel command = new Command.ConditionFiltPresel(Convert.ToByte(numberURP));
                byte[] mess = SdS.Serialization(command);
                byte[] answer = MessPort(mess, true);
                switch (answer[3])
                {
                    case 0x01:
                        Filter1.Value = true;
                        Filter2.Value = false;
                        Filter3.Value = false;
                        Filter4.Value = false;
                        Filter5.Value = false;
                        Filter6.Value = false;
                        Filter7.Value = false;
                        Filter8.Value = false;
                        Filter9.Value = false;
                        Filter10.Value = false;
                        Switch1.BackgroundImage = Properties.Resources.ON1;
                        Switch4.BackgroundImage = Properties.Resources.ON2;
                        Switch5.BackgroundImage = Properties.Resources.ON1;
                        flag = true;
                        panelATT1.Enabled = true;
                        panelATT2.Enabled = false;
                        break;
                    case 0x02:
                        Filter1.Value = false;
                        Filter2.Value = true;
                        Filter3.Value = false;
                        Filter4.Value = false;
                        Filter5.Value = false;
                        Filter6.Value = false;
                        Filter7.Value = false;
                        Filter8.Value = false;
                        Filter9.Value = false;
                        Filter10.Value = false;
                        Switch1.BackgroundImage = Properties.Resources.ON1;
                        Switch4.BackgroundImage = Properties.Resources.ON2;
                        Switch5.BackgroundImage = Properties.Resources.ON1;
                        flag = true;
                        panelATT1.Enabled = true;
                        panelATT2.Enabled = false;
                        break;
                    case 0x03:
                        Filter1.Value = false;
                        Filter2.Value = false;
                        Filter3.Value = true;
                        Filter4.Value = false;
                        Filter5.Value = false;
                        Filter6.Value = false;
                        Filter7.Value = false;
                        Filter8.Value = false;
                        Filter9.Value = false;
                        Filter10.Value = false;
                        Switch1.BackgroundImage = Properties.Resources.ON1;
                        Switch4.BackgroundImage = Properties.Resources.ON2;
                        Switch5.BackgroundImage = Properties.Resources.ON1;
                        flag = true;
                        panelATT1.Enabled = true;
                        panelATT2.Enabled = false;
                        break;
                    case 0x04:
                        Filter1.Value = false;
                        Filter2.Value = false;
                        Filter3.Value = false;
                        Filter4.Value = true;
                        Filter5.Value = false;
                        Filter6.Value = false;
                        Filter7.Value = false;
                        Filter8.Value = false;
                        Filter9.Value = false;
                        Filter10.Value = false;
                        Switch1.BackgroundImage = Properties.Resources.ON1;
                        Switch4.BackgroundImage = Properties.Resources.ON2;
                        Switch5.BackgroundImage = Properties.Resources.ON1;
                        flag = true;
                        panelATT1.Enabled = true;
                        panelATT2.Enabled = false;
                        break;
                    case 0x05:
                        Filter1.Value = false;
                        Filter2.Value = false;
                        Filter3.Value = false;
                        Filter4.Value = false;
                        Filter5.Value = true;
                        Filter6.Value = false;
                        Filter7.Value = false;
                        Filter8.Value = false;
                        Filter9.Value = false;
                        Filter10.Value = false;
                        Switch1.BackgroundImage = Properties.Resources.ON1;
                        Switch4.BackgroundImage = Properties.Resources.ON2;
                        Switch5.BackgroundImage = Properties.Resources.ON1;
                        flag = true;
                        panelATT1.Enabled = true;
                        panelATT2.Enabled = false;
                        break;
                    case 0x06:
                        Filter1.Value = false;
                        Filter2.Value = false;
                        Filter3.Value = false;
                        Filter4.Value = false;
                        Filter5.Value = false;
                        Filter6.Value = true;
                        Filter7.Value = false;
                        Filter8.Value = false;
                        Filter9.Value = false;
                        Filter10.Value = false;
                        Switch1.BackgroundImage = Properties.Resources.ON1;
                        Switch4.BackgroundImage = Properties.Resources.ON2;
                        Switch5.BackgroundImage = Properties.Resources.ON1;
                        flag = true;
                        panelATT1.Enabled = true;
                        panelATT2.Enabled = false;
                        break;
                    case 0x07:
                        Filter1.Value = false;
                        Filter2.Value = false;
                        Filter3.Value = false;
                        Filter4.Value = false;
                        Filter5.Value = false;
                        Filter6.Value = false;
                        Filter7.Value = true;
                        Filter9.Value = false;
                        Filter10.Value = false;
                        Switch1.BackgroundImage = Properties.Resources.ON1;
                        Switch4.BackgroundImage = Properties.Resources.ON2;
                        Switch5.BackgroundImage = Properties.Resources.ON1;
                        flag = true;
                        panelATT1.Enabled = true;
                        panelATT2.Enabled = false;
                        break;
                    case 0x08:
                        Filter1.Value = false;
                        Filter2.Value = false;
                        Filter3.Value = false;
                        Filter4.Value = false;
                        Filter5.Value = false;
                        Filter6.Value = false;
                        Filter7.Value = false;
                        Filter8.Value = true;
                        Filter9.Value = false;
                        Filter10.Value = false;
                        Switch1.BackgroundImage = Properties.Resources.ON1;
                        Switch4.BackgroundImage = Properties.Resources.ON2;
                        Switch5.BackgroundImage = Properties.Resources.ON1;
                        flag = true;
                        panelATT1.Enabled = true;
                        panelATT2.Enabled = false;
                        break;
                    case 0x09:
                        Filter1.Value = false;
                        Filter2.Value = false;
                        Filter3.Value = false;
                        Filter4.Value = false;
                        Filter5.Value = false;
                        Filter6.Value = false;
                        Filter7.Value = false;
                        Filter8.Value = false;
                        Filter9.Value = true;
                        Filter10.Value = false;
                        Switch1.BackgroundImage = Properties.Resources.OFF1;
                        Switch4.BackgroundImage = Properties.Resources.OFF2;
                        Switch5.BackgroundImage = Properties.Resources.OFF1;
                        Switch6.BackgroundImage = Properties.Resources.ON1;
                        Switch7.BackgroundImage = Properties.Resources.ON2;
                        flag = false;
                        panelATT1.Enabled = false;
                        panelATT2.Enabled = true;
                        break;
                    case 0x0A:
                        Filter1.Value = false;
                        Filter2.Value = false;
                        Filter3.Value = false;
                        Filter4.Value = false;
                        Filter5.Value = false;
                        Filter6.Value = false;
                        Filter7.Value = false;
                        Filter8.Value = false;
                        Filter9.Value = false;
                        Filter10.Value = true;
                        Switch1.BackgroundImage = Properties.Resources.OFF1;
                        Switch4.BackgroundImage = Properties.Resources.OFF2;
                        Switch5.BackgroundImage = Properties.Resources.OFF1;
                        Switch6.BackgroundImage = Properties.Resources.OFF1;
                        Switch7.BackgroundImage = Properties.Resources.OFF2;
                        flag = false;
                        panelATT1.Enabled = false;
                        panelATT2.Enabled = true;
                        break;
                    default:
                        MessageBox.Show("Ошибка запроса состояния фильтров");
                        break;
                }
            }
            else
            {
                MessageBox.Show("Необходимо выбрать номер УРП. Режим пеленгования НЕ выбирать(УРП1...УРП6)");
                NumberURP.Focus();
            }
        }
        void Seven(byte code, byte xx, bool flagg)// отработка команды "Установка номера полосы приема" xx-номер полосы приема
        {
            if (numberURP >= 0)
            {
                flagSeven = true;
                byte[] message;
                if (code == 0x00)
                {
                    Command.CurrentStateURP command = new Command.CurrentStateURP(Convert.ToByte(numberURP));
                    message = SdS.Serialization(command);
                }
                else
                {
                    Command.SetBandNumberParam command = new Command.SetBandNumberParam(Convert.ToByte(numberURP), code,
                        xx);
                    message = SdS.Serialization(command);
                }
                byte[] answer = MessPort(message, flagg);
                BandNumber.Text = answer[3].ToString();//(Convert.ToInt32(xx)).ToString();
                BandNumber.BackColor = Color.White;
                FreqLabel.Text = "F = " + (Convert.ToInt32(BandNumber.Text) * 30 + 40).ToString() + " Mhz";
                SwitchFilter(); //отображение попадания частоты в определенный фильтр
                LedATT1.Value = false;
                LedATT2.Value = false;
                LedATT3.Value = false;
                LedATT4.Value = false;
                if (flag == true)
                {
                    ATTBox1.Text = (answer[4] / 2).ToString(); //значение атт в полоce приема
                    ATTBox1.BackColor = Color.White;
                }
                else
                {
                    ATTBox2.Text = (answer[4] / 2).ToString(); //значение атт в полоce приема
                    ATTBox2.BackColor = Color.White;
                }
                if (Gain(false, answer[5]) >= -12 && Gain(false, answer[5]) <= 22)
                {
                    UNNBox.Text = (Gain(false, answer[5])).ToString(); //значение усиления
                    UNNBox.BackColor = Color.White;
                }
                int mess = answer[6];
                int[] BinAnswer;
                string mesS = Convert.ToString(mess, 2);
                BinAnswer = new int[4];

                if (BinAnswer.Length > mesS.Length)
                {
                    int i;
                    for (i = 0; i < (BinAnswer.Length - mesS.Length); i++)
                    {
                        BinAnswer[i] = 0;
                    }
                    for (int j = 0; j < mesS.Length; j++)
                    {
                        BinAnswer[i] = Convert.ToInt32(mesS.Substring(j, 1));
                        i++;
                    }
                }
                else
                {

                    for (int i = 0; i < 4; i++)
                    {
                        BinAnswer[i] = Convert.ToInt32(mesS.Substring(i, 1));
                    }
                }
                int rowIndex = dataGridView.Rows.Add();
                string answerS = "";
                for (int i = 3; i >= 0; i--)
                {
                    answerS += " " + BinAnswer[i].ToString("X2");
                }
                dataGridView.Rows[rowIndex].Cells[0].Value = mess.ToString();
                dataGridView.Rows[rowIndex].Cells[1].Value = answerS;

                //Отображение последнего добавленного элемнта
                dataGridView.FirstDisplayedScrollingRowIndex = dataGridView.RowCount - 1;

                LOHeter2.Value = Convert.ToBoolean(BinAnswer[3]);
                // 0-й бит (3-й) - диапазон 2-го гетеродина (1 - верхний диапазон, 0 - нижний диапазон)
                //1-1 бит (2-й) не используется

                if (flag == true) // 2-й (1-й) бит - состояние аттенюатора -10дБ (1-ВКЛ, 0-ВЫКЛ)
                {
                    if (BinAnswer[1] == 1)
                    {
                        LedATT1.Value = false;
                        LedATT2.Value = true;
                        LedATT3.Value = false;
                        LedATT4.Value = false;
                        Switch2.BackgroundImage = Properties.Resources.OFF1;
                        Switch3.BackgroundImage = Properties.Resources.OFF2;
                    }
                    else
                    {
                        LedATT1.Value = true;
                        LedATT2.Value = false;
                        LedATT3.Value = false;
                        LedATT4.Value = false;
                        Switch2.BackgroundImage = Properties.Resources.ON1;
                        Switch3.BackgroundImage = Properties.Resources.ON2;
                    }
                }
                else
                {
                    if (BinAnswer[1] == 1)
                    {
                        LedATT1.Value = false;
                        LedATT2.Value = false;
                        LedATT3.Value = false;
                        LedATT4.Value = true;
                        Switch8.BackgroundImage = Properties.Resources.OFF1;
                        Switch9.BackgroundImage = Properties.Resources.OFF2;
                    }
                    else
                    {
                        LedATT1.Value = false;
                        LedATT2.Value = false;
                        LedATT3.Value = true;
                        LedATT4.Value = false;
                        Switch8.BackgroundImage = Properties.Resources.ON1;
                        Switch9.BackgroundImage = Properties.Resources.ON2;
                    }
                }

                LOHeter1.Value = Convert.ToBoolean(BinAnswer[0]);
                // 3-й бит (0-й) - диапазон 1-го гетеродина (1 - верхний диапазон, 0 - нижний диапазон)


                //////************************************************************************////////////
                //Command.ManageATTLO1 commandLO1 = new Command.ManageATTLO1(Convert.ToByte(numberURP), 0xFF);// управление АТТ н выходе 1-го гетеродина 0..30дБ
                //message = SdS.Serialization(commandLO1);
                //answer = new byte[4];
                //answer = MessPort(message, false);
                //LO1_ATT.Text = (answer[3] / 2).ToString();
                //LO1_ATT.BackColor = Color.White;
                /////*************************************************************************/////////////


                flagSeven = false;
            }
            else
            {
                MessageBox.Show("Необходимо выбрать номер УРП");
                NumberURP.Focus();
            }
        }

        byte[] MessPort(byte[] mess, bool first)
        {
            if (portCOM != null)
            {
                portCOM.ReadExisting();
                portCOM.Write(mess, 0, mess.Length);
            }
            else 
            {
                portSPI.DataChannel.Write(mess, 0, mess.Length);
            }
            Thread.Sleep(DelayOther);
            byte[] answer = new byte[mess.Length];
            try
            {
                if (portCOM != null)
                {
                    portCOM.Read(answer, 0, answer.Length);
                }
                else
                {
                    portSPI.DataChannel.Read(answer, 0, answer.Length);
                }
                if (EvEnLOG != null)
                {
                    EvEnLOG(answer);
                }
            }
            catch
            {
                if (EvEnLOG != null)
                {
                    EvEnLOG(answer);
                }
            }
            int rowIndex = dataGridView.Rows.Add();
            string answerS = "", mesS = "";
            for (int i = 0; i < mess.Length; i++)
            {
                answerS += " " + answer[i].ToString("X2");
                mesS += " " + mess[i].ToString("X2");
            }
            dataGridView.Rows[rowIndex].Cells[0].Value = mesS;
            dataGridView.Rows[rowIndex].Cells[1].Value = answerS;

            //Отображение последнего добавленного элемнта
            dataGridView.FirstDisplayedScrollingRowIndex = dataGridView.RowCount - 1;
            if (flagSeven == false)
            {
                if (first == true)
                {
                    dataGridView.Rows[rowIndex].Cells[0].Style.BackColor = Color.Aqua;
                    dataGridView.Rows[rowIndex].Cells[1].Style.BackColor = Color.Aqua;
                }
            }

            return answer;
        }
        private void SetParam()
        {
            SchemeControl.Enabled = false;
            NumberURP.Enabled = false;
            flagOpen = false; 
            label10.Text = "\u1403" + "F = " + 30 + "MHz";
            label11.Text = "\u1403" + "F = " + 30 + "MHz";
            label13.Text = "\u1403" + "F = " + 30 + "MHz";
            string[] portnames = SerialPort.GetPortNames();
            ComPortName.Items.AddRange(portnames);
            ini = new IniFile(path + "\\Settings.ini");
            if (ini.IniReadValue("COM", "Name") != "")
            {
                ComPortName.SelectedIndex = ComPortName.FindString(ini.IniReadValue("COM", "Name"));
            }
            SpeedPortBox.Text = ini.IniReadValue("COM", "Speed");
            LO1_FirstNormStart.Value = Convert.ToInt16(ini.IniReadValue("LO1", "FirstNormStart"));
            LO1_FirstNormEnd.Value = Convert.ToInt16(ini.IniReadValue("LO1", "FirstNormEnd"));
            LO1_SecondNormEnd.Value = Convert.ToInt16(ini.IniReadValue("LO1", "SecondNormEnd"));
            LO1_SecondNormStart.Value = Convert.ToInt16(ini.IniReadValue("LO1", "SecondNormStart"));
            LO2_NormStart.Value = Convert.ToInt16(ini.IniReadValue("LO2", "NormStart"));
            LO2_NormEnd.Value = Convert.ToInt16(ini.IniReadValue("LO2", "NormEnd"));
            DelayCalib = Convert.ToInt32(ini.IniReadValue("TimeSleep", "Calib"));
            DelayRead = Convert.ToInt32(ini.IniReadValue("TimeSleep", "Read"));
            DelayOther = Convert.ToInt32(ini.IniReadValue("TimeSleep", "Other"));
        }
        void EnabledPanel(bool flagSPI, bool flagCom)
        {
            SPIPanel.Enabled = flagSPI;
            ComPanel.Enabled = flagCom;
        }
        void EnableScheme(bool flagScheme)
        {
            SchemeControl.Enabled = flagScheme;
            NumberURP.Enabled = flagScheme;
            flagOpen = flagScheme;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            SetParam();
            EnabledComponents(false);
        }

        private void ComPortName_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = ComPortName.SelectedIndex;
            PortName = ComPortName.Items[index].ToString();
            /*try
            {
                if (portCOM != null)
                {
                    portCOM.Close();
                    portCOM = new SerialPort(PortName, Convert.ToInt32(SpeedPortBox.Text), Parity.None, 8, StopBits.One);
                    portCOM.WriteTimeout = 500;
                    portCOM.ReadTimeout = 500;
                    portCOM.Open();
                    if (portCOM.IsOpen)
                    {
                        Command.EmptyCommand empty = new Command.EmptyCommand(0x03, 0x0A, 0x00);
                        byte[] mess = SDS.SdS.Serialization(empty);
                        MessPort(mess, true);
                        OpenPort.Text = "Close";
                        EnabledPanel(false, true);
                        EnableScheme(true);
                    }
                }
                else
                {
                    portCOM = new SerialPort(PortName, Convert.ToInt32(SpeedPortBox.Text), Parity.None, 8, StopBits.One);
                    portCOM.WriteTimeout = 500;
                    portCOM.ReadTimeout = 500;
                    portCOM.Open();
                    if (portCOM.IsOpen)
                    {
                        Command.EmptyCommand empty = new Command.EmptyCommand(0x03, 0x0A, 0x00);
                        byte[] mess = SDS.SdS.Serialization(empty);
                        MessPort(mess, true);
                        OpenPort.Text = "Close";
                        EnabledPanel(false, true);
                        EnableScheme(true);
                    }
                }
            }
            catch (Exception)
            {
                OpenPort.Text = "Open";
            }*/
        }

        private void OpenPort_Click(object sender, EventArgs e)
        {
            if (SpeedPortBox.Text != "")
            {
                try
                {
                    if (flagOpen == false)
                    {
                        portCOM = new SerialPort(PortName, Convert.ToInt32(SpeedPortBox.Text), Parity.None, 8, StopBits.One);
                        portCOM.WriteTimeout = 500;
                        portCOM.ReadTimeout = 500;
                        portCOM.Open();
                        if (portCOM.IsOpen)
                        {
                            //Command.EmptyCommand empty = new Command.EmptyCommand(0x03, 0x0A, 0x00);
                            byte[] mess = { 0x03, 0x0A, 0x00 };//SDS.SdS.Serialization(empty);
                            MessPort(mess, true);
                            OpenPort.Text = "Close";
                            EnabledPanel(false, true);
                            EnableScheme(true);
                        }

                    }
                    else
                    {
                        portCOM.Close();
                        portCOM.Dispose();
                        OpenPort.Text = "Open";
                        EnabledPanel(true, true);
                        EnableScheme(false);
                        portCOM = null;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Ошибка порта");
                }
            }
            else
            {
                MessageBox.Show("Введите значение скорости передачи comport");
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ini.IniWriteValue("COM", "Name", PortName);
            ini.IniWriteValue("COM", "Speed", SpeedPortBox.Text);
            if (portCOM != null) 
            {
                portCOM.Close();
            }
        }

        private void LO1_Click(object sender, EventArgs e)
        {

            SchemeControl.SelectedTab = Heter1;
        }

        private void LO2_Click(object sender, EventArgs e)
        {

            SchemeControl.SelectedTab = Heter2;
        }



        private void ATT1_Click(object sender, EventArgs e)
        {
            if (flag == true)
            {
                if (numberURP >= 0)
                {
                    Command.Manage10dB command = new Command.Manage10dB(Convert.ToByte(numberURP), 0x00);
                    byte[] mess = SdS.Serialization(command);
                    byte[] answer = MessPort(mess, true);
                    if (answer[3] == mess[3])
                    {
                        LedATT1.Value = true;
                        LedATT2.Value = false;
                        LedATT3.Value = false;
                        LedATT4.Value = false;
                        Switch2.BackgroundImage = Properties.Resources.ON1;
                        Switch3.BackgroundImage = Properties.Resources.ON2;
                        if (EventLevelSignal != null)
                        {
                            EventLevelSignal();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Необходимо выбрать номер УРП");
                    NumberURP.Focus();
                }
            }
            else
            {
                LedATT1.Value = false;
                LedATT2.Value = false;
            }
        }

        private void ATT2_Click(object sender, EventArgs e)
        {
            if (flag == true)
            {
                if (numberURP >= 0)
                {
                    Command.Manage10dB command = new Command.Manage10dB(Convert.ToByte(numberURP), 0x01);
                    byte[] mess = SdS.Serialization(command);
                    byte[] answer = MessPort(mess, true);
                    if (answer[3] == mess[3])
                    {
                        LedATT1.Value = false;
                        LedATT2.Value = true;
                        LedATT3.Value = false;
                        LedATT4.Value = false;
                        Switch2.BackgroundImage = Properties.Resources.OFF1;
                        Switch3.BackgroundImage = Properties.Resources.OFF2;
                        if (EventLevelSignal != null)
                        {
                            EventLevelSignal();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Необходимо выбрать номер УРП");
                    NumberURP.Focus();
                }
            }
            else
            {
                LedATT1.Value = false;
                LedATT2.Value = false;
            }

        }

        private void ATT3_Click(object sender, EventArgs e)
        {
            if (flag == false)
            {
                if (numberURP >= 0)
                {
                    Command.Manage10dB command = new Command.Manage10dB(Convert.ToByte(numberURP), 0x00);
                    byte[] mess = SdS.Serialization(command);
                    byte[] answer = MessPort(mess, true);
                    if (answer[3] == mess[3])
                    {
                        LedATT1.Value = false;
                        LedATT2.Value = false;
                        LedATT3.Value = true;
                        LedATT4.Value = false;
                        Switch8.BackgroundImage = Properties.Resources.ON1;
                        Switch9.BackgroundImage = Properties.Resources.ON2;
                        if (EventLevelSignal != null)
                        {
                            EventLevelSignal();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Необходимо выбрать номер УРП");
                    NumberURP.Focus();
                }
            }
            else
            {
                LedATT3.Value = false;
                LedATT4.Value = false;
            }
        }

        private void ATT4_Click(object sender, EventArgs e)
        {
            if (flag == false)
            {
                if (numberURP >= 0)
                {
                    Command.Manage10dB command = new Command.Manage10dB(Convert.ToByte(numberURP), 0x01);
                    byte[] mess = SdS.Serialization(command);
                    byte[] answer = MessPort(mess, true);
                    if (answer[3] == mess[3])
                    {
                        LedATT1.Value = false;
                        LedATT2.Value = false;
                        LedATT3.Value = false;
                        LedATT4.Value = true;
                        Switch8.BackgroundImage = Properties.Resources.OFF1;
                        Switch9.BackgroundImage = Properties.Resources.OFF2;
                        if (EventLevelSignal != null)
                        {
                            EventLevelSignal();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Необходимо выбрать номер УРП");
                    NumberURP.Focus();
                }
            }
            else
            {
                LedATT3.Value = false;
                LedATT4.Value = false;
            }
        }


        private void Heter1_Click(object sender, EventArgs e)
        {
            SchemeControl.SelectedTab = Scheme;
        }

        private void Heter2_Click(object sender, EventArgs e)
        {

            SchemeControl.SelectedTab = Scheme;
        }

        private void BandNumber_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if ((BandNumber.Text != "") && (Convert.ToInt32(BandNumber.Text) >= 0) &&
                    (Convert.ToInt32(BandNumber.Text) <= 99))
                {
                    int number = Convert.ToInt32(BandNumber.Text);
                    Seven(0x0F, Convert.ToByte(number), true); //установка номера полосы приема УРП
                    FreqLabel.Text = "F = " + (Convert.ToInt32(BandNumber.Text) * 30 + 40).ToString() + " MHz";
                    if (EventLevelSignal != null)
                    {
                        EventLevelSignal();
                    }

                    /*int number = Convert.ToInt32(BandNumber.Text);
                    byte[] mess = new byte[4] { 0x04, 0x01, 0x0F, Convert.ToByte(number)}; //команда по установке номера полосы приема РПУ (№6)
                   byte[] answer =  MessPort(mess, true);
                    if (answer[3] == mess[3])
                    {
                        FreqLabel.Text = "F = " + (Convert.ToInt32(BandNumber.Text) * 30 + 40).ToString() + " MHz";
                        Seven(Convert.ToByte(number));
                        BandNumber.BackColor = Color.White;
                    }*/
                }
            }
        }

        private void BandNumber_TextChanged(object sender, EventArgs e)
        {
            BandNumber.BackColor = Color.Aquamarine;
        }

        private void BandNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }
        void ClearLabel(Label[] lab, string text)
        {
            int index = 1;
            for (int i = 0; i < lab.Length; i++)
            {
                lab[i].Text = text + " " + index.ToString();
                index++;
                if (index > 5)
                    index = 1;
            } 
        }

        private void PlusBand_Click(object sender, EventArgs e)
        {
            if (BandNumber.Text != "")
            {
                if ((Convert.ToInt32(BandNumber.Text) >= 0) && (Convert.ToInt32(BandNumber.Text) < 99))
                {
                    int number = Convert.ToInt32(BandNumber.Text) + 1;
                    BandNumber.Text = number.ToString();
                    BandNumber_KeyUp(null, new KeyEventArgs(Keys.Enter));
                    //Seven(0x13, Convert.ToByte(number), true); // Перестройка на частотную полосу на шаг вверх
                    FreqLabel.Text = "F = " + (Convert.ToInt32(BandNumber.Text) * 30 + 40).ToString() + " MHz";
                }
                else // BandNumber = 99
                {
                    int number = 99;
                    BandNumber.Text = number.ToString();
                    BandNumber_KeyUp(null, new KeyEventArgs(Keys.Enter));                    
                    //Seven(0x0F, Convert.ToByte(number), true);//Установка номера полосы приема Урп с параметрами
                    FreqLabel.Text = "F = " + (Convert.ToInt32(BandNumber.Text) * 30 + 40).ToString() + " MHz";
                }
                if (EventLevelSignal  != null)
                {
                    EventLevelSignal();
                }
            }
        }

        private void MinusBand_Click(object sender, EventArgs e)
        {
            if (BandNumber.Text != "")
            {
                if ((Convert.ToInt32(BandNumber.Text) > 0) && (Convert.ToInt32(BandNumber.Text) <= 99))
                {
                    int number = Convert.ToInt32(BandNumber.Text) - 1;
                    BandNumber.Text = number.ToString();
                    BandNumber_KeyUp(null, new KeyEventArgs(Keys.Enter));
                    //Seven(0x14, Convert.ToByte(number), true); //Перестройка на частотную полосу на шаг вниз
                    FreqLabel.Text = "F = " + (Convert.ToInt32(BandNumber.Text) * 30 + 40).ToString() + " MHz";
                    /*
                    int number = Convert.ToInt32(BandNumber.Text) - 1;
                    BandNumber.Text = number.ToString();
                    byte[] mess = new byte[4] { 0x04, 0x01, 0x0F, Convert.ToByte(number) };//команда по установке номера полосы приема РПУ (№6)
                    byte[] answer = MessPort(mess, true);
                    FreqLabel.Text = "F = ";
                    if (answer[3] == mess[3])
                    {
                        FreqLabel.Text = "F = " + (Convert.ToInt32(BandNumber.Text) * 30 + 40).ToString() + " MHz";
                        Seven(Convert.ToByte(number));
                        BandNumber.BackColor = Color.White;
                    }*/
                }
                else // BandNumber = 0
                {
                    int number = 0;
                    BandNumber.Text = number.ToString();
                    BandNumber_KeyUp(null, new KeyEventArgs(Keys.Enter));
                    //Seven(0x0F, Convert.ToByte(number), true); //Установка номера полосы приема УРП с параметрами
                    FreqLabel.Text = "F = " + (Convert.ToInt32(BandNumber.Text) * 30 + 40).ToString() + " MHz";
                }
                if (EventLevelSignal != null)
                {
                    EventLevelSignal();
                }
            }
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            dataGridView.Rows.Clear();
        }

        private void LOHeter1_StateChanged(object sender, NationalInstruments.UI.ActionEventArgs e)
        {
            if (heter1 == true)
            {
                if (numberURP >= 0)
                {
                    int state_LO1 = Convert.ToInt16(LOHeter1.Value);
                    Command.SwitchLO command = new Command.SwitchLO(Convert.ToByte(numberURP), 0x0C,
                        Convert.ToByte(state_LO1));
                    byte[] mess = SdS.Serialization(command);
                    byte[] answer = MessPort(mess, true);
                    heter1 = true;
                    if (answer[3] != mess[3])
                    {
                        heter1 = false;
                        LOHeter1.Value = !Convert.ToBoolean(state_LO1);
                    }
                }
                else
                {
                    MessageBox.Show("Необходимо выбрать номер УРП. Режим пеленгования НЕ выбирать(УРП1...УРП6)");
                    NumberURP.Focus();
                }
            }
            else
            {
                heter1 = true;
            }
        }

        private void LO1_ATT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void LO1_ATT_TextChanged(object sender, EventArgs e)
        {
            LO1_ATT.BackColor = Color.Aquamarine;
        }

        private void LO1_ATT_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (numberURP == 0)
                {
                    numberURP = 1;
                }
                KeyUpATT(LO1_ATT, LO1_SignalLevel);
                numberURP = NumberURP.SelectedIndex;
            }
        }

        private void PlusLO1_Click(object sender, EventArgs e)
        {
            if (numberURP == 0) 
            {
                numberURP = 1;
            }
            PlusATTLO1(LO1_ATT, LO1_SignalLevel);
            numberURP = NumberURP.SelectedIndex;
        }

        private void MinusLO1_Click(object sender, EventArgs e)
        {
            if (numberURP == 0)
            {
                numberURP = 1;
            }
            MinusATTLO1(LO1_ATT, LO1_SignalLevel);
            numberURP = NumberURP.SelectedIndex;
        }

        private void ATTBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void ATTBox1_TextChanged(object sender, EventArgs e)
        {
            ATTBox1.BackColor = Color.Aquamarine;
        }

        private void ATTBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (numberURP >= 0)
                {
                    if (flag == true)
                    {
                        if ((ATTBox1.Text != "") && (Convert.ToInt32(ATTBox1.Text) >= 0) &&
                            (Convert.ToInt32(ATTBox1.Text) <= 31))
                        {
                            int ATT = Convert.ToInt32(ATTBox1.Text);
                            Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                                Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                ATTBox1.BackColor = Color.White; 
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                        else
                        {
                            ATTBox1.Clear();
                        }
                    }
                    else
                    {
                        ATTBox1.Clear();
                    }
                }
                else
                {
                    MessageBox.Show("Необходимо выбрать номер УРП");
                    NumberURP.Focus();
                }

            }
        }

        private void PlusATT1_Click(object sender, EventArgs e)
        {
            if (numberURP >= 0)
            {
                if (flag == true)
                {
                    if (ATTBox1.Text != "")
                    {
                        if ((Convert.ToInt32(ATTBox1.Text) >= 0) && (Convert.ToInt32(ATTBox1.Text) < 31))
                        {
                            int ATT = Convert.ToInt32(ATTBox1.Text) + 1;
                            ATTBox1.Text = ATT.ToString();
                            Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                                Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                ATTBox1.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                        else //ATT=31
                        {
                            int ATT = 31;
                            ATTBox1.Text = ATT.ToString();
                            Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                                Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                ATTBox1.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                    }
                    else
                    {
                        int ATT = 0;
                        ATTBox1.Text = ATT.ToString();
                        byte[] mess = new byte[4] { 0x04, 0x01, 0x07, Convert.ToByte(ATT * 2) };
                        //команда по управлению атт в канале (№9/дБ*2)
                        byte[] answer = MessPort(mess, true);
                        if (answer[3] == mess[3])
                        {
                            ATTBox1.BackColor = Color.White;
                            if (EventLevelSignal != null)
                            {
                                EventLevelSignal();
                            }
                        }
                    }
                }
                else
                {
                    ATTBox1.Clear();
                }
            }
            else
            {
                MessageBox.Show("Необходимо выбрать номер УРП");
                NumberURP.Focus();
            }
        }

        private void MinusATT1_Click(object sender, EventArgs e)
        {
            if (numberURP >= 0)
            {
                if (flag == true)
                {
                    if (ATTBox1.Text != "")
                    {
                        if ((Convert.ToInt32(ATTBox1.Text) > 0) && (Convert.ToInt32(ATTBox1.Text) <= 31))
                        {
                            int ATT = Convert.ToInt32(ATTBox1.Text) - 1;
                            ATTBox1.Text = ATT.ToString();
                            Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                                Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                ATTBox1.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                        else //ATT=0
                        {
                            int ATT = 0;
                            ATTBox1.Text = ATT.ToString();
                            Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                                Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                ATTBox1.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                    }
                    else
                    {
                        int ATT = 31;
                        ATTBox1.Text = ATT.ToString();
                        Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                            Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, true);
                        if (answer[3] == mess[3])
                        {
                            ATTBox1.BackColor = Color.White;
                            if (EventLevelSignal != null)
                            {
                                EventLevelSignal();
                            }
                        }
                    }
                }
                else
                {
                    ATTBox1.Clear();
                }
            }
            else
            {
                MessageBox.Show("Необходимо выбрать номер УРП");
                NumberURP.Focus();
            }
        }

        private void ATTBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void ATTBox2_TextChanged(object sender, EventArgs e)
        {
            ATTBox2.BackColor = Color.Aquamarine;
        }

        private void ATTBox2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (numberURP >= 0)
                {
                    if (flag == false)
                    {
                        if ((ATTBox2.Text != "") && (Convert.ToInt32(ATTBox2.Text) >= 0) &&
                            (Convert.ToInt32(ATTBox2.Text) <= 31))
                        {
                            int ATT = Convert.ToInt32(ATTBox2.Text);
                            Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                                Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                ATTBox2.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                        else
                        {
                            ATTBox2.Clear();
                        }
                    }
                    else
                    {
                        ATTBox2.Clear();
                    }
                }
                else
                {
                    MessageBox.Show("Необходимо выбрать номер УРП");
                    NumberURP.Focus();
                }
            }
        }

        private void PlusATT2_Click(object sender, EventArgs e)
        {
            if (numberURP >= 0)
            {
                if (flag == false)
                {
                    if (ATTBox2.Text != "")
                    {
                        if ((Convert.ToInt32(ATTBox2.Text) >= 0) && (Convert.ToInt32(ATTBox2.Text) < 31))
                        {
                            int ATT = Convert.ToInt32(ATTBox2.Text) + 1;
                            ATTBox2.Text = ATT.ToString();
                            Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                                Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                ATTBox2.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                        else //ATT=31
                        {
                            int ATT = 31;
                            ATTBox2.Text = ATT.ToString();
                            Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                                Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                ATTBox2.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                    }
                    else
                    {
                        int ATT = 0;
                        ATTBox2.Text = ATT.ToString();
                        Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                            Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, true);
                        if (answer[3] == mess[3])
                        {
                            ATTBox2.BackColor = Color.White;
                            if (EventLevelSignal != null)
                            {
                                EventLevelSignal();
                            }
                        }
                    }
                }
                else
                {
                    ATTBox2.Clear();
                }
            }
            else
            {
                MessageBox.Show("Необходимо выбрать номер УРП");
                NumberURP.Focus();
            }
        }

        private void MinusATT2_Click(object sender, EventArgs e)
        {
            if (numberURP >= 0)
            {
                if (flag == false)
                {
                    if (ATTBox2.Text != "")
                    {
                        if ((Convert.ToInt32(ATTBox2.Text) > 0) && (Convert.ToInt32(ATTBox2.Text) <= 31))
                        {
                            int ATT = Convert.ToInt32(ATTBox2.Text) - 1;
                            ATTBox2.Text = ATT.ToString();
                            Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                                Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                ATTBox2.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                        else //ATT=0
                        {
                            int ATT = 0;
                            ATTBox2.Text = ATT.ToString();
                            Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                                Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                ATTBox2.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                    }
                    else
                    {
                        int ATT = 31;
                        ATTBox2.Text = ATT.ToString();
                        Command.ManageATT command = new Command.ManageATT(Convert.ToByte(numberURP),
                            Convert.ToByte(ATT * 2)); //команда по управлению атт в канале (дБ*2)
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, true);
                        if (answer[3] == mess[3])
                        {
                            ATTBox2.BackColor = Color.White;
                            if (EventLevelSignal != null)
                            {
                                EventLevelSignal();
                            }
                        }
                    }
                }
                else
                {
                    ATTBox2.Clear();
                }
            }
            else
            {
                MessageBox.Show("Необходимо выбрать номер УРП");
                NumberURP.Focus();
            }
        }

        private void UNNBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (!e.KeyChar.Equals('-')))
            {
                e.Handled = true;
            }
        }


        private void LOHeter2_StateChanged(object sender, NationalInstruments.UI.ActionEventArgs e)
        {
            if (heter2 == true)
            {
                if (numberURP >= 0)
                {
                    int state_LO2 = Convert.ToInt16(LOHeter2.Value);
                    Command.SwitchLO command = new Command.SwitchLO(Convert.ToByte(numberURP), 0x0D,
                        Convert.ToByte(state_LO2));
                    byte[] mess = SdS.Serialization(command);
                    byte[] answer = MessPort(mess, true);
                    heter2 = true;
                    if (answer[3] != mess[3])
                    {
                        heter2 = false;
                        LOHeter2.Value = !Convert.ToBoolean(state_LO2);
                    }
                }
                else
                {
                    MessageBox.Show("Необходимо выбрать номер УРП. Режим пеленгования НЕ выбирать(УРП1...УРП6)");
                    NumberURP.Focus();//jkhl
                }
            }
            else
            {
                heter2 = true;
            }
        }

        private void UNNBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    if (numberURP >= 0)
                    {
                        if ((UNNBox.Text != "") && (Convert.ToInt32(UNNBox.Text) >= -12) &&
                            (Convert.ToInt32(UNNBox.Text) <= 22))
                        {
                            int UNN = Convert.ToInt32(UNNBox.Text);
                            Command.OverallGain command = new Command.OverallGain(Convert.ToByte(numberURP),
                                Convert.ToByte(Gain(true, UNN))); //команда регулировки общего усиления 
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                UNNBox.BackColor = Color.White;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Необходимо выбрать номер УРП");
                        NumberURP.Focus();
                    }
                }
                catch { UNNBox.Clear(); }
            }
        }

        private void UNNBox_TextChanged(object sender, EventArgs e)
        {
            UNNBox.BackColor = Color.Aquamarine;
        }

        private void PlusUUF_Click(object sender, EventArgs e)
        {
            if (numberURP >= 0)
            {
                try
                {
                    if (UNNBox.Text != "")
                    {

                        if ((Convert.ToInt32(UNNBox.Text) >= -12) && (Convert.ToInt32(UNNBox.Text) < 22))
                        {
                            int UNN = Convert.ToInt32(UNNBox.Text) + 1;
                            UNNBox.Text = UNN.ToString();
                            Command.OverallGain command = new Command.OverallGain(Convert.ToByte(numberURP),
                                Convert.ToByte(Gain(true, UNN))); //команда регулировки общего усиления 
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                UNNBox.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                        else //UNN = 22
                        {
                            int UNN = 22;
                            UNNBox.Text = UNN.ToString();
                            Command.OverallGain command = new Command.OverallGain(Convert.ToByte(numberURP), 
                             Convert.ToByte(Gain(true, UNN))); //команда регулировки общего усиления 
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                UNNBox.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                    }
                    else
                    {
                        int UNN = -12;
                        UNNBox.Text = UNN.ToString();
                        byte[] mess = new byte[4] { 0x04, 0x01, 0x0A, Convert.ToByte(Gain(true, UNN)) };
                        //команда регулировки общего усиления (№11)
                        byte[] answer = MessPort(mess, true);
                        if (answer[3] == mess[3])
                        {
                            UNNBox.BackColor = Color.White;
                            if (EventLevelSignal != null)
                            {
                                EventLevelSignal();
                            }
                        }

                    }
                }
                catch { UNNBox.Text = (22).ToString(); }

            }
            else
            {
                MessageBox.Show("Необходимо выбрать номер УРП");
                NumberURP.Focus();
            }
        }

        private void MinusUFF_Click(object sender, EventArgs e)
        {
            if (numberURP >= 0)
            {
                try
                {
                    if (UNNBox.Text != "")
                    {
                        if ((Convert.ToInt32(UNNBox.Text) > -12) && (Convert.ToInt32(UNNBox.Text) <= 22))
                        {
                            int UNN = Convert.ToInt32(UNNBox.Text) - 1;
                            UNNBox.Text = UNN.ToString();
                            Command.OverallGain command = new Command.OverallGain(Convert.ToByte(numberURP),
                                Convert.ToByte(Gain(true, UNN))); //команда регулировки общего усиления 
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                UNNBox.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                        else //UNN = -12
                        {
                            int UNN = -12;
                            UNNBox.Text = UNN.ToString();
                            Command.OverallGain command = new Command.OverallGain(Convert.ToByte(numberURP),
                            Convert.ToByte(Gain(true, UNN))); //команда регулировки общего усиления 
                            byte[] mess = SdS.Serialization(command);
                            byte[] answer = MessPort(mess, true);
                            if (answer[3] == mess[3])
                            {
                                UNNBox.BackColor = Color.White;
                                if (EventLevelSignal != null)
                                {
                                    EventLevelSignal();
                                }
                            }
                        }
                    }
                    else
                    {
                        int UNN = 22;
                        UNNBox.Text = UNN.ToString();
                        Command.OverallGain command = new Command.OverallGain(Convert.ToByte(numberURP),
                            Convert.ToByte(Gain(true, UNN))); //команда регулировки общего усиления 
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, true);
                        if (answer[3] == mess[3])
                        {
                            UNNBox.BackColor = Color.White;
                            if (EventLevelSignal != null)
                            {
                                EventLevelSignal();
                            }
                        }
                    }
                }
                catch { UNNBox.Text = (-12).ToString(); }
            }

            else
            {
                MessageBox.Show("Необходимо выбрать номер УРП");
                NumberURP.Focus();
            }
        }

        private void NumberURP_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            var list = sender as CheckedListBox;
            if (e.NewValue == CheckState.Checked)
            {
                foreach (int index in list.CheckedIndices)
                    if (index != e.Index)
                        list.SetItemChecked(index, false);
            }
        }
        void ShowLevelSignalSheme()
        {
            try
            {
                if (numberURP != 0)
                {
                    Command.ManageATTLO1 commandATT = new Command.ManageATTLO1(Convert.ToByte(numberURP), 0xFF);
                    byte[] messAtt = SdS.Serialization(commandATT);
                    byte[] answerAtt = MessPort(messAtt, false);// опрос сосотояния аттенюатора на  выходе 1-го гетеродина
                    LO1_ATT.Text = (answerAtt[3] / 2).ToString();
                    LO1_ATT.BackColor = Color.White;


                    Label[] labelLO1 = new[] { LO1_SignalLevel, LO1_SignalLevel2, LO1_SignalLevel3, LO1_SignalLevel4, LO1_SignalLevel5,
                    LO2_SignalLevel, LO2_SignalLevel2, LO2_SignalLevel3, LO2_SignalLevel4, LO2_SignalLevel5,
                    Level_PCH1, Level_PCH2, Level_PCH3, Level_PCH4, Level_PCH5 };
                    ClearLabel(labelLO1, "Pout ");
                    PCH_Peleng.Visible = false;
                    LO1_Peleng.Visible = false;
                    LO2_Peleng.Visible = false;
                    SetSignalLevel(LO1_SignalLevel, Convert.ToByte(numberURP), 0x39);
                    SetSignalLevel(LO2_SignalLevel, Convert.ToByte(numberURP), 0x3A);
                    timerThermometer.Enabled = true;
                    SetSignalLevel(Level_PCH1, Convert.ToByte(numberURP), 0x38);


                }
                else
                {
                    Command.ManageATTLO1 commandATT;// = new Command.ManageATTLO1(Convert.ToByte(numberURP), 0xFF);
                    TextBox[] LO1ATT = new[] { LO1_ATT, LO1_URP2, LO1_URP3, LO1_URP4, LO1_URP5 };

                    timerThermometer.Enabled = false;
                    PCH_Peleng.Visible = true;
                    LO1_Peleng.Visible = true;
                    LO2_Peleng.Visible = true;
                    Label[] labelLO1 = new[] { LO1_SignalLevel, LO1_SignalLevel2, LO1_SignalLevel3, LO1_SignalLevel4, LO1_SignalLevel5 };
                    Label[] labelLO2 = new[] { LO2_SignalLevel, LO2_SignalLevel2, LO2_SignalLevel3, LO2_SignalLevel4, LO2_SignalLevel5 };
                    Label[] labelPCH = new[] { Level_PCH1, Level_PCH2, Level_PCH3, Level_PCH4, Level_PCH5 };
                    for (int i = 0; i < labelLO1.Length; i++)
                    {
                        byte[] mess = new byte[7] {0x07, Convert.ToByte(i+1), 0,0,0,0,0};
                        byte[] answer = MessPort(mess, false);
                        commandATT = new Command.ManageATTLO1(Convert.ToByte(i + 1), 0xFF);
                        byte[] messAtt = SdS.Serialization(commandATT);
                        byte[] answerAtt = MessPort(messAtt, false);// опрос сосотояния аттенюатора на  выходе 1-го гетеродина
                        LO1ATT[i].Text = (answerAtt[3] / 2).ToString();
                        LO1ATT[i].BackColor = Color.White;

                        SetSignalLevel(labelLO1[i], Convert.ToByte(i + 1), 0x39);
                        SetSignalLevel(labelLO2[i], Convert.ToByte(i + 1), 0x3A);
                        SetSignalLevel(labelPCH[i], Convert.ToByte(i + 1), 0x38);
                    }

                }
            }
            catch { }
        }

        private void NumberURP_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                numberURP = NumberURP.SelectedIndex;
                if (numberURP > 0) //(numberURP != 0)
                {
                    Seven(0x00, Convert.ToByte(numberURP), true); //Установка ноера УРП

                    if (EventLevelSignal != null)
                    {
                        EventLevelSignal();
                    }
                    /*LO1_Peleng.Visible = false;
                    LO2_Peleng.Visible = false;
                    SetSignalLevel(LO1_SignalLevel, Convert.ToByte(numberURP ), 0x39);
                    SetSignalLevel(LO2_SignalLevel, Convert.ToByte(numberURP ), 0x3A);
                    timerThermometer.Enabled = true;
                    SetSignalLevel(Level_PCH1, Convert.ToByte(numberURP), 0x38);*/

                    Command.SetBandNumber command = new Command.SetBandNumber(Convert.ToByte(numberURP), Convert.ToByte(BandNumber.Text));
                    byte[] mess = SdS.Serialization(command);
                    byte[] answer = MessPort(mess, false); // настройк номера полосы

                    Command.ManageATTLO1 commandATT = new Command.ManageATTLO1(Convert.ToByte(numberURP), 0xFF);
                    byte[] messAtt = SdS.Serialization(commandATT);
                    byte[] answerAtt = MessPort(messAtt, false);// опрос сосотояния аттенюатора на  выходе 1-го гетеродина
                    LO1_ATT.Text = (answerAtt[3] / 2).ToString();
                    LO1_ATT.BackColor = Color.White;

                }
                else
                {
                    if (numberURP == 0)
                    {
                        Seven(0x00, Convert.ToByte(numberURP), true); //Установка ноера УРП

                        if (EventLevelSignal != null)
                        {
                            EventLevelSignal();
                        }
                        /*timerThermometer.Enabled = false;
                        LO1_Peleng.Visible = true;
                        LO2_Peleng.Visible = true;
                        Label[] labelLO1 = new[] { LO1_SignalLevel, LO1_SignalLevel2, LO1_SignalLevel3, LO1_SignalLevel4, LO1_SignalLevel5 };
                        Label[] labelLO2 = new[] { LO2_SignalLevel, LO2_SignalLevel2, LO2_SignalLevel3, LO2_SignalLevel4, LO2_SignalLevel5 };
                        for (int i = 0; i < labelLO1.Length; i++)
                        {
                            SetSignalLevel(labelLO1[i], Convert.ToByte(i + 1), 0x39);
                            SetSignalLevel(labelLO2[i], Convert.ToByte(i + 1), 0x3A);
                        }*/

                        Command.SetBandNumber command = new Command.SetBandNumber(Convert.ToByte(numberURP), Convert.ToByte(BandNumber.Text));
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, false); // настройк номера полосы
                        TextBox[] Boxes = new[] { LO1_ATT, LO1_URP2, LO1_URP3, LO1_URP4, LO1_URP5 };
                        for (int i = 0; i < Boxes.Length; i++)
                        {
                            Command.ManageATTLO1 commandATT = new Command.ManageATTLO1(Convert.ToByte(i+1), 0xFF);
                            byte[] messAtt = SdS.Serialization(commandATT);
                            byte[] answerAtt = MessPort(messAtt, false);// опрос сосотояния аттенюатора на  выходе 1-го гетеродина
                            Boxes[i].Text = (answerAtt[3] / 2).ToString();
                            Boxes[i].BackColor = Color.White;
 
                        }
                    }
                }
            }
            catch { }
        }


        public void SetSignalLevel(Label label, byte number, byte constValue)
        {
            Command.SignalLevel commandLO = new Command.SignalLevel(number, constValue);//запрос уровня сигнала 
            byte[] mess = SdS.Serialization(commandLO);
            byte[] answer = MessPort(mess, true);
            short signalLevel = BitConverter.ToInt16(answer, 3);
            double signalLevelLO;
            int numberBand = Convert.ToInt32(BandNumber.Text);
            switch (constValue)
            {
                case 0x39: //Уровень сигнала первого гетеродина
                    signalLevelLO = LevelLO1(signalLevel, numberBand);
                    break;
                case 0x3A: //Уровень сигнала второго гетеродина
                    signalLevelLO = LevelLO2(signalLevel);
                    break;
                default:
                    // 0x38 - Уровень сигнала на ПЧ-выходе
                    signalLevelLO = LevelPCH(signalLevel);
                    break;
            }
            label.Text = "Pout " + number.ToString() + "  " + signalLevelLO.ToString() + " dBm";
        }

        public void PlusATTLO1(TextBox box, Label label)
        {
            if (numberURP >= 1)
            {
                if (box.Text != "")
                {
                    if ((Convert.ToInt32(box.Text) >= 0) && (Convert.ToInt32(box.Text) < 31))
                    {
                        int ATT = Convert.ToInt32(box.Text) + 1;
                        box.Text = ATT.ToString();
                        Command.ManageATTLO1 command = new Command.ManageATTLO1(Convert.ToByte(numberURP),
                            Convert.ToByte(ATT * 2));
                        //команда по управлению атт на выходе 1-го гетеродина (№10/дБ*2)
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, true);
                        if (answer[3] == mess[3])
                        {
                            box.BackColor = Color.White;
                        }
                        SetSignalLevel(label, Convert.ToByte(numberURP), 0x39);
                    }
                    else //ATT=31
                    {
                        int ATT = 31;
                        box.Text = ATT.ToString();
                        Command.ManageATTLO1 command = new Command.ManageATTLO1(Convert.ToByte(numberURP),
                            Convert.ToByte(ATT * 2));
                        //команда по управлению атт на выходе 1-го гетеродина (№10/дБ*2)
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, true);
                        if (answer[3] == mess[3])
                        {
                            box.BackColor = Color.White;
                        }
                        SetSignalLevel(label, Convert.ToByte(numberURP), 0x39);
                    }
                }
                else
                {
                    int ATT = 0;
                    box.Text = ATT.ToString();
                    Command.ManageATTLO1 command = new Command.ManageATTLO1(Convert.ToByte(numberURP),
                        Convert.ToByte(ATT * 2));
                    //команда по управлению атт на выходе 1-го гетеродина (№10/дБ*2)
                    byte[] mess = SdS.Serialization(command);
                    byte[] answer = MessPort(mess, true);
                    if (answer[3] == mess[3])
                    {
                        box.BackColor = Color.White;
                    }
                    SetSignalLevel(label, Convert.ToByte(numberURP), 0x39);
                }
            }
            else
            {
                MessageBox.Show("Необходимо выбрать номер УРП. Режим пеленгования НЕ выбирать(УРП1...УРП6)");
                NumberURP.Focus();
            }
        }

        public void MinusATTLO1(TextBox box, Label label)
        {
            if (numberURP >= 1)
            {
                if (box.Text != "")
                {
                    if ((Convert.ToInt32(box.Text) > 0) && (Convert.ToInt32(box.Text) <= 31))
                    {
                        int ATT = Convert.ToInt32(box.Text) - 1;
                        box.Text = ATT.ToString();
                        Command.ManageATTLO1 command = new Command.ManageATTLO1(Convert.ToByte(numberURP),
                            Convert.ToByte(ATT * 2));
                        //команда по управлению атт на выходе 1-го гетеродина (№10/дБ*2)
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, true);
                        if (answer[3] == mess[3])
                        {
                            box.BackColor = Color.White;
                        }
                        SetSignalLevel(label, Convert.ToByte(numberURP), 0x39);

                    }
                    else //ATT=0
                    {
                        int ATT = 0;
                        box.Text = ATT.ToString();
                        Command.ManageATTLO1 command = new Command.ManageATTLO1(Convert.ToByte(numberURP),
                            Convert.ToByte(ATT * 2));
                        //команда по управлению атт на выходе 1-го гетеродина (№10/дБ*2)
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, true);
                        if (answer[3] == mess[3])
                        {
                            box.BackColor = Color.White;
                        }
                        SetSignalLevel(label, Convert.ToByte(numberURP), 0x39);
                    }
                }
                else
                {
                    int ATT = 31;
                    box.Text = ATT.ToString();
                    Command.ManageATTLO1 command = new Command.ManageATTLO1(Convert.ToByte(numberURP),
                        Convert.ToByte(ATT * 2));
                    //команда по управлению атт на выходе 1-го гетеродина (№10/дБ*2)
                    byte[] mess = SdS.Serialization(command);
                    byte[] answer = MessPort(mess, true);
                    if (answer[3] == mess[3])
                    {
                        box.BackColor = Color.White;
                    }
                    SetSignalLevel(label, Convert.ToByte(numberURP), 0x39);
                }
            }
            else
            {
                MessageBox.Show("Необходимо выбрать номер УРП. Режим пеленгования НЕ выбирать(УРП1...УРП6)");
                NumberURP.Focus();
            }
        }

        public void KeyUpATT(TextBox box, Label label)
        {

            if (numberURP >= 1)
            {
                if ((box.Text != "") && (Convert.ToInt32(box.Text) >= 0) &&
                    (Convert.ToInt32(box.Text) <= 31))
                {
                    int ATT = Convert.ToInt32(box.Text);
                    Command.ManageATTLO1 command = new Command.ManageATTLO1(Convert.ToByte(numberURP), Convert.ToByte(ATT * 2));
                    //команда по управлению атт на выходе 1-го гетеродина (НЕактуальна для работы в режиме пеленгования )
                    byte[] mess = SdS.Serialization(command);
                    byte[] answer = MessPort(mess, true);
                    if (answer[3] == mess[3])
                    {
                        box.BackColor = Color.White;
                    }
                    SetSignalLevel(label, Convert.ToByte(numberURP), 0x39);
                }
            }
            else
            {
                MessageBox.Show("Необходимо выбрать номер УРП. Режим пеленгования НЕ выбирать(УРП1...УРП6)");
                NumberURP.Focus();
            }
        }

        private void PlusLO1_URP2_Click(object sender, EventArgs e)
        {
            if (numberURP == 0)
            {
                numberURP = 2;
            }
            PlusATTLO1(LO1_URP2, LO1_SignalLevel2);
            numberURP = NumberURP.SelectedIndex; 
        }

        private void PlusLO1_URP3_Click(object sender, EventArgs e)
        {
            if (numberURP == 0)
            {
                numberURP = 3;
            }
            PlusATTLO1(LO1_URP3, LO1_SignalLevel3);
            numberURP = NumberURP.SelectedIndex;
        }

        private void PlusLO1_URP4_Click(object sender, EventArgs e)
        {
            if (numberURP == 0)
            {
                numberURP = 4;
            }
            PlusATTLO1(LO1_URP4, LO1_SignalLevel4);
            numberURP = NumberURP.SelectedIndex;
        }

        private void PlusLO1_URP5_Click(object sender, EventArgs e)
        {
            if (numberURP == 0)
            {
                numberURP = 5;
            }
            PlusATTLO1(LO1_URP5, LO1_SignalLevel5);
            numberURP = NumberURP.SelectedIndex;
        }

        private void MinusLO1_URP2_Click(object sender, EventArgs e)
        {
            if (numberURP == 0)
            {
                numberURP = 2;
            }
            MinusATTLO1(LO1_URP2, LO1_SignalLevel2);
            numberURP = NumberURP.SelectedIndex; 
        }

        private void MinusLO1_URP3_Click(object sender, EventArgs e)
        {
            if (numberURP == 0)
            {
                numberURP = 3;
            }
            MinusATTLO1(LO1_URP3, LO1_SignalLevel3);
            numberURP = NumberURP.SelectedIndex;
        }

        private void MinusLO1_URP4_Click(object sender, EventArgs e)
        {
            if (numberURP == 0)
            {
                numberURP = 4;
            }
            MinusATTLO1(LO1_URP4, LO1_SignalLevel4);
            numberURP = NumberURP.SelectedIndex;
        }

        private void MinusLO1_URP5_Click(object sender, EventArgs e)
        {
            if (numberURP == 0)
            {
                numberURP = 5;
            }
            MinusATTLO1(LO1_URP5, LO1_SignalLevel5);
            numberURP = NumberURP.SelectedIndex;
        }

        private void LO1_URP2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void LO1_URP3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void LO1_URP4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void LO1_URP5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void LO1_URP2_TextChanged(object sender, EventArgs e)
        {
            LO1_URP2.BackColor = Color.Aquamarine;
        }

        private void LO1_URP3_TextChanged(object sender, EventArgs e)
        {
            LO1_URP3.BackColor = Color.Aquamarine;
        }

        private void LO1_URP4_TextChanged(object sender, EventArgs e)
        {
            LO1_URP4.BackColor = Color.Aquamarine;
        }

        private void LO1_URP5_TextChanged(object sender, EventArgs e)
        {
            LO1_URP5.BackColor = Color.Aquamarine;
        }

        private void LO1_URP2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (numberURP == 0)
                {
                    numberURP = 2;
                }
                KeyUpATT(LO1_URP2, LO1_SignalLevel2);
                numberURP = NumberURP.SelectedIndex;
            }
        }

        private void LO1_URP3_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (numberURP == 0)
                {
                    numberURP = 3;
                }
                KeyUpATT(LO1_URP3, LO1_SignalLevel3);
                numberURP = NumberURP.SelectedIndex;
            }
        }

        private void LO1_URP4_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (numberURP == 0)
                {
                    numberURP = 4;
                }
                KeyUpATT(LO1_URP4, LO1_SignalLevel4);
                numberURP = NumberURP.SelectedIndex;
            }
        }

        private void LO1_URP5_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (numberURP == 0)
                {
                    numberURP = 5;
                }
                KeyUpATT(LO1_URP5, LO1_SignalLevel5);
                numberURP = NumberURP.SelectedIndex;
            }
        }

        public bool IndependetMode() //Независимый режим калибровки
        {
            try
            {
                byte[] bandNumber = new byte[100];
                for (byte i = 0; i < bandNumber.Length; i++) bandNumber[i] = i;
                foreach (var number in bandNumber)
                {
                    int rowIndex = DataIndependet.Rows.Add();
                    DataIndependet.Rows[rowIndex].Cells[0].Value = number;
                    DataIndependet.Rows[rowIndex].Cells[1].Value = 30 * Convert.ToInt16(number) + 40;
                    int index = 2;
                    for (byte yy = 0x01; yy <= 0x06; yy++)
                    {
                        Command.SetBandNumber command = new Command.SetBandNumber(yy, number);
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, false); // настройка номера полосы

                        // Thread.Sleep(5);

                        Command.CalibLO1 calib = new Command.CalibLO1(yy);
                        mess = SdS.Serialization(calib);
                        /////*******************///////////////////

                        if (portCOM != null)
                        {
                            portCOM.ReadExisting();
                            portCOM.Write(mess, 0, mess.Length);// Калибровка 1-го гетеродина на текущей полосе приема
                        }
                        else
                        {
                            portSPI.DataChannel.Write(mess, 0, mess.Length);
                        }
                        Thread.Sleep(DelayCalib);
                        answer = new byte[mess.Length];
                        if (portCOM != null)
                        {
                            portCOM.Read(answer, 0, answer.Length);
                        }
                        else
                        {
                            portSPI.DataChannel.Read(answer, 0, answer.Length);
                        }
                        if (EvEnLOG != null)
                        {
                            EvEnLOG(answer);
                        }
                        int Index = dataGridView.Rows.Add();
                        string answerS = "", mesS = "";
                        for (int i = 0; i < mess.Length; i++)
                        {
                            answerS += " " + answer[i].ToString("X2");
                            mesS += " " + mess[i].ToString("X2");
                        }
                        dataGridView.Rows[Index].Cells[0].Value = mesS;
                        dataGridView.Rows[Index].Cells[1].Value = answerS;

                        //Отображение последнего добавленного элемнта
                        dataGridView.FirstDisplayedScrollingRowIndex = dataGridView.RowCount - 1;

                        dataGridView.Rows[rowIndex].Cells[0].Style.BackColor = Color.Aqua;
                        dataGridView.Rows[rowIndex].Cells[1].Style.BackColor = Color.Aqua;
                        ////**********************///////////////////

                        bool SuccesFlag = !Convert.ToBoolean(answer[3]);
                        // хх = 00 - калибровка прошла успешно/ хх = 01 - ошибка

                        Command.SignalLevel level = new Command.SignalLevel(yy, 0x39);
                        mess = SdS.Serialization(level);
                        answer = MessPort(mess, false); // Запрос уровня сигнала первого гетеродина

                        short levelB = BitConverter.ToInt16(answer, 3);
                        double levl = LevelLO1(levelB, number);
                        DataIndependet.Rows[rowIndex].Cells[index].Value = levl;
                        if (number >= 0 && number < 33)
                        {
                            if (levl < Convert.ToDouble(LO1_FirstNormStart.Value) || levl > Convert.ToDouble(LO1_FirstNormEnd.Value))
                            {
                                DataIndependet.Rows[rowIndex].Cells[index].Style.BackColor = Color.Red;
                            }
                        }
                        else// >= 33 
                        {
                            if (levl < Convert.ToDouble(LO1_SecondNormStart.Value) || levl > Convert.ToDouble(LO1_SecondNormEnd.Value))
                            {
                                DataIndependet.Rows[rowIndex].Cells[index].Style.BackColor = Color.Red;
                            }

                        }
                        index++;
                    }
                    DataIndependet.FirstDisplayedScrollingRowIndex = DataIndependet.RowCount - 1;
                    // прокрутка до последней строки
                }
                return true;
            }
            catch
            {
                return false;
            }

        }

        public bool CoherentMode() //Когерентный режим калибровки
        {
            try
            {
                byte[] bandNumber = new byte[100];
                for (byte i = 0; i < bandNumber.Length; i++) bandNumber[i] = i;
                foreach (var number in bandNumber)
                {
                    Command.SetBandNumber command = new Command.SetBandNumber(0x00, number);
                    byte[] mess = SdS.Serialization(command);
                    MessPort(mess, false); // настройк номера полосы

                    // Thread.Sleep(5);

                    int rowIndex = DataCoherent.Rows.Add();
                    DataCoherent.Rows[rowIndex].Cells[0].Value = number;
                    DataCoherent.Rows[rowIndex].Cells[1].Value = 30 * Convert.ToInt16(number) + 40;
                    int index = 2;
                    for (byte yy = 0x01; yy <= 0x05; yy++)
                    {
                        Command.CalibLO1 calib = new Command.CalibLO1(yy);
                        mess = SdS.Serialization(calib);
                        /////*******************///////////////////
                        /*portCOM.ReadExisting();
                        portCOM.Write(mess, 0, mess.Length);// Калибровка 1-го гетеродина на текущей полосе приема
                        Thread.Sleep(25);
                        byte[] answer = new byte[mess.Length];
                        portCOM.Read(answer, 0, answer.Length);*/
                        if (portCOM != null)
                        {
                            portCOM.ReadExisting();
                            portCOM.Write(mess, 0, mess.Length);// Калибровка 1-го гетеродина на текущей полосе приема
                        }
                        else
                        {
                            portSPI.DataChannel.Write(mess, 0, mess.Length);
                        }
                        Thread.Sleep(DelayCalib);
                        byte[] answer = new byte[mess.Length];
                        if (portCOM != null)
                        {
                            portCOM.Read(answer, 0, answer.Length);
                        }
                        else
                        {
                            portSPI.DataChannel.Read(answer, 0, answer.Length);
                        }

                        if (EvEnLOG != null)
                        {
                            EvEnLOG(answer);
                        }
                        int Index = dataGridView.Rows.Add();
                        string answerS = "", mesS = "";
                        for (int i = 0; i < mess.Length; i++)
                        {
                            answerS += " " + answer[i].ToString("X2");
                            mesS += " " + mess[i].ToString("X2");
                        }
                        dataGridView.Rows[Index].Cells[0].Value = mesS;
                        dataGridView.Rows[Index].Cells[1].Value = answerS;

                        //Отображение последнего добавленного элемнта
                        dataGridView.FirstDisplayedScrollingRowIndex = dataGridView.RowCount - 1;

                        dataGridView.Rows[rowIndex].Cells[0].Style.BackColor = Color.Aqua;
                        dataGridView.Rows[rowIndex].Cells[1].Style.BackColor = Color.Aqua;
                        ////**********************///////////////////

                        bool SuccesFlag = Convert.ToBoolean(answer[3]);
                        // хх = 00 - калибровка прошла успешно/ хх = 01 - ошибка

                        Command.SignalLevel level = new Command.SignalLevel(yy, 0x39);
                        mess = SdS.Serialization(level);
                        answer = MessPort(mess, false); // Запрос уровня сигнала первого гетеродина

                        short levelB = BitConverter.ToInt16(answer, 3);
                        double levl = LevelLO1(levelB, number);//LevelLO2(levelB);
                        DataCoherent.Rows[rowIndex].Cells[index].Value = levl;
                        if (number >= 0 && number < 33)
                        {
                            if (levl < Convert.ToDouble(LO1_FirstNormStart.Value) || levl > Convert.ToDouble(LO1_FirstNormEnd.Value))
                            {
                                DataCoherent.Rows[rowIndex].Cells[index].Style.BackColor = Color.Red;
                            }
                        }
                        else// >= 33 
                        {
                            if (levl < Convert.ToDouble(LO1_SecondNormStart.Value) || levl > Convert.ToDouble(LO1_SecondNormEnd.Value))
                            {
                                DataCoherent.Rows[rowIndex].Cells[index].Style.BackColor = Color.Red;
                            }

                        }
                        index++;
                    }
                    DataCoherent.FirstDisplayedScrollingRowIndex = DataCoherent.RowCount - 1;
                    // прокрутка до последней строки
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void ButCalibInd_Click(object sender, EventArgs e)
        {
            int number;
            if ((BandNumber.Text != "") && BandNumber.BackColor == Color.White)
            {
                number = Convert.ToInt32(BandNumber.Text);
            }
            else
            {
                number = 0;
            }

            ButCalibInd.BackColor = Color.Crimson;
            this.Enabled = false;
            bool flagCalib = IndependetMode();
            if (flagCalib == true)
            {
                ButCalibInd.BackColor = Color.Green;
                this.Enabled = true;

                if (EventLevelSignal != null)
                {
                    EventLevelSignal();
                }
            }
            else
            {
                MessageBox.Show("Произошла ошибка при калибровке");

                this.Enabled = true;
            }
            if (numberURP >= 0)
            {
                Command.SetBandNumber command = new Command.SetBandNumber(Convert.ToByte(numberURP), Convert.ToByte(number));
                byte[] mess = SdS.Serialization(command);
                MessPort(mess, true);
            }
        }

        private void ButCalibCoh_Click(object sender, EventArgs e)
        {
            int number;
            if ((BandNumber.Text != "") && BandNumber.BackColor == Color.White)
            {
                number = Convert.ToInt32(BandNumber.Text);
            }
            else
            {
                number = 0;
            }

            ButCalibCoh.BackColor = Color.Crimson;
            this.Enabled = false;
            bool flagCalib = CoherentMode();
            if (flagCalib == true)
            {
                ButCalibCoh.BackColor = Color.Green;
                this.Enabled = true;

                if (EventLevelSignal != null)
                {
                    EventLevelSignal();
                }
            }
            else
            {
                MessageBox.Show("Произошла ошибка при калибровке");

                this.Enabled = true;
            }

            if (numberURP >= 0)
            {
                Command.SetBandNumber command = new Command.SetBandNumber(Convert.ToByte(numberURP), Convert.ToByte(number));
                byte[] mess = SdS.Serialization(command);
                MessPort(mess, true);
            }
        }

        private void ButDelInd_Click(object sender, EventArgs e)
        {
            DataIndependet.Rows.Clear();
        }

        private void ButDelCoh_Click(object sender, EventArgs e)
        {
            DataCoherent.Rows.Clear();
        }

        public double LevelLO1(short x, int numberBand)
        {
            int Fn = numberBand * 30 + 40;
            int Fg;
            if (Fn <= 1015)
            {
                Fg = Fn + 1500;
            }
            else// Fn > 1015
            {
                Fg = Fn + 704;
            }
            double A = Math.Log10(2 * x / 4095f);
            double B = -0.0014 * Fg;
            double level = ((22.42 * A - 1.527) + 10 + (B + 16.59) - 1);
            return Math.Round(level, 4);
        }

        public double LevelLO2(short x)
        {
            double A = Math.Log10(2 * x / 4095f);
            double level = 22.42 * A - 1.527 + 10 + 15;
            return Math.Round(level, 4);
        }

        public double LevelPCH(short x)
        {
            double coef_1 = 20.783; // 22.42
            double coef_2 = 3.403; // 1.527
            double A = Math.Log10(2 * x / 4095f);
            double level = coef_1 * A - coef_2;
            return Math.Round(level, 4);
        }

        private void ButSavInd_Click(object sender, EventArgs e)
        {
            SaveTxt(DataIndependet);
        }


        private void ButSavCoh_Click(object sender, EventArgs e)
        {
            SaveTxt(DataCoherent);
        }
        public void SaveTxt(DataGridView dataGrid)
        {
            //Save

            Stream myStream;

            saveIndependentMode.Filter = "Text Files | *.txt";
            saveIndependentMode.FilterIndex = 2;
            saveIndependentMode.Title = "Сохранить";
            saveIndependentMode.DefaultExt = "txt";
            saveIndependentMode.RestoreDirectory = true;
            saveIndependentMode.RestoreDirectory = true;
            int rowIndex = dataGrid.RowCount - 1;

            if (saveIndependentMode.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveIndependentMode.OpenFile()) != null)
                {
                    StreamWriter myWritet = new StreamWriter(myStream);
                    try
                    {
                        for (int i = 0; i < rowIndex; i++)
                        {
                            for (int j = 0; j < dataGrid.ColumnCount; j++)
                            {
                                myWritet.Write(dataGrid.Rows[i].Cells[j].Value.ToString() + " ");
                            }
                            myWritet.WriteLine();
                        }

                    }
                    catch (Exception)
                    {
                        //MessageBox.Show(ex.Message);
                        throw;
                    }
                    finally
                    {
                        myWritet.Close();
                    }

                    myStream.Close();
                }
            }
        }

        private void timerThermometer_Tick(object sender, EventArgs e)
        {
            if (Temperatur.Checked == true)
            {
                Command.ModuleTemper command = new Command.ModuleTemper(Convert.ToByte(numberURP));
                byte[] mess = SdS.Serialization(command);
                byte[] answer = MessPort(mess, true);

                Thermometer.Value = BitConverter.ToInt16(answer, 3);
            }
        }
        struct NewCommad
        {
            public string name;
            public string command;
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            this.panel21.Controls.Clear();

            //Load
            Stream myStream = null;
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = openFileDialog1.OpenFile()) != null)
                {
                    StreamReader myReader = new StreamReader(myStream, System.Text.Encoding.Default);

                    //string[] str;
                    int num = 0;
                    try
                    {
                        string[] str1 =  myReader.ReadToEnd().Split('\r');
                        num = str1.Count();
                        int count = (int)(num / 2);
                        NewCommad[] newCommand = new NewCommad[count];
                        int j = 0;
                        string subString = "\n";
                        int indexOfSubstring;
                        for (int i = 0; i < count; i++)
                        {
                            indexOfSubstring = str1[j].IndexOf(subString);
                            if (indexOfSubstring >= 0) { str1[j] = str1[j].Substring(indexOfSubstring + subString.Length); }
                            newCommand[i].name = str1[j];
                            j++;
                            newCommand[i].command = str1[j];
                            j++;
                        }
                        LabelPanel = new System.Windows.Forms.Label[newCommand.Length];
                        Textbox = new System.Windows.Forms.TextBox[newCommand.Length];
                        BuTTon = new System.Windows.Forms.Button[newCommand.Length];
                        this.SuspendLayout();
                        const int Location_X = 7;
                        int LocLabel_Y_Start = 10;
                        int LocLabel_Y = LocLabel_Y_Start;
                        int DeltaLabel_Box_Y = 15;//30;
                        const int LocButton_X = 300;
                        const int DelLocation_Y = 40;//70;
                        ///Propet///
                        for (int i = 0; i < LabelPanel.Length; i++)
                        {
                            LabelPanel[i] = new System.Windows.Forms.Label();
                            Textbox[i] = new System.Windows.Forms.TextBox();
                            BuTTon[i] = new System.Windows.Forms.Button();

                            //Location///
                            LabelPanel[i].Location = new System.Drawing.Point(Location_X, LocLabel_Y);
                            Textbox[i].Location = new System.Drawing.Point(Location_X, LocLabel_Y + DeltaLabel_Box_Y);
                            BuTTon[i].Location = new System.Drawing.Point(LocButton_X, LocLabel_Y + DeltaLabel_Box_Y);
                            LocLabel_Y += DelLocation_Y;

                            //Font///
                            LabelPanel[i].Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                            Textbox[i].Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
                            BuTTon[i].Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);

                            //Size///
                            Textbox[i].Size = new System.Drawing.Size(200, 26);
                            BuTTon[i].Size = new System.Drawing.Size(123, 29);


                            //Button///
                            BuTTon[i].Text = ">>>>";
                            BuTTon[i].Click += new EventHandler(this.EnterCommand);
                            BuTTon[i].Name = "B" + i.ToString();

                            //Label//
                            LabelPanel[i].Text = newCommand[i].name;
                            LabelPanel[i].Name = "L" + i.ToString();
                            LabelPanel[i].AutoSize = true;

                            //TextBox///
                            Textbox[i].Text = newCommand[i].command;
                            Textbox[i].Name = "T" + i.ToString();

                            ///Добавить подписи для Лэйблов, команды в Боксы

                            panel21.Controls.Add(LabelPanel[i]);
                            panel21.Controls.Add(Textbox[i]);
                            panel21.Controls.Add(BuTTon[i]);
                            
                        }
                        this.ResumeLayout();
                        
                    }
                    catch (Exception)
                    {
                        //MessageBox.Show(ex.Message);
                        throw;
                    }
                    finally
                    {
                        myReader.Close();
                    }
                }
            }

        }
        void EnterCommand(object sender, EventArgs e)
        {
            string name = ((System.Windows.Forms.Control)(sender)).Name;
            int index = Convert.ToInt16(name.Substring(1));
            string text = Textbox[index].Text;
            string subString = "\n";
            int indexOfSubstring;
            string[] comman1;

            //////////////////// Преобразуем в байты/////
            indexOfSubstring = text.IndexOf(subString);
            if (indexOfSubstring >= 0) { text = text.Substring(indexOfSubstring + subString.Length); } // удаляем Enter

            /*comman1 = new string[(int)(text.Length / 2)];
            for (int i = 0; i < comman1.Length; i++) 
            {
                comman1[i] = text.Substring(0, 2);
                text = text.Substring(2);
            }*/
            comman1 = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            byte[] CodeMes = new byte[comman1.Length];
            text = "";
            for (int i = 0; i < comman1.Length; i++)
            {
                CodeMes[i] = Convert.ToByte((comman1[i]), 16);
                text += CodeMes[i].ToString("X2") + " ";
            }
            /////////////////////////////////////////////
            textBox1.SelectionColor = Color.Red;
            textBox1.AppendText(text);
            textBox1.AppendText("\n");
            textBox1.ScrollToCaret();

            //////////////////Отправка байт/////////////
            try
            {
                byte[] answer = MessPort(CodeMes, true);
                text = "";
                for (int i = 0; i < answer.Length; i++)
                {
                    text += answer[i].ToString("X2") + " ";
                }
                textBox1.SelectionColor = Color.Black;
                textBox1.AppendText(text);
                textBox1.AppendText("\n");
                textBox1.ScrollToCaret();
            }
            catch
            {
            }

        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

       /* public bool IndependetModeLO2() //Независимый режим калибровки
        {
            try
            {
                byte[] bandNumber = new byte[100];
                for (byte i = 0; i < bandNumber.Length; i++) bandNumber[i] = i;
                foreach (var number in bandNumber)
                {
                    int rowIndex = DataIndependetLO2.Rows.Add();
                    DataIndependetLO2.Rows[rowIndex].Cells[0].Value = number;
                    DataIndependetLO2.Rows[rowIndex].Cells[1].Value = 30 * Convert.ToInt16(number) + 40;
                    int index = 2;
                    for (byte yy = 0x01; yy <= 0x06; yy++)
                    {
                        Command.SetBandNumber command = new Command.SetBandNumber(yy, number);
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, false); // настройк номера полосы

                        // Thread.Sleep(5);

                        Command.SignalLevel calib = new Command.SignalLevel(yy, 0x3A);
                        mess = SdS.Serialization(calib);
                        ////////////////////////
                        port.ReadExisting();
                        port.Write(mess, 0, mess.Length);// Запрос уровня сигнала 2-го гетеродина на текущей полосе приема
                        Thread.Sleep(10);
                        answer = new byte[mess.Length];
                        port.Read(answer, 0, answer.Length);
                        int Index = dataGridView.Rows.Add();
                        string answerS = "", mesS = "";
                        for (int i = 0; i < mess.Length; i++)
                        {
                            answerS += " " + answer[i].ToString("X2");
                            mesS += " " + mess[i].ToString("X2");
                        }
                        dataGridView.Rows[Index].Cells[0].Value = mesS;
                        dataGridView.Rows[Index].Cells[1].Value = answerS;

                        //Отображение последнего добавленного элемнта
                        dataGridView.FirstDisplayedScrollingRowIndex = dataGridView.RowCount - 1;

                        dataGridView.Rows[rowIndex].Cells[0].Style.BackColor = Color.Aqua;
                        dataGridView.Rows[rowIndex].Cells[1].Style.BackColor = Color.Aqua;
                        ///////////////////////


                        short levelB = BitConverter.ToInt16(answer, 3);
                        DataIndependetLO2.Rows[rowIndex].Cells[index].Value = LevelLO2(levelB);
                        index++;
                    }
                    DataIndependetLO2.FirstDisplayedScrollingRowIndex = DataIndependetLO2.RowCount - 1;
                    // прокрутка до последней строки
                }
                return true;
            }
            catch
            {
                return false;
            }

        }*/

        public bool ReadLevel(DataGridView DGV, byte Code) //Независимый режим калибровки
        {
            try
            {
                byte[] bandNumber = new byte[100];
                for (byte i = 0; i < bandNumber.Length; i++) bandNumber[i] = i;
                foreach (var number in bandNumber)
                {
                    int rowIndex = DGV.Rows.Add();
                    DGV.Rows[rowIndex].Cells[0].Value = number;
                    DGV.Rows[rowIndex].Cells[1].Value = 30 * Convert.ToInt16(number) + 40;
                    int index = 2;
                    for (byte yy = 0x01; yy <= 0x06; yy++)
                    {
                        Command.SetBandNumber command = new Command.SetBandNumber(yy, number);
                        byte[] mess = SdS.Serialization(command);
                        byte[] answer = MessPort(mess, false); // настройк номера полосы

                        // Thread.Sleep(5);

                        Command.SignalLevel calib = new Command.SignalLevel(yy, Code);
                        mess = SdS.Serialization(calib);
                        /////*******************///////////////////
                        /*portCOM.ReadExisting();
                        portCOM.Write(mess, 0, mess.Length);// Запрос уровня сигнала 2-го гетеродина на текущей полосе приема
                        Thread.Sleep(25);
                        answer = new byte[mess.Length];
                        portCOM.Read(answer, 0, answer.Length);*/

                        if (portCOM != null)
                        {
                            portCOM.ReadExisting();
                            portCOM.Write(mess, 0, mess.Length); // Запрос уровня сигнала 2-го гетеродина на текущей полосе приема
                        }
                        else
                        {
                            portSPI.DataChannel.Write(mess, 0, mess.Length);
                        }
                        Thread.Sleep(DelayRead);
                        answer = new byte[mess.Length];
                        if (portCOM != null)
                        {
                            portCOM.Read(answer, 0, answer.Length);
                        }
                        else
                        {
                            portSPI.DataChannel.Read(answer, 0, answer.Length);
                        }
                        if (EvEnLOG != null)
                        {
                            EvEnLOG(answer);
                        }
                        int Index = dataGridView.Rows.Add();
                        string answerS = "", mesS = "";
                        for (int i = 0; i < mess.Length; i++)
                        {
                            answerS += " " + answer[i].ToString("X2");
                            mesS += " " + mess[i].ToString("X2");
                        }
                        dataGridView.Rows[Index].Cells[0].Value = mesS;
                        dataGridView.Rows[Index].Cells[1].Value = answerS;

                        //Отображение последнего добавленного элемнта
                        dataGridView.FirstDisplayedScrollingRowIndex = dataGridView.RowCount - 1;

                        dataGridView.Rows[rowIndex].Cells[0].Style.BackColor = Color.Aqua;
                        dataGridView.Rows[rowIndex].Cells[1].Style.BackColor = Color.Aqua;
                        ////**********************///////////////////


                        short levelB = BitConverter.ToInt16(answer, 3);
                        switch (Code)
                        {
                            case 0x3A:
                                double levl = LevelLO2(levelB);
                                DGV.Rows[rowIndex].Cells[index].Value = levl;// LevelLO2(levelB);
                                if (levl < Convert.ToDouble(LO2_NormStart.Value) || levl > Convert.ToDouble(LO2_NormEnd.Value))
                                {
                                    DGV.Rows[rowIndex].Cells[index].Style.BackColor = Color.Red;
                                }
                                break;
                            case 0x39:
                                levl = LevelLO1(levelB, number);
                                DGV.Rows[rowIndex].Cells[index].Value = levl;
                                if (number >= 0 && number < 33)
                                {
                                    if (levl < Convert.ToDouble(LO1_FirstNormStart.Value) || levl > Convert.ToDouble(LO1_FirstNormEnd.Value))
                                    {
                                        DGV.Rows[rowIndex].Cells[index].Style.BackColor = Color.Red;
                                    }
                                }
                                else// >= 33 
                                {
                                    if (levl < Convert.ToDouble(LO1_SecondNormStart.Value) || levl > Convert.ToDouble(LO1_SecondNormEnd.Value))
                                    {
                                        DGV.Rows[rowIndex].Cells[index].Style.BackColor = Color.Red;
                                    }

                                }
                                break;
                        }
                        index++;
                    }
                    DGV.FirstDisplayedScrollingRowIndex = DGV.RowCount - 1;
                    // прокрутка до последней строки
                }
                return true;
            }
            catch
            {
                return false;
            }

        }

        /*public bool CoherentModeLO2() //Когерентный режим калибровки
        {
            try
            {
                byte[] bandNumber = new byte[100];
                for (byte i = 0; i < bandNumber.Length; i++) bandNumber[i] = i;
                foreach (var number in bandNumber)
                {
                    Command.SetBandNumber command = new Command.SetBandNumber(0x00, number);
                    byte[] mess = SdS.Serialization(command);
                    MessPort(mess, false); // настройк номера полосы

                    // Thread.Sleep(5);

                    int rowIndex = DataCoherentLO2.Rows.Add();
                    DataCoherentLO2.Rows[rowIndex].Cells[0].Value = number;
                    DataCoherentLO2.Rows[rowIndex].Cells[1].Value = 30 * Convert.ToInt16(number) + 40;
                    int index = 2;
                    for (byte yy = 0x01; yy <= 0x05; yy++)
                    {
                        Command.SignalLevel calib = new Command.SignalLevel(yy, 0x3A);
                        mess = SdS.Serialization(calib);
                        ////////////////////////
                        port.ReadExisting();
                        port.Write(mess, 0, mess.Length);// Запрос уровня сигнала 2-го гетеродина на текущей полосе приема
                        Thread.Sleep(10);
                        byte[] answer = new byte[mess.Length];
                        port.Read(answer, 0, answer.Length);
                        int Index = dataGridView.Rows.Add();
                        string answerS = "", mesS = "";
                        for (int i = 0; i < mess.Length; i++)
                        {
                            answerS += " " + answer[i].ToString("X2");
                            mesS += " " + mess[i].ToString("X2");
                        }
                        dataGridView.Rows[Index].Cells[0].Value = mesS;
                        dataGridView.Rows[Index].Cells[1].Value = answerS;

                        //Отображение последнего добавленного элемнта
                        dataGridView.FirstDisplayedScrollingRowIndex = dataGridView.RowCount - 1;

                        dataGridView.Rows[rowIndex].Cells[0].Style.BackColor = Color.Aqua;
                        dataGridView.Rows[rowIndex].Cells[1].Style.BackColor = Color.Aqua;
                        ///////////////////////


                        short levelB = BitConverter.ToInt16(answer, 3);

                        DataCoherentLO2.Rows[rowIndex].Cells[index].Value = LevelLO2(levelB);
                        index++;
                    }
                    DataCoherentLO2.FirstDisplayedScrollingRowIndex = DataCoherent.RowCount - 1;
                    // прокрутка до последней строки
                }
                return true;
            }
            catch
            {
                return false;
            }
        }*/

        public bool ReadLevelCoherentMode(DataGridView DGV, byte Code) //Когерентный режим калибровки
        {
            try
            {
                byte[] bandNumber = new byte[100];
                for (byte i = 0; i < bandNumber.Length; i++) bandNumber[i] = i;
                foreach (var number in bandNumber)
                {
                    Command.SetBandNumber command = new Command.SetBandNumber(0x00, number);
                    byte[] mess = SdS.Serialization(command);
                    MessPort(mess, false); // настройк номера полосы

                    // Thread.Sleep(5);

                    int rowIndex = DGV.Rows.Add();
                    DGV.Rows[rowIndex].Cells[0].Value = number;
                    DGV.Rows[rowIndex].Cells[1].Value = 30 * Convert.ToInt16(number) + 40;
                    int index = 2;
                    for (byte yy = 0x01; yy <= 0x05; yy++)
                    {
                        Command.SignalLevel calib = new Command.SignalLevel(yy, Code);
                        mess = SdS.Serialization(calib);
                        /////*******************///////////////////
                        /*portCOM.ReadExisting();
                        portCOM.Write(mess, 0, mess.Length);// Запрос уровня сигнала 2-го гетеродина на текущей полосе приема
                        Thread.Sleep(25);
                        answer = new byte[mess.Length];
                        portCOM.Read(answer, 0, answer.Length);*/

                        if (portCOM != null)
                        {
                            portCOM.ReadExisting();
                            portCOM.Write(mess, 0, mess.Length); // Запрос уровня сигнала 2-го гетеродина на текущей полосе приема
                        }
                        else
                        {
                            portSPI.DataChannel.Write(mess, 0, mess.Length);
                        }
                        Thread.Sleep(DelayRead);
                        byte[] answer = new byte[mess.Length];
                        if (portCOM != null)
                        {
                            portCOM.Read(answer, 0, answer.Length);
                        }
                        else
                        {
                            portSPI.DataChannel.Read(answer, 0, answer.Length);
                        }
                        if (EvEnLOG != null)
                        {
                            EvEnLOG(answer);
                        }
                        int Index = dataGridView.Rows.Add();
                        string answerS = "", mesS = "";
                        for (int i = 0; i < mess.Length; i++)
                        {
                            answerS += " " + answer[i].ToString("X2");
                            mesS += " " + mess[i].ToString("X2");
                        }
                        dataGridView.Rows[Index].Cells[0].Value = mesS;
                        dataGridView.Rows[Index].Cells[1].Value = answerS;

                        //Отображение последнего добавленного элемнта
                        dataGridView.FirstDisplayedScrollingRowIndex = dataGridView.RowCount - 1;

                        dataGridView.Rows[rowIndex].Cells[0].Style.BackColor = Color.Aqua;
                        dataGridView.Rows[rowIndex].Cells[1].Style.BackColor = Color.Aqua;
                        ////**********************///////////////////


                        short levelB = BitConverter.ToInt16(answer, 3);
                        switch (Code)
                        {
                            case 0x3A:
                                double levl = LevelLO2(levelB);
                                DGV.Rows[rowIndex].Cells[index].Value = levl;// LevelLO2(levelB);
                                if (levl < Convert.ToDouble(LO2_NormStart.Value) || levl > Convert.ToDouble(LO2_NormEnd.Value))
                                {
                                    DGV.Rows[rowIndex].Cells[index].Style.BackColor = Color.Red;
                                }
                                break;
                            case 0x39:
                                levl = LevelLO1(levelB, number);
                                DGV.Rows[rowIndex].Cells[index].Value = levl;
                                if (number >= 0 && number < 33)
                                {
                                    if (levl < Convert.ToDouble(LO1_FirstNormStart.Value) || levl > Convert.ToDouble(LO1_FirstNormEnd.Value))
                                    {
                                        DGV.Rows[rowIndex].Cells[index].Style.BackColor = Color.Red;
                                    }
                                }
                                else// >= 33 
                                {
                                    if (levl < Convert.ToDouble(LO1_SecondNormStart.Value) || levl > Convert.ToDouble(LO1_SecondNormEnd.Value))
                                    {
                                        DGV.Rows[rowIndex].Cells[index].Style.BackColor = Color.Red;
                                    }

                                }
                                break;
                        }
                        index++;
                    }
                    DGV.FirstDisplayedScrollingRowIndex = DGV.RowCount - 1;
                    // прокрутка до последней строки
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            int number;
            if ((BandNumber.Text != "") && BandNumber.BackColor == Color.White)
            {
                number = Convert.ToInt32(BandNumber.Text);
            }
            else
            {
                number = 0;
            }

            ButCalibIndLO2.BackColor = Color.Crimson;
            this.Enabled = false;
            //bool flagCalib = IndependetModeLO2();
            bool flagCalib = ReadLevel(DataIndependetLO2, 0x3A);
            if (flagCalib == true)
            {
                ButCalibIndLO2.BackColor = Color.Green;
                this.Enabled = true;
                if (EventLevelSignal != null)
                {
                    EventLevelSignal();
                }
            }
            else
            {
                MessageBox.Show("Произошла ошибка при калбровке");

                this.Enabled = true;
            }
            if (numberURP >= 0)
            {
                Command.SetBandNumber command = new Command.SetBandNumber(Convert.ToByte(numberURP), Convert.ToByte(number));
                byte[] mess = SdS.Serialization(command);
                MessPort(mess, true);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DataIndependetLO2.Rows.Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            SaveTxt(DataIndependetLO2);
        }

        private void button2_Click(object sender, EventArgs e)
        {

            DataCoherentLO2.Rows.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            SaveTxt(DataCoherentLO2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int number;
            if ((BandNumber.Text != "") && BandNumber.BackColor == Color.White)
            {
                number = Convert.ToInt32(BandNumber.Text);
            }
            else
            {
                number = 0;
            }

            ButCalibCohLO2.BackColor = Color.Crimson;
            this.Enabled = false;
            bool flagCalib = ReadLevelCoherentMode(DataCoherentLO2, 0x3A);
            if (flagCalib == true)
            {
                ButCalibCohLO2.BackColor = Color.Green;
                this.Enabled = true;

                if (EventLevelSignal != null)
                {
                    EventLevelSignal();
                }
            }
            else
            {
                MessageBox.Show("Произошла ошибка при калбровке");

                this.Enabled = true;
            }

            if (numberURP >= 0)
            {
                Command.SetBandNumber command = new Command.SetBandNumber(Convert.ToByte(numberURP), Convert.ToByte(number));
                byte[] mess = SdS.Serialization(command);
                MessPort(mess, true);
            }
        }

        private void ReadIndLO1_Click(object sender, EventArgs e)
        {
            int number;
            if ((BandNumber.Text != "") && BandNumber.BackColor == Color.White)
            {
                number = Convert.ToInt32(BandNumber.Text);
            }
            else
            {
                number = 0;
            }

            ReadIndLO1.BackColor = Color.Crimson;
            this.Enabled = false;
            //bool flagCalib = IndependetModeLO2();
            bool flagCalib = ReadLevel(DataIndependet, 0x39);
            if (flagCalib == true)
            {
                ReadIndLO1.BackColor = Color.Green;
                this.Enabled = true;
               
                if (EventLevelSignal != null)
                {
                    EventLevelSignal();
                }
            }
            else
            {
                MessageBox.Show("Произошла ошибка при калбровке");

                this.Enabled = true;
            }
            if (numberURP >= 0)
            {
                Command.SetBandNumber command = new Command.SetBandNumber(Convert.ToByte(numberURP), Convert.ToByte(number));
                byte[] mess = SdS.Serialization(command);
                MessPort(mess, true);
            }
        }

        private void ReadCohLO1_Click(object sender, EventArgs e)
        {
            int number;
            if ((BandNumber.Text != "") && BandNumber.BackColor == Color.White)
            {
                number = Convert.ToInt32(BandNumber.Text);
            }
            else
            {
                number = 0;
            }

            ReadCohLO1.BackColor = Color.Crimson;
            this.Enabled = false;
            bool flagCalib = ReadLevelCoherentMode(DataCoherent, 0x39);
            if (flagCalib == true)
            {
                ReadCohLO1.BackColor = Color.Green;
                this.Enabled = true;
                

                if (EventLevelSignal != null)
                {
                    EventLevelSignal();
                }
            }
            else
            {
                MessageBox.Show("Произошла ошибка при калбровке");

                this.Enabled = true;
            }

            if (numberURP >= 0)
            {
                Command.SetBandNumber command = new Command.SetBandNumber(Convert.ToByte(numberURP), Convert.ToByte(number));
                byte[] mess = SdS.Serialization(command);
                MessPort(mess, true);
            }
        }

        private void lineShape10_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Shift && e.Control && e.Alt && e.KeyCode == Keys.Back)
            {
                EnabledComponents(FlagEnabled);
                SaveSettings.Visible = FlagEnabled;
                FlagEnabled = !FlagEnabled;
            }
        }

        private void OpenSpi_Click(object sender, EventArgs e)
        {
            if (portSPI == null)
            {
                var deviceManager = new FpgaDeviceManager();
                var receiverManager = new ReceiverManager(deviceManager, deviceManager.GetFpgaTransmissionChannel());

                deviceManager.Initialize();
                receiverManager.Initialize();

                portSPI = receiverManager;

                if (portSPI.DataChannel.IsConnected)
                {
                    Command.EmptyCommand empty = new Command.EmptyCommand(0x03, 0x0A, 0x00);
                    byte[] mess = SDS.SdS.Serialization(empty);
                    MessPort(mess, true);
                    EnabledPanel(true, false);
                    EnableScheme(true);
                    OpenSpi.Text = "Close";
                }
            }
            else
            {
                portSPI.DataChannel.Disconnect();
                portSPI = null;
                EnabledPanel(true, true);
                EnableScheme(false);
                OpenSpi.Text = "Open";
            }

        }

        private void ChangedValue(object sender, EventArgs e)
        {
            string[] StrNames =(sender as NumericUpDown).Name.Split('_');
            ini.IniWriteValue(StrNames[0], StrNames[1], Convert.ToString((sender as NumericUpDown).Value));
        }

        private void SaveSettings_Click(object sender, EventArgs e)
        {
           // if (portCOM != null)
            
                if (numberURP != -1)
                {
                    Command.SaveData saveData = new Command.SaveData(Convert.ToByte(numberURP));
                    byte[] mess;
                    mess = SDS.SdS.Serialization(saveData);
                   /* if (portCOM != null)
                    { 
                        mess = new byte[]{0x03, Convert.ToByte(numberURP), 0x36};
                    }
                    else
                    {
                        mess = SDS.SdS.Serialization(saveData); ;
                    }*/
                    MessPort(mess, true);
                }
                else
                {
                    MessageBox.Show("Необходимо выбрать номер УРП");
                    NumberURP.Focus();
                }
            
        }



    }
}
