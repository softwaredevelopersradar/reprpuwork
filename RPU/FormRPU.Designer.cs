﻿namespace RPU
{
    partial class FormRPU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRPU));
            this.ComPanel = new System.Windows.Forms.Panel();
            this.OpenPort = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SpeedPortBox = new System.Windows.Forms.TextBox();
            this.ComPortName = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Temperatur = new System.Windows.Forms.CheckBox();
            this.Clear = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Request = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Answer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel19 = new System.Windows.Forms.Panel();
            this.NumberURP = new System.Windows.Forms.CheckedListBox();
            this.FreqLabel = new System.Windows.Forms.Label();
            this.PlusBand = new System.Windows.Forms.Button();
            this.MinusBand = new System.Windows.Forms.Button();
            this.BandNumber = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.SchemeControl = new System.Windows.Forms.TabControl();
            this.Scheme = new System.Windows.Forms.TabPage();
            this.panel20 = new System.Windows.Forms.Panel();
            this.Level_PCH1 = new System.Windows.Forms.Label();
            this.PCH_Peleng = new System.Windows.Forms.Panel();
            this.Level_PCH5 = new System.Windows.Forms.Label();
            this.Level_PCH4 = new System.Windows.Forms.Label();
            this.Level_PCH3 = new System.Windows.Forms.Label();
            this.Level_PCH2 = new System.Windows.Forms.Label();
            this.Thermometer = new NationalInstruments.UI.WindowsForms.Thermometer();
            this.Filter8 = new NationalInstruments.UI.WindowsForms.Led();
            this.Filter7 = new NationalInstruments.UI.WindowsForms.Led();
            this.Filter6 = new NationalInstruments.UI.WindowsForms.Led();
            this.Filter5 = new NationalInstruments.UI.WindowsForms.Led();
            this.Filter4 = new NationalInstruments.UI.WindowsForms.Led();
            this.Filter3 = new NationalInstruments.UI.WindowsForms.Led();
            this.Filter2 = new NationalInstruments.UI.WindowsForms.Led();
            this.Filter1 = new NationalInstruments.UI.WindowsForms.Led();
            this.Filter9 = new NationalInstruments.UI.WindowsForms.Led();
            this.label36 = new System.Windows.Forms.Label();
            this.Filter10 = new NationalInstruments.UI.WindowsForms.Led();
            this.label35 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panelUNN = new System.Windows.Forms.Panel();
            this.MinusUFF = new System.Windows.Forms.Button();
            this.UNNBox = new System.Windows.Forms.TextBox();
            this.PlusUUF = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.panelATT1 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.MinusATT1 = new System.Windows.Forms.Button();
            this.PlusATT1 = new System.Windows.Forms.Button();
            this.ATTBox1 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.LedATT4 = new NationalInstruments.UI.WindowsForms.Led();
            this.LedATT3 = new NationalInstruments.UI.WindowsForms.Led();
            this.LedATT2 = new NationalInstruments.UI.WindowsForms.Led();
            this.LedATT1 = new NationalInstruments.UI.WindowsForms.Led();
            this.ATT4 = new System.Windows.Forms.Button();
            this.ATT3 = new System.Windows.Forms.Button();
            this.ATT2 = new System.Windows.Forms.Button();
            this.ATT1 = new System.Windows.Forms.Button();
            this.panelATT2 = new System.Windows.Forms.Panel();
            this.MinusATT2 = new System.Windows.Forms.Button();
            this.PlusATT2 = new System.Windows.Forms.Button();
            this.ATTBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Switch9 = new System.Windows.Forms.PictureBox();
            this.Switch7 = new System.Windows.Forms.PictureBox();
            this.Switch8 = new System.Windows.Forms.PictureBox();
            this.Switch6 = new System.Windows.Forms.PictureBox();
            this.Switch5 = new System.Windows.Forms.PictureBox();
            this.Switch4 = new System.Windows.Forms.PictureBox();
            this.Switch3 = new System.Windows.Forms.PictureBox();
            this.Switch2 = new System.Windows.Forms.PictureBox();
            this.Switch1 = new System.Windows.Forms.PictureBox();
            this.LO2 = new System.Windows.Forms.Button();
            this.LO1 = new System.Windows.Forms.Button();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape56 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape55 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape54 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape53 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape50 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape49 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape17 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape16 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape15 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape14 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape13 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape12 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape48 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape47 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape46 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape45 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.ovalShape1 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.lineShape44 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape43 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape42 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape41 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape40 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape39 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape38 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape37 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape36 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape35 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape34 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape33 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape32 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape31 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape30 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape29 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape28 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape27 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape26 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape11 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape26 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape25 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape24 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape52 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape51 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape10 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape25 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape24 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape9 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape23 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape22 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape8 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape21 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape20 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape7 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape19 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape18 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape6 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape17 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape16 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape5 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape15 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape14 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape13 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape4 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape12 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape11 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape10 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape9 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape8 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape7 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape3 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.Heter1 = new System.Windows.Forms.TabPage();
            this.LO1_SignalLevel = new System.Windows.Forms.Label();
            this.LO1_Peleng = new System.Windows.Forms.Panel();
            this.LO1_SignalLevel5 = new System.Windows.Forms.Label();
            this.LO1_SignalLevel4 = new System.Windows.Forms.Label();
            this.LO1_SignalLevel3 = new System.Windows.Forms.Label();
            this.LO1_SignalLevel2 = new System.Windows.Forms.Label();
            this.MinusLO1_URP5 = new System.Windows.Forms.Button();
            this.LO1_URP5 = new System.Windows.Forms.TextBox();
            this.PlusLO1_URP5 = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.MinusLO1_URP4 = new System.Windows.Forms.Button();
            this.LO1_URP4 = new System.Windows.Forms.TextBox();
            this.PlusLO1_URP4 = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.MinusLO1_URP3 = new System.Windows.Forms.Button();
            this.LO1_URP3 = new System.Windows.Forms.TextBox();
            this.PlusLO1_URP3 = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.MinusLO1_URP2 = new System.Windows.Forms.Button();
            this.LO1_URP2 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.PlusLO1_URP2 = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.LOHeter1 = new NationalInstruments.UI.WindowsForms.Switch();
            this.panelATTLo1 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.MinusLO1 = new System.Windows.Forms.Button();
            this.LO1_ATT = new System.Windows.Forms.TextBox();
            this.PlusLO1 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.LO1_PF4 = new System.Windows.Forms.Button();
            this.LO1_PF3 = new System.Windows.Forms.Button();
            this.LO1_PF2 = new System.Windows.Forms.Button();
            this.LO1_PF1 = new System.Windows.Forms.Button();
            this.Heter2 = new System.Windows.Forms.TabPage();
            this.LO2_Peleng = new System.Windows.Forms.Panel();
            this.LO2_SignalLevel5 = new System.Windows.Forms.Label();
            this.LO2_SignalLevel4 = new System.Windows.Forms.Label();
            this.LO2_SignalLevel3 = new System.Windows.Forms.Label();
            this.LO2_SignalLevel2 = new System.Windows.Forms.Label();
            this.LO2_SignalLevel = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.LOHeter2 = new NationalInstruments.UI.WindowsForms.Switch();
            this.Switch11 = new System.Windows.Forms.PictureBox();
            this.Switch10 = new System.Windows.Forms.PictureBox();
            this.LO2_PF2 = new System.Windows.Forms.Button();
            this.LO2_PF1 = new System.Windows.Forms.Button();
            this.Calibration = new System.Windows.Forms.TabPage();
            this.LO1_SecondNormEnd = new System.Windows.Forms.NumericUpDown();
            this.label49 = new System.Windows.Forms.Label();
            this.LO1_SecondNormStart = new System.Windows.Forms.NumericUpDown();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.LO1_FirstNormEnd = new System.Windows.Forms.NumericUpDown();
            this.label48 = new System.Windows.Forms.Label();
            this.LO1_FirstNormStart = new System.Windows.Forms.NumericUpDown();
            this.label47 = new System.Windows.Forms.Label();
            this.FirstRangeLo1 = new System.Windows.Forms.Label();
            this.panelCohLO1 = new System.Windows.Forms.Panel();
            this.ReadCohLO1 = new System.Windows.Forms.Button();
            this.ButDelCoh = new System.Windows.Forms.Button();
            this.ButSavCoh = new System.Windows.Forms.Button();
            this.panelIndLO1 = new System.Windows.Forms.Panel();
            this.ReadIndLO1 = new System.Windows.Forms.Button();
            this.ButDelInd = new System.Windows.Forms.Button();
            this.ButSavInd = new System.Windows.Forms.Button();
            this.ButCalibCoh = new System.Windows.Forms.Button();
            this.ButCalibInd = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.DataCoherent = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataIndependet = new System.Windows.Forms.DataGridView();
            this.ColumnNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFreq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnURP1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnURP2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnURP3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnURP4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnURP5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnURP6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CalibrationLO2 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label54 = new System.Windows.Forms.Label();
            this.LO2_NormEnd = new System.Windows.Forms.NumericUpDown();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.LO2_NormStart = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ButCalibCohLO2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.ButCalibIndLO2 = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.DataCoherentLO2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataIndependetLO2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CommandTab = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.RichTextBox();
            this.LoadButton = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.ClearButton = new System.Windows.Forms.Button();
            this.saveIndependentMode = new System.Windows.Forms.SaveFileDialog();
            this.saveCoherentMode = new System.Windows.Forms.SaveFileDialog();
            this.timerThermometer = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.ComTab = new System.Windows.Forms.TabPage();
            this.SpiTab = new System.Windows.Forms.TabPage();
            this.SPIPanel = new System.Windows.Forms.Panel();
            this.OpenSpi = new System.Windows.Forms.Button();
            this.SaveSettings = new System.Windows.Forms.Button();
            this.ComPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.panel19.SuspendLayout();
            this.SchemeControl.SuspendLayout();
            this.Scheme.SuspendLayout();
            this.panel20.SuspendLayout();
            this.PCH_Peleng.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Thermometer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter10)).BeginInit();
            this.panelUNN.SuspendLayout();
            this.panelATT1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LedATT4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedATT3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedATT2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedATT1)).BeginInit();
            this.panelATT2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Switch9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch1)).BeginInit();
            this.Heter1.SuspendLayout();
            this.LO1_Peleng.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LOHeter1)).BeginInit();
            this.panelATTLo1.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel14.SuspendLayout();
            this.Heter2.SuspendLayout();
            this.LO2_Peleng.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LOHeter2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch10)).BeginInit();
            this.Calibration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LO1_SecondNormEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LO1_SecondNormStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LO1_FirstNormEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LO1_FirstNormStart)).BeginInit();
            this.panelCohLO1.SuspendLayout();
            this.panelIndLO1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataCoherent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataIndependet)).BeginInit();
            this.CalibrationLO2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LO2_NormEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LO2_NormStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataCoherentLO2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataIndependetLO2)).BeginInit();
            this.CommandTab.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.ComTab.SuspendLayout();
            this.SpiTab.SuspendLayout();
            this.SPIPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ComPanel
            // 
            this.ComPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ComPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ComPanel.Controls.Add(this.OpenPort);
            this.ComPanel.Controls.Add(this.label2);
            this.ComPanel.Controls.Add(this.label1);
            this.ComPanel.Controls.Add(this.SpeedPortBox);
            this.ComPanel.Controls.Add(this.ComPortName);
            this.ComPanel.Location = new System.Drawing.Point(0, 1);
            this.ComPanel.Name = "ComPanel";
            this.ComPanel.Size = new System.Drawing.Size(190, 101);
            this.ComPanel.TabIndex = 0;
            // 
            // OpenPort
            // 
            this.OpenPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenPort.Location = new System.Drawing.Point(54, 70);
            this.OpenPort.Name = "OpenPort";
            this.OpenPort.Size = new System.Drawing.Size(65, 23);
            this.OpenPort.TabIndex = 4;
            this.OpenPort.Text = "Open";
            this.OpenPort.UseVisualStyleBackColor = true;
            this.OpenPort.Click += new System.EventHandler(this.OpenPort_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Speed:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Com Port Name:";
            // 
            // SpeedPortBox
            // 
            this.SpeedPortBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SpeedPortBox.Location = new System.Drawing.Point(113, 45);
            this.SpeedPortBox.Name = "SpeedPortBox";
            this.SpeedPortBox.Size = new System.Drawing.Size(68, 20);
            this.SpeedPortBox.TabIndex = 1;
            this.SpeedPortBox.Text = "115200";
            // 
            // ComPortName
            // 
            this.ComPortName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ComPortName.FormattingEnabled = true;
            this.ComPortName.Location = new System.Drawing.Point(113, 9);
            this.ComPortName.Name = "ComPortName";
            this.ComPortName.Size = new System.Drawing.Size(68, 21);
            this.ComPortName.TabIndex = 0;
            this.ComPortName.SelectedIndexChanged += new System.EventHandler(this.ComPortName_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.Temperatur);
            this.panel1.Controls.Add(this.Clear);
            this.panel1.Controls.Add(this.dataGridView);
            this.panel1.Controls.Add(this.panel19);
            this.panel1.Controls.Add(this.FreqLabel);
            this.panel1.Controls.Add(this.PlusBand);
            this.panel1.Controls.Add(this.MinusBand);
            this.panel1.Controls.Add(this.BandNumber);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Location = new System.Drawing.Point(3, 139);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 390);
            this.panel1.TabIndex = 1;
            // 
            // Temperatur
            // 
            this.Temperatur.AutoSize = true;
            this.Temperatur.Location = new System.Drawing.Point(11, 84);
            this.Temperatur.Name = "Temperatur";
            this.Temperatur.Size = new System.Drawing.Size(111, 17);
            this.Temperatur.TabIndex = 56;
            this.Temperatur.Text = "Auto Temperature";
            this.Temperatur.UseVisualStyleBackColor = true;
            // 
            // Clear
            // 
            this.Clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Clear.Location = new System.Drawing.Point(58, 359);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(75, 23);
            this.Clear.TabIndex = 55;
            this.Clear.Text = "Clear";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Request,
            this.Answer});
            this.dataGridView.Location = new System.Drawing.Point(3, 232);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.Size = new System.Drawing.Size(192, 121);
            this.dataGridView.TabIndex = 54;
            this.dataGridView.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridView_KeyUp);
            // 
            // Request
            // 
            this.Request.FillWeight = 115F;
            this.Request.HeaderText = "Request";
            this.Request.Name = "Request";
            // 
            // Answer
            // 
            this.Answer.FillWeight = 115F;
            this.Answer.HeaderText = "Answer";
            this.Answer.Name = "Answer";
            // 
            // panel19
            // 
            this.panel19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel19.BackColor = System.Drawing.Color.LightGray;
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel19.Controls.Add(this.NumberURP);
            this.panel19.Location = new System.Drawing.Point(4, 108);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(191, 119);
            this.panel19.TabIndex = 53;
            // 
            // NumberURP
            // 
            this.NumberURP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NumberURP.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.NumberURP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NumberURP.CheckOnClick = true;
            this.NumberURP.FormattingEnabled = true;
            this.NumberURP.Items.AddRange(new object[] {
            "URP 1...URP 5",
            "URP 1",
            "URP 2",
            "URP 3",
            "URP 4",
            "URP 5",
            "URP 6"});
            this.NumberURP.Location = new System.Drawing.Point(5, 3);
            this.NumberURP.Name = "NumberURP";
            this.NumberURP.Size = new System.Drawing.Size(177, 107);
            this.NumberURP.TabIndex = 0;
            this.NumberURP.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.NumberURP_ItemCheck);
            this.NumberURP.SelectedIndexChanged += new System.EventHandler(this.NumberURP_SelectedIndexChanged);
            // 
            // FreqLabel
            // 
            this.FreqLabel.AutoSize = true;
            this.FreqLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FreqLabel.Location = new System.Drawing.Point(11, 57);
            this.FreqLabel.Name = "FreqLabel";
            this.FreqLabel.Size = new System.Drawing.Size(30, 20);
            this.FreqLabel.TabIndex = 52;
            this.FreqLabel.Text = "F=";
            // 
            // PlusBand
            // 
            this.PlusBand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PlusBand.BackgroundImage = global::RPU.Properties.Resources.bfa_plus_simple_aqua_48x48;
            this.PlusBand.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlusBand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlusBand.ForeColor = System.Drawing.Color.Gainsboro;
            this.PlusBand.Location = new System.Drawing.Point(155, 6);
            this.PlusBand.Name = "PlusBand";
            this.PlusBand.Size = new System.Drawing.Size(29, 26);
            this.PlusBand.TabIndex = 51;
            this.PlusBand.UseVisualStyleBackColor = true;
            this.PlusBand.Click += new System.EventHandler(this.PlusBand_Click);
            // 
            // MinusBand
            // 
            this.MinusBand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MinusBand.BackgroundImage = global::RPU.Properties.Resources.bfa_minus_simple_aqua_48x48;
            this.MinusBand.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MinusBand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinusBand.ForeColor = System.Drawing.Color.Gainsboro;
            this.MinusBand.Location = new System.Drawing.Point(155, 33);
            this.MinusBand.Name = "MinusBand";
            this.MinusBand.Size = new System.Drawing.Size(26, 28);
            this.MinusBand.TabIndex = 50;
            this.MinusBand.UseVisualStyleBackColor = true;
            this.MinusBand.Click += new System.EventHandler(this.MinusBand_Click);
            // 
            // BandNumber
            // 
            this.BandNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BandNumber.Location = new System.Drawing.Point(99, 17);
            this.BandNumber.Name = "BandNumber";
            this.BandNumber.Size = new System.Drawing.Size(50, 20);
            this.BandNumber.TabIndex = 2;
            this.BandNumber.TextChanged += new System.EventHandler(this.BandNumber_TextChanged);
            this.BandNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BandNumber_KeyPress);
            this.BandNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.BandNumber_KeyUp);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(27, 33);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(40, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "(0...99)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(8, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(85, 15);
            this.label20.TabIndex = 0;
            this.label20.Text = "Band number:";
            // 
            // SchemeControl
            // 
            this.SchemeControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SchemeControl.Controls.Add(this.Scheme);
            this.SchemeControl.Controls.Add(this.Heter1);
            this.SchemeControl.Controls.Add(this.Heter2);
            this.SchemeControl.Controls.Add(this.Calibration);
            this.SchemeControl.Controls.Add(this.CalibrationLO2);
            this.SchemeControl.Controls.Add(this.CommandTab);
            this.SchemeControl.Location = new System.Drawing.Point(209, 1);
            this.SchemeControl.Name = "SchemeControl";
            this.SchemeControl.SelectedIndex = 0;
            this.SchemeControl.Size = new System.Drawing.Size(1328, 523);
            this.SchemeControl.TabIndex = 2;
            // 
            // Scheme
            // 
            this.Scheme.BackColor = System.Drawing.Color.LightGray;
            this.Scheme.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Scheme.BackgroundImage")));
            this.Scheme.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Scheme.Controls.Add(this.SaveSettings);
            this.Scheme.Controls.Add(this.panel20);
            this.Scheme.Controls.Add(this.PCH_Peleng);
            this.Scheme.Controls.Add(this.Thermometer);
            this.Scheme.Controls.Add(this.Filter8);
            this.Scheme.Controls.Add(this.Filter7);
            this.Scheme.Controls.Add(this.Filter6);
            this.Scheme.Controls.Add(this.Filter5);
            this.Scheme.Controls.Add(this.Filter4);
            this.Scheme.Controls.Add(this.Filter3);
            this.Scheme.Controls.Add(this.Filter2);
            this.Scheme.Controls.Add(this.Filter1);
            this.Scheme.Controls.Add(this.Filter9);
            this.Scheme.Controls.Add(this.label36);
            this.Scheme.Controls.Add(this.Filter10);
            this.Scheme.Controls.Add(this.label35);
            this.Scheme.Controls.Add(this.label33);
            this.Scheme.Controls.Add(this.label32);
            this.Scheme.Controls.Add(this.label31);
            this.Scheme.Controls.Add(this.label30);
            this.Scheme.Controls.Add(this.label29);
            this.Scheme.Controls.Add(this.label28);
            this.Scheme.Controls.Add(this.label27);
            this.Scheme.Controls.Add(this.label26);
            this.Scheme.Controls.Add(this.panelUNN);
            this.Scheme.Controls.Add(this.panelATT1);
            this.Scheme.Controls.Add(this.panel13);
            this.Scheme.Controls.Add(this.panel12);
            this.Scheme.Controls.Add(this.panel11);
            this.Scheme.Controls.Add(this.panel10);
            this.Scheme.Controls.Add(this.panel9);
            this.Scheme.Controls.Add(this.panel8);
            this.Scheme.Controls.Add(this.panel7);
            this.Scheme.Controls.Add(this.panel6);
            this.Scheme.Controls.Add(this.panel5);
            this.Scheme.Controls.Add(this.panel4);
            this.Scheme.Controls.Add(this.LedATT4);
            this.Scheme.Controls.Add(this.LedATT3);
            this.Scheme.Controls.Add(this.LedATT2);
            this.Scheme.Controls.Add(this.LedATT1);
            this.Scheme.Controls.Add(this.ATT4);
            this.Scheme.Controls.Add(this.ATT3);
            this.Scheme.Controls.Add(this.ATT2);
            this.Scheme.Controls.Add(this.ATT1);
            this.Scheme.Controls.Add(this.panelATT2);
            this.Scheme.Controls.Add(this.Switch9);
            this.Scheme.Controls.Add(this.Switch7);
            this.Scheme.Controls.Add(this.Switch8);
            this.Scheme.Controls.Add(this.Switch6);
            this.Scheme.Controls.Add(this.Switch5);
            this.Scheme.Controls.Add(this.Switch4);
            this.Scheme.Controls.Add(this.Switch3);
            this.Scheme.Controls.Add(this.Switch2);
            this.Scheme.Controls.Add(this.Switch1);
            this.Scheme.Controls.Add(this.LO2);
            this.Scheme.Controls.Add(this.LO1);
            this.Scheme.Controls.Add(this.shapeContainer2);
            this.Scheme.Location = new System.Drawing.Point(4, 22);
            this.Scheme.Name = "Scheme";
            this.Scheme.Padding = new System.Windows.Forms.Padding(3);
            this.Scheme.Size = new System.Drawing.Size(1320, 497);
            this.Scheme.TabIndex = 0;
            this.Scheme.Text = "Scheme";
            // 
            // panel20
            // 
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel20.Controls.Add(this.Level_PCH1);
            this.panel20.Location = new System.Drawing.Point(1144, 376);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(170, 27);
            this.panel20.TabIndex = 74;
            // 
            // Level_PCH1
            // 
            this.Level_PCH1.AutoSize = true;
            this.Level_PCH1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Level_PCH1.Location = new System.Drawing.Point(3, 7);
            this.Level_PCH1.Name = "Level_PCH1";
            this.Level_PCH1.Size = new System.Drawing.Size(44, 13);
            this.Level_PCH1.TabIndex = 71;
            this.Level_PCH1.Text = "Pout 1";
            // 
            // PCH_Peleng
            // 
            this.PCH_Peleng.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PCH_Peleng.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PCH_Peleng.Controls.Add(this.Level_PCH5);
            this.PCH_Peleng.Controls.Add(this.Level_PCH4);
            this.PCH_Peleng.Controls.Add(this.Level_PCH3);
            this.PCH_Peleng.Controls.Add(this.Level_PCH2);
            this.PCH_Peleng.Location = new System.Drawing.Point(1144, 402);
            this.PCH_Peleng.Name = "PCH_Peleng";
            this.PCH_Peleng.Size = new System.Drawing.Size(170, 76);
            this.PCH_Peleng.TabIndex = 72;
            this.PCH_Peleng.Visible = false;
            // 
            // Level_PCH5
            // 
            this.Level_PCH5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Level_PCH5.AutoSize = true;
            this.Level_PCH5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Level_PCH5.Location = new System.Drawing.Point(2, 58);
            this.Level_PCH5.Name = "Level_PCH5";
            this.Level_PCH5.Size = new System.Drawing.Size(44, 13);
            this.Level_PCH5.TabIndex = 75;
            this.Level_PCH5.Text = "Pout 5";
            // 
            // Level_PCH4
            // 
            this.Level_PCH4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Level_PCH4.AutoSize = true;
            this.Level_PCH4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Level_PCH4.Location = new System.Drawing.Point(2, 40);
            this.Level_PCH4.Name = "Level_PCH4";
            this.Level_PCH4.Size = new System.Drawing.Size(44, 13);
            this.Level_PCH4.TabIndex = 74;
            this.Level_PCH4.Text = "Pout 4";
            // 
            // Level_PCH3
            // 
            this.Level_PCH3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Level_PCH3.AutoSize = true;
            this.Level_PCH3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Level_PCH3.Location = new System.Drawing.Point(2, 22);
            this.Level_PCH3.Name = "Level_PCH3";
            this.Level_PCH3.Size = new System.Drawing.Size(44, 13);
            this.Level_PCH3.TabIndex = 73;
            this.Level_PCH3.Text = "Pout 3";
            // 
            // Level_PCH2
            // 
            this.Level_PCH2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Level_PCH2.AutoSize = true;
            this.Level_PCH2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Level_PCH2.Location = new System.Drawing.Point(2, 4);
            this.Level_PCH2.Name = "Level_PCH2";
            this.Level_PCH2.Size = new System.Drawing.Size(44, 13);
            this.Level_PCH2.TabIndex = 72;
            this.Level_PCH2.Text = "Pout 2";
            // 
            // Thermometer
            // 
            this.Thermometer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Thermometer.FillColor = System.Drawing.SystemColors.MenuHighlight;
            this.Thermometer.Location = new System.Drawing.Point(1242, 4);
            this.Thermometer.Name = "Thermometer";
            this.Thermometer.Range = new NationalInstruments.UI.Range(-40D, 85D);
            this.Thermometer.Size = new System.Drawing.Size(72, 184);
            this.Thermometer.TabIndex = 70;
            // 
            // Filter8
            // 
            this.Filter8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Filter8.BackColor = System.Drawing.Color.LightGray;
            this.Filter8.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.Filter8.Location = new System.Drawing.Point(187, 294);
            this.Filter8.Name = "Filter8";
            this.Filter8.OffColor = System.Drawing.Color.Crimson;
            this.Filter8.Size = new System.Drawing.Size(20, 18);
            this.Filter8.TabIndex = 69;
            // 
            // Filter7
            // 
            this.Filter7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Filter7.BackColor = System.Drawing.Color.LightGray;
            this.Filter7.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.Filter7.Location = new System.Drawing.Point(187, 265);
            this.Filter7.Name = "Filter7";
            this.Filter7.OffColor = System.Drawing.Color.Crimson;
            this.Filter7.Size = new System.Drawing.Size(20, 18);
            this.Filter7.TabIndex = 68;
            // 
            // Filter6
            // 
            this.Filter6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Filter6.BackColor = System.Drawing.Color.LightGray;
            this.Filter6.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.Filter6.Location = new System.Drawing.Point(187, 235);
            this.Filter6.Name = "Filter6";
            this.Filter6.OffColor = System.Drawing.Color.Crimson;
            this.Filter6.Size = new System.Drawing.Size(20, 18);
            this.Filter6.TabIndex = 67;
            // 
            // Filter5
            // 
            this.Filter5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Filter5.BackColor = System.Drawing.Color.LightGray;
            this.Filter5.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.Filter5.Location = new System.Drawing.Point(187, 206);
            this.Filter5.Name = "Filter5";
            this.Filter5.OffColor = System.Drawing.Color.Crimson;
            this.Filter5.Size = new System.Drawing.Size(20, 18);
            this.Filter5.TabIndex = 66;
            // 
            // Filter4
            // 
            this.Filter4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Filter4.BackColor = System.Drawing.Color.LightGray;
            this.Filter4.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.Filter4.Location = new System.Drawing.Point(188, 176);
            this.Filter4.Name = "Filter4";
            this.Filter4.OffColor = System.Drawing.Color.Crimson;
            this.Filter4.Size = new System.Drawing.Size(20, 18);
            this.Filter4.TabIndex = 65;
            // 
            // Filter3
            // 
            this.Filter3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Filter3.BackColor = System.Drawing.Color.LightGray;
            this.Filter3.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.Filter3.Location = new System.Drawing.Point(188, 146);
            this.Filter3.Name = "Filter3";
            this.Filter3.OffColor = System.Drawing.Color.Crimson;
            this.Filter3.Size = new System.Drawing.Size(20, 18);
            this.Filter3.TabIndex = 64;
            // 
            // Filter2
            // 
            this.Filter2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Filter2.BackColor = System.Drawing.Color.LightGray;
            this.Filter2.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.Filter2.Location = new System.Drawing.Point(187, 116);
            this.Filter2.Name = "Filter2";
            this.Filter2.OffColor = System.Drawing.Color.Crimson;
            this.Filter2.Size = new System.Drawing.Size(20, 18);
            this.Filter2.TabIndex = 63;
            // 
            // Filter1
            // 
            this.Filter1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Filter1.BackColor = System.Drawing.Color.LightGray;
            this.Filter1.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.Filter1.Location = new System.Drawing.Point(187, 86);
            this.Filter1.Name = "Filter1";
            this.Filter1.OffColor = System.Drawing.Color.Crimson;
            this.Filter1.Size = new System.Drawing.Size(20, 18);
            this.Filter1.TabIndex = 62;
            // 
            // Filter9
            // 
            this.Filter9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Filter9.BackColor = System.Drawing.Color.LightGray;
            this.Filter9.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.Filter9.Location = new System.Drawing.Point(189, 341);
            this.Filter9.Name = "Filter9";
            this.Filter9.OffColor = System.Drawing.Color.Crimson;
            this.Filter9.Size = new System.Drawing.Size(20, 22);
            this.Filter9.TabIndex = 61;
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(204, 345);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(104, 13);
            this.label36.TabIndex = 60;
            this.label36.Text = "1000 - 2100 MHz";
            // 
            // Filter10
            // 
            this.Filter10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Filter10.BackColor = System.Drawing.Color.LightGray;
            this.Filter10.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.Filter10.Location = new System.Drawing.Point(189, 384);
            this.Filter10.Name = "Filter10";
            this.Filter10.OffColor = System.Drawing.Color.Crimson;
            this.Filter10.Size = new System.Drawing.Size(20, 22);
            this.Filter10.TabIndex = 59;
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.Location = new System.Drawing.Point(207, 388);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(104, 13);
            this.label35.TabIndex = 58;
            this.label35.Text = "2000 - 3000 MHz";
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.Location = new System.Drawing.Point(204, 296);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(111, 16);
            this.label33.TabIndex = 56;
            this.label33.Text = "880 - 1000 MHz";
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(210, 265);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(103, 16);
            this.label32.TabIndex = 55;
            this.label32.Text = "470 - 880 MHz";
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(212, 235);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(103, 16);
            this.label31.TabIndex = 54;
            this.label31.Text = "292 - 490 MHz";
            // 
            // label30
            // 
            this.label30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(210, 206);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(103, 16);
            this.label30.TabIndex = 53;
            this.label30.Text = "186 - 340 MHz";
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.Location = new System.Drawing.Point(210, 176);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(103, 16);
            this.label29.TabIndex = 52;
            this.label29.Text = "120 - 210 MHz";
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(213, 146);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(95, 16);
            this.label28.TabIndex = 51;
            this.label28.Text = "75 - 131 MHz";
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(213, 116);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(87, 16);
            this.label27.TabIndex = 50;
            this.label27.Text = "60 - 90 MHz";
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(213, 86);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(87, 16);
            this.label26.TabIndex = 49;
            this.label26.Text = "30 - 70 MHz";
            // 
            // panelUNN
            // 
            this.panelUNN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelUNN.BackColor = System.Drawing.Color.Gainsboro;
            this.panelUNN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelUNN.Controls.Add(this.MinusUFF);
            this.panelUNN.Controls.Add(this.UNNBox);
            this.panelUNN.Controls.Add(this.PlusUUF);
            this.panelUNN.Controls.Add(this.label23);
            this.panelUNN.Location = new System.Drawing.Point(1143, 345);
            this.panelUNN.Name = "panelUNN";
            this.panelUNN.Size = new System.Drawing.Size(123, 29);
            this.panelUNN.TabIndex = 48;
            // 
            // MinusUFF
            // 
            this.MinusUFF.BackgroundImage = global::RPU.Properties.Resources.bfa_minus_simple_aqua_48x48;
            this.MinusUFF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MinusUFF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinusUFF.ForeColor = System.Drawing.Color.Gainsboro;
            this.MinusUFF.Location = new System.Drawing.Point(-1, -1);
            this.MinusUFF.Name = "MinusUFF";
            this.MinusUFF.Size = new System.Drawing.Size(26, 28);
            this.MinusUFF.TabIndex = 50;
            this.MinusUFF.UseVisualStyleBackColor = true;
            this.MinusUFF.Click += new System.EventHandler(this.MinusUFF_Click);
            // 
            // UNNBox
            // 
            this.UNNBox.Location = new System.Drawing.Point(29, 3);
            this.UNNBox.Name = "UNNBox";
            this.UNNBox.Size = new System.Drawing.Size(34, 20);
            this.UNNBox.TabIndex = 22;
            this.UNNBox.TextChanged += new System.EventHandler(this.UNNBox_TextChanged);
            this.UNNBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.UNNBox_KeyPress);
            this.UNNBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.UNNBox_KeyUp);
            // 
            // PlusUUF
            // 
            this.PlusUUF.BackgroundImage = global::RPU.Properties.Resources.bfa_plus_simple_aqua_48x48;
            this.PlusUUF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlusUUF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlusUUF.ForeColor = System.Drawing.Color.Gainsboro;
            this.PlusUUF.Location = new System.Drawing.Point(89, -1);
            this.PlusUUF.Name = "PlusUUF";
            this.PlusUUF.Size = new System.Drawing.Size(29, 26);
            this.PlusUUF.TabIndex = 51;
            this.PlusUUF.UseVisualStyleBackColor = true;
            this.PlusUUF.Click += new System.EventHandler(this.PlusUUF_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(66, 6);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(20, 13);
            this.label23.TabIndex = 23;
            this.label23.Text = "dB";
            // 
            // panelATT1
            // 
            this.panelATT1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelATT1.BackColor = System.Drawing.Color.Gainsboro;
            this.panelATT1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelATT1.Controls.Add(this.panel16);
            this.panelATT1.Controls.Add(this.textBox2);
            this.panelATT1.Controls.Add(this.label3);
            this.panelATT1.Location = new System.Drawing.Point(640, 217);
            this.panelATT1.Name = "panelATT1";
            this.panelATT1.Size = new System.Drawing.Size(123, 29);
            this.panelATT1.TabIndex = 27;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Gainsboro;
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel16.Controls.Add(this.MinusATT1);
            this.panel16.Controls.Add(this.PlusATT1);
            this.panel16.Controls.Add(this.ATTBox1);
            this.panel16.Controls.Add(this.label22);
            this.panel16.Location = new System.Drawing.Point(-2, -2);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(123, 29);
            this.panel16.TabIndex = 28;
            // 
            // MinusATT1
            // 
            this.MinusATT1.BackgroundImage = global::RPU.Properties.Resources.bfa_minus_simple_aqua_48x48;
            this.MinusATT1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MinusATT1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinusATT1.ForeColor = System.Drawing.Color.Gainsboro;
            this.MinusATT1.Location = new System.Drawing.Point(0, -1);
            this.MinusATT1.Name = "MinusATT1";
            this.MinusATT1.Size = new System.Drawing.Size(26, 28);
            this.MinusATT1.TabIndex = 49;
            this.MinusATT1.UseVisualStyleBackColor = true;
            this.MinusATT1.Click += new System.EventHandler(this.MinusATT1_Click);
            // 
            // PlusATT1
            // 
            this.PlusATT1.BackgroundImage = global::RPU.Properties.Resources.bfa_plus_simple_aqua_48x48;
            this.PlusATT1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlusATT1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlusATT1.ForeColor = System.Drawing.Color.Gainsboro;
            this.PlusATT1.Location = new System.Drawing.Point(92, -1);
            this.PlusATT1.Name = "PlusATT1";
            this.PlusATT1.Size = new System.Drawing.Size(29, 26);
            this.PlusATT1.TabIndex = 49;
            this.PlusATT1.UseVisualStyleBackColor = true;
            this.PlusATT1.Click += new System.EventHandler(this.PlusATT1_Click);
            // 
            // ATTBox1
            // 
            this.ATTBox1.Location = new System.Drawing.Point(31, 1);
            this.ATTBox1.Name = "ATTBox1";
            this.ATTBox1.Size = new System.Drawing.Size(40, 20);
            this.ATTBox1.TabIndex = 22;
            this.ATTBox1.TextChanged += new System.EventHandler(this.ATTBox1_TextChanged);
            this.ATTBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ATTBox1_KeyPress);
            this.ATTBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ATTBox1_KeyUp);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(74, 4);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(20, 13);
            this.label22.TabIndex = 23;
            this.label22.Text = "dB";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(29, 3);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(46, 20);
            this.textBox2.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "dB";
            // 
            // panel13
            // 
            this.panel13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel13.BackColor = System.Drawing.Color.Gainsboro;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel13.Controls.Add(this.label17);
            this.panel13.Location = new System.Drawing.Point(667, 337);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(84, 21);
            this.panel13.TabIndex = 47;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(3, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 15);
            this.label17.TabIndex = 26;
            this.label17.Text = "0...31dB";
            // 
            // panel12
            // 
            this.panel12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel12.BackColor = System.Drawing.Color.Gainsboro;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel12.Controls.Add(this.label16);
            this.panel12.Location = new System.Drawing.Point(667, 152);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(84, 21);
            this.panel12.TabIndex = 46;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 15);
            this.label16.TabIndex = 26;
            this.label16.Text = "0...31dB";
            // 
            // panel11
            // 
            this.panel11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel11.BackColor = System.Drawing.Color.Gainsboro;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Controls.Add(this.label15);
            this.panel11.Location = new System.Drawing.Point(1153, 251);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(92, 21);
            this.panel11.TabIndex = 45;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 15);
            this.label15.TabIndex = 26;
            this.label15.Text = "-12...+22dB";
            // 
            // panel10
            // 
            this.panel10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel10.BackColor = System.Drawing.Color.Gainsboro;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Controls.Add(this.label13);
            this.panel10.Controls.Add(this.label14);
            this.panel10.Location = new System.Drawing.Point(1053, 357);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(84, 47);
            this.panel10.TabIndex = 44;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(1, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "F = 30 MHZ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(3, 2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "F = 75MHz";
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.BackColor = System.Drawing.Color.Gainsboro;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.label12);
            this.panel9.Location = new System.Drawing.Point(789, 423);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(84, 47);
            this.panel9.TabIndex = 43;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(1, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "F = 30 MHZ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(3, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "F = 704MHz";
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.BackColor = System.Drawing.Color.Gainsboro;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.label9);
            this.panel8.Location = new System.Drawing.Point(789, 241);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(84, 47);
            this.panel8.TabIndex = 41;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(1, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "F = 30 MHZ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(0, 2);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "F = 1500MHz";
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.Color.Gainsboro;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.label7);
            this.panel7.Location = new System.Drawing.Point(491, 413);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(49, 17);
            this.panel7.TabIndex = 42;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(-2, -2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 15);
            this.label7.TabIndex = 27;
            this.label7.Text = "10 dB";
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.Gainsboro;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.label8);
            this.panel6.Location = new System.Drawing.Point(490, 315);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(49, 17);
            this.panel6.TabIndex = 41;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(3, -2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 15);
            this.label8.TabIndex = 26;
            this.label8.Text = "0 dB";
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackColor = System.Drawing.Color.Gainsboro;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.label6);
            this.panel5.Location = new System.Drawing.Point(491, 231);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(49, 17);
            this.panel5.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(-2, -2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 15);
            this.label6.TabIndex = 27;
            this.label6.Text = "10 dB";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.Gainsboro;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label5);
            this.panel4.Location = new System.Drawing.Point(491, 139);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(49, 17);
            this.panel4.TabIndex = 39;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(3, -2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 15);
            this.label5.TabIndex = 26;
            this.label5.Text = "0 dB";
            // 
            // LedATT4
            // 
            this.LedATT4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LedATT4.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.LedATT4.Location = new System.Drawing.Point(456, 399);
            this.LedATT4.Name = "LedATT4";
            this.LedATT4.OffColor = System.Drawing.Color.Crimson;
            this.LedATT4.Size = new System.Drawing.Size(27, 28);
            this.LedATT4.TabIndex = 37;
            // 
            // LedATT3
            // 
            this.LedATT3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LedATT3.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.LedATT3.Location = new System.Drawing.Point(456, 324);
            this.LedATT3.Name = "LedATT3";
            this.LedATT3.OffColor = System.Drawing.Color.Crimson;
            this.LedATT3.Size = new System.Drawing.Size(27, 28);
            this.LedATT3.TabIndex = 36;
            // 
            // LedATT2
            // 
            this.LedATT2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LedATT2.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.LedATT2.Location = new System.Drawing.Point(456, 220);
            this.LedATT2.Name = "LedATT2";
            this.LedATT2.OffColor = System.Drawing.Color.Crimson;
            this.LedATT2.Size = new System.Drawing.Size(27, 28);
            this.LedATT2.TabIndex = 35;
            // 
            // LedATT1
            // 
            this.LedATT1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LedATT1.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.LedATT1.Location = new System.Drawing.Point(456, 145);
            this.LedATT1.Name = "LedATT1";
            this.LedATT1.OffColor = System.Drawing.Color.Crimson;
            this.LedATT1.Size = new System.Drawing.Size(27, 28);
            this.LedATT1.TabIndex = 34;
            // 
            // ATT4
            // 
            this.ATT4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ATT4.BackColor = System.Drawing.Color.LightGray;
            this.ATT4.BackgroundImage = global::RPU.Properties.Resources.ATT;
            this.ATT4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ATT4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ATT4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ATT4.Location = new System.Drawing.Point(489, 380);
            this.ATT4.Name = "ATT4";
            this.ATT4.Size = new System.Drawing.Size(51, 27);
            this.ATT4.TabIndex = 33;
            this.ATT4.UseVisualStyleBackColor = false;
            this.ATT4.Click += new System.EventHandler(this.ATT4_Click);
            // 
            // ATT3
            // 
            this.ATT3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ATT3.BackColor = System.Drawing.Color.LightGray;
            this.ATT3.BackgroundImage = global::RPU.Properties.Resources.ATT;
            this.ATT3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ATT3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ATT3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ATT3.Location = new System.Drawing.Point(489, 338);
            this.ATT3.Name = "ATT3";
            this.ATT3.Size = new System.Drawing.Size(51, 27);
            this.ATT3.TabIndex = 32;
            this.ATT3.UseVisualStyleBackColor = false;
            this.ATT3.Click += new System.EventHandler(this.ATT3_Click);
            // 
            // ATT2
            // 
            this.ATT2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ATT2.BackColor = System.Drawing.Color.LightGray;
            this.ATT2.BackgroundImage = global::RPU.Properties.Resources.ATT;
            this.ATT2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ATT2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ATT2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ATT2.Location = new System.Drawing.Point(489, 203);
            this.ATT2.Name = "ATT2";
            this.ATT2.Size = new System.Drawing.Size(51, 27);
            this.ATT2.TabIndex = 31;
            this.ATT2.UseVisualStyleBackColor = false;
            this.ATT2.Click += new System.EventHandler(this.ATT2_Click);
            // 
            // ATT1
            // 
            this.ATT1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ATT1.BackColor = System.Drawing.Color.LightGray;
            this.ATT1.BackgroundImage = global::RPU.Properties.Resources.ATT;
            this.ATT1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ATT1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ATT1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ATT1.Location = new System.Drawing.Point(489, 161);
            this.ATT1.Name = "ATT1";
            this.ATT1.Size = new System.Drawing.Size(51, 27);
            this.ATT1.TabIndex = 30;
            this.ATT1.UseVisualStyleBackColor = false;
            this.ATT1.Click += new System.EventHandler(this.ATT1_Click);
            // 
            // panelATT2
            // 
            this.panelATT2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelATT2.BackColor = System.Drawing.Color.Gainsboro;
            this.panelATT2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelATT2.Controls.Add(this.MinusATT2);
            this.panelATT2.Controls.Add(this.PlusATT2);
            this.panelATT2.Controls.Add(this.ATTBox2);
            this.panelATT2.Controls.Add(this.label4);
            this.panelATT2.Location = new System.Drawing.Point(648, 402);
            this.panelATT2.Name = "panelATT2";
            this.panelATT2.Size = new System.Drawing.Size(123, 29);
            this.panelATT2.TabIndex = 25;
            // 
            // MinusATT2
            // 
            this.MinusATT2.BackgroundImage = global::RPU.Properties.Resources.bfa_minus_simple_aqua_48x48;
            this.MinusATT2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MinusATT2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinusATT2.ForeColor = System.Drawing.Color.Gainsboro;
            this.MinusATT2.Location = new System.Drawing.Point(-2, 0);
            this.MinusATT2.Name = "MinusATT2";
            this.MinusATT2.Size = new System.Drawing.Size(26, 28);
            this.MinusATT2.TabIndex = 50;
            this.MinusATT2.UseVisualStyleBackColor = true;
            this.MinusATT2.Click += new System.EventHandler(this.MinusATT2_Click);
            // 
            // PlusATT2
            // 
            this.PlusATT2.BackgroundImage = global::RPU.Properties.Resources.bfa_plus_simple_aqua_48x48;
            this.PlusATT2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlusATT2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlusATT2.ForeColor = System.Drawing.Color.Gainsboro;
            this.PlusATT2.Location = new System.Drawing.Point(90, 0);
            this.PlusATT2.Name = "PlusATT2";
            this.PlusATT2.Size = new System.Drawing.Size(29, 26);
            this.PlusATT2.TabIndex = 51;
            this.PlusATT2.UseVisualStyleBackColor = true;
            this.PlusATT2.Click += new System.EventHandler(this.PlusATT2_Click);
            // 
            // ATTBox2
            // 
            this.ATTBox2.Location = new System.Drawing.Point(29, 3);
            this.ATTBox2.Name = "ATTBox2";
            this.ATTBox2.Size = new System.Drawing.Size(38, 20);
            this.ATTBox2.TabIndex = 22;
            this.ATTBox2.TextChanged += new System.EventHandler(this.ATTBox2_TextChanged);
            this.ATTBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ATTBox2_KeyPress);
            this.ATTBox2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ATTBox2_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(71, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "dB";
            // 
            // Switch9
            // 
            this.Switch9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Switch9.BackgroundImage = global::RPU.Properties.Resources.ON2;
            this.Switch9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Switch9.Location = new System.Drawing.Point(564, 364);
            this.Switch9.Name = "Switch9";
            this.Switch9.Size = new System.Drawing.Size(85, 20);
            this.Switch9.TabIndex = 10;
            this.Switch9.TabStop = false;
            // 
            // Switch7
            // 
            this.Switch7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Switch7.BackgroundImage = global::RPU.Properties.Resources.ON2;
            this.Switch7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Switch7.Location = new System.Drawing.Point(317, 361);
            this.Switch7.Name = "Switch7";
            this.Switch7.Size = new System.Drawing.Size(83, 24);
            this.Switch7.TabIndex = 9;
            this.Switch7.TabStop = false;
            // 
            // Switch8
            // 
            this.Switch8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Switch8.BackgroundImage = global::RPU.Properties.Resources.ON1;
            this.Switch8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Switch8.Location = new System.Drawing.Point(369, 361);
            this.Switch8.Name = "Switch8";
            this.Switch8.Size = new System.Drawing.Size(83, 24);
            this.Switch8.TabIndex = 8;
            this.Switch8.TabStop = false;
            // 
            // Switch6
            // 
            this.Switch6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Switch6.BackgroundImage = global::RPU.Properties.Resources.ON1;
            this.Switch6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Switch6.Location = new System.Drawing.Point(100, 362);
            this.Switch6.Name = "Switch6";
            this.Switch6.Size = new System.Drawing.Size(83, 24);
            this.Switch6.TabIndex = 7;
            this.Switch6.TabStop = false;
            // 
            // Switch5
            // 
            this.Switch5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Switch5.BackgroundImage = global::RPU.Properties.Resources.ON1;
            this.Switch5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Switch5.Location = new System.Drawing.Point(621, 258);
            this.Switch5.Name = "Switch5";
            this.Switch5.Size = new System.Drawing.Size(86, 38);
            this.Switch5.TabIndex = 6;
            this.Switch5.TabStop = false;
            // 
            // Switch4
            // 
            this.Switch4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Switch4.BackgroundImage = global::RPU.Properties.Resources.ON2;
            this.Switch4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Switch4.Location = new System.Drawing.Point(882, 289);
            this.Switch4.Name = "Switch4";
            this.Switch4.Size = new System.Drawing.Size(86, 29);
            this.Switch4.TabIndex = 5;
            this.Switch4.TabStop = false;
            // 
            // Switch3
            // 
            this.Switch3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Switch3.BackgroundImage = global::RPU.Properties.Resources.ON2;
            this.Switch3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Switch3.Location = new System.Drawing.Point(564, 182);
            this.Switch3.Name = "Switch3";
            this.Switch3.Size = new System.Drawing.Size(85, 23);
            this.Switch3.TabIndex = 4;
            this.Switch3.TabStop = false;
            // 
            // Switch2
            // 
            this.Switch2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Switch2.BackgroundImage = global::RPU.Properties.Resources.ON1;
            this.Switch2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Switch2.Location = new System.Drawing.Point(364, 182);
            this.Switch2.Name = "Switch2";
            this.Switch2.Size = new System.Drawing.Size(86, 23);
            this.Switch2.TabIndex = 3;
            this.Switch2.TabStop = false;
            // 
            // Switch1
            // 
            this.Switch1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Switch1.BackgroundImage = global::RPU.Properties.Resources.ON1;
            this.Switch1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Switch1.Location = new System.Drawing.Point(23, 183);
            this.Switch1.Name = "Switch1";
            this.Switch1.Size = new System.Drawing.Size(76, 21);
            this.Switch1.TabIndex = 2;
            this.Switch1.TabStop = false;
            // 
            // LO2
            // 
            this.LO2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LO2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LO2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO2.Location = new System.Drawing.Point(909, 365);
            this.LO2.Name = "LO2";
            this.LO2.Size = new System.Drawing.Size(84, 39);
            this.LO2.TabIndex = 1;
            this.LO2.Text = "LO2";
            this.LO2.UseVisualStyleBackColor = true;
            this.LO2.Click += new System.EventHandler(this.LO2_Click);
            // 
            // LO1
            // 
            this.LO1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LO1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LO1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO1.Location = new System.Drawing.Point(526, 263);
            this.LO1.Name = "LO1";
            this.LO1.Size = new System.Drawing.Size(84, 39);
            this.LO1.TabIndex = 0;
            this.LO1.Text = "LO1";
            this.LO1.UseVisualStyleBackColor = true;
            this.LO1.Click += new System.EventHandler(this.LO1_Click);
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape56,
            this.lineShape55,
            this.lineShape54,
            this.lineShape53,
            this.lineShape50,
            this.lineShape49,
            this.rectangleShape17,
            this.rectangleShape16,
            this.rectangleShape15,
            this.rectangleShape14,
            this.rectangleShape13,
            this.rectangleShape12,
            this.lineShape48,
            this.lineShape47,
            this.lineShape46,
            this.lineShape45,
            this.ovalShape1,
            this.lineShape44,
            this.lineShape43,
            this.lineShape42,
            this.lineShape41,
            this.lineShape40,
            this.lineShape39,
            this.lineShape38,
            this.lineShape37,
            this.lineShape36,
            this.lineShape35,
            this.lineShape34,
            this.lineShape33,
            this.lineShape32,
            this.lineShape31,
            this.lineShape30,
            this.lineShape29,
            this.lineShape28,
            this.lineShape27,
            this.lineShape26,
            this.rectangleShape11,
            this.lineShape1,
            this.rectangleShape26,
            this.rectangleShape25,
            this.rectangleShape24,
            this.lineShape52,
            this.lineShape51,
            this.rectangleShape10,
            this.lineShape25,
            this.lineShape24,
            this.rectangleShape9,
            this.lineShape23,
            this.lineShape22,
            this.rectangleShape8,
            this.lineShape21,
            this.lineShape20,
            this.rectangleShape7,
            this.lineShape19,
            this.lineShape18,
            this.rectangleShape6,
            this.lineShape17,
            this.lineShape16,
            this.rectangleShape5,
            this.lineShape15,
            this.lineShape14,
            this.lineShape13,
            this.rectangleShape4,
            this.lineShape12,
            this.lineShape11,
            this.lineShape10,
            this.lineShape9,
            this.lineShape8,
            this.lineShape7,
            this.lineShape6,
            this.lineShape5,
            this.lineShape4,
            this.lineShape3,
            this.lineShape2,
            this.rectangleShape3,
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(1314, 491);
            this.shapeContainer2.TabIndex = 73;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape56
            // 
            this.lineShape56.Name = "lineShape56";
            this.lineShape56.X1 = 640;
            this.lineShape56.X2 = 676;
            this.lineShape56.Y1 = 196;
            this.lineShape56.Y2 = 196;
            // 
            // lineShape55
            // 
            this.lineShape55.Name = "lineShape55";
            this.lineShape55.X1 = 1213;
            this.lineShape55.X2 = 1170;
            this.lineShape55.Y1 = 286;
            this.lineShape55.Y2 = 330;
            // 
            // lineShape54
            // 
            this.lineShape54.Name = "lineShape54";
            this.lineShape54.X1 = 677;
            this.lineShape54.X2 = 737;
            this.lineShape54.Y1 = 377;
            this.lineShape54.Y2 = 377;
            // 
            // lineShape53
            // 
            this.lineShape53.Name = "lineShape53";
            this.lineShape53.X1 = 674;
            this.lineShape53.X2 = 734;
            this.lineShape53.Y1 = 196;
            this.lineShape53.Y2 = 196;
            // 
            // lineShape50
            // 
            this.lineShape50.Name = "lineShape50";
            this.lineShape50.X1 = 718;
            this.lineShape50.X2 = 689;
            this.lineShape50.Y1 = 362;
            this.lineShape50.Y2 = 392;
            // 
            // lineShape49
            // 
            this.lineShape49.Name = "lineShape49";
            this.lineShape49.X1 = 718;
            this.lineShape49.X2 = 689;
            this.lineShape49.Y1 = 181;
            this.lineShape49.Y2 = 211;
            // 
            // rectangleShape17
            // 
            this.rectangleShape17.Location = new System.Drawing.Point(1166, 274);
            this.rectangleShape17.Name = "rectangleShape17";
            this.rectangleShape17.Size = new System.Drawing.Size(62, 64);
            // 
            // rectangleShape16
            // 
            this.rectangleShape16.Location = new System.Drawing.Point(1057, 274);
            this.rectangleShape16.Name = "rectangleShape16";
            this.rectangleShape16.Size = new System.Drawing.Size(64, 64);
            // 
            // rectangleShape15
            // 
            this.rectangleShape15.Location = new System.Drawing.Point(794, 344);
            this.rectangleShape15.Name = "rectangleShape15";
            this.rectangleShape15.Size = new System.Drawing.Size(64, 66);
            // 
            // rectangleShape14
            // 
            this.rectangleShape14.Location = new System.Drawing.Point(794, 164);
            this.rectangleShape14.Name = "rectangleShape14";
            this.rectangleShape14.Size = new System.Drawing.Size(64, 64);
            // 
            // rectangleShape13
            // 
            this.rectangleShape13.Location = new System.Drawing.Point(674, 358);
            this.rectangleShape13.Name = "rectangleShape13";
            this.rectangleShape13.Size = new System.Drawing.Size(61, 36);
            // 
            // rectangleShape12
            // 
            this.rectangleShape12.Location = new System.Drawing.Point(749, 362);
            this.rectangleShape12.Name = "rectangleShape12";
            this.rectangleShape12.Size = new System.Drawing.Size(30, 29);
            // 
            // lineShape48
            // 
            this.lineShape48.Name = "lineShape48";
            this.lineShape48.X1 = 749;
            this.lineShape48.X2 = 778;
            this.lineShape48.Y1 = 362;
            this.lineShape48.Y2 = 390;
            // 
            // lineShape47
            // 
            this.lineShape47.Name = "lineShape47";
            this.lineShape47.X1 = 779;
            this.lineShape47.X2 = 752;
            this.lineShape47.Y1 = 361;
            this.lineShape47.Y2 = 389;
            // 
            // lineShape46
            // 
            this.lineShape46.Name = "lineShape46";
            this.lineShape46.X1 = 778;
            this.lineShape46.X2 = 751;
            this.lineShape46.Y1 = 180;
            this.lineShape46.Y2 = 208;
            // 
            // lineShape45
            // 
            this.lineShape45.Name = "lineShape45";
            this.lineShape45.X1 = 748;
            this.lineShape45.X2 = 777;
            this.lineShape45.Y1 = 181;
            this.lineShape45.Y2 = 209;
            // 
            // ovalShape1
            // 
            this.ovalShape1.Location = new System.Drawing.Point(1265, 300);
            this.ovalShape1.Name = "ovalShape1";
            this.ovalShape1.Size = new System.Drawing.Size(14, 13);
            // 
            // lineShape44
            // 
            this.lineShape44.Name = "lineShape44";
            this.lineShape44.X1 = 1264;
            this.lineShape44.X2 = 1282;
            this.lineShape44.Y1 = 313;
            this.lineShape44.Y2 = 313;
            // 
            // lineShape43
            // 
            this.lineShape43.Name = "lineShape43";
            this.lineShape43.X1 = 1227;
            this.lineShape43.X2 = 1306;
            this.lineShape43.Y1 = 306;
            this.lineShape43.Y2 = 306;
            // 
            // lineShape42
            // 
            this.lineShape42.Name = "lineShape42";
            this.lineShape42.X1 = 1122;
            this.lineShape42.X2 = 1164;
            this.lineShape42.Y1 = 307;
            this.lineShape42.Y2 = 307;
            // 
            // lineShape41
            // 
            this.lineShape41.Name = "lineShape41";
            this.lineShape41.X1 = 1029;
            this.lineShape41.X2 = 1056;
            this.lineShape41.Y1 = 307;
            this.lineShape41.Y2 = 307;
            // 
            // lineShape40
            // 
            this.lineShape40.Name = "lineShape40";
            this.lineShape40.X1 = 933;
            this.lineShape40.X2 = 997;
            this.lineShape40.Y1 = 307;
            this.lineShape40.Y2 = 307;
            // 
            // lineShape39
            // 
            this.lineShape39.Name = "lineShape39";
            this.lineShape39.X1 = 682;
            this.lineShape39.X2 = 761;
            this.lineShape39.Y1 = 279;
            this.lineShape39.Y2 = 279;
            // 
            // lineShape38
            // 
            this.lineShape38.Name = "lineShape38";
            this.lineShape38.X1 = 683;
            this.lineShape38.X2 = 762;
            this.lineShape38.Y1 = 266;
            this.lineShape38.Y2 = 266;
            // 
            // lineShape37
            // 
            this.lineShape37.Name = "lineShape37";
            this.lineShape37.X1 = 637;
            this.lineShape37.X2 = 673;
            this.lineShape37.Y1 = 377;
            this.lineShape37.Y2 = 377;
            // 
            // lineShape36
            // 
            this.lineShape36.Name = "lineShape36";
            this.lineShape36.X1 = 735;
            this.lineShape36.X2 = 746;
            this.lineShape36.Y1 = 376;
            this.lineShape36.Y2 = 376;
            // 
            // lineShape35
            // 
            this.lineShape35.Name = "lineShape35";
            this.lineShape35.X1 = 780;
            this.lineShape35.X2 = 793;
            this.lineShape35.Y1 = 376;
            this.lineShape35.Y2 = 376;
            // 
            // lineShape34
            // 
            this.lineShape34.Name = "lineShape34";
            this.lineShape34.X1 = 1002;
            this.lineShape34.X2 = 1014;
            this.lineShape34.Y1 = 383;
            this.lineShape34.Y2 = 383;
            // 
            // lineShape33
            // 
            this.lineShape33.Name = "lineShape33";
            this.lineShape33.X1 = 1016;
            this.lineShape33.X2 = 1016;
            this.lineShape33.Y1 = 325;
            this.lineShape33.Y2 = 383;
            // 
            // lineShape32
            // 
            this.lineShape32.Name = "lineShape32";
            this.lineShape32.X1 = 878;
            this.lineShape32.X2 = 877;
            this.lineShape32.Y1 = 309;
            this.lineShape32.Y2 = 376;
            // 
            // lineShape31
            // 
            this.lineShape31.Name = "lineShape31";
            this.lineShape31.X1 = 860;
            this.lineShape31.X2 = 878;
            this.lineShape31.Y1 = 376;
            this.lineShape31.Y2 = 376;
            // 
            // lineShape30
            // 
            this.lineShape30.Name = "lineShape30";
            this.lineShape30.X1 = 860;
            this.lineShape30.X2 = 878;
            this.lineShape30.Y1 = 197;
            this.lineShape30.Y2 = 197;
            // 
            // lineShape29
            // 
            this.lineShape29.Name = "lineShape29";
            this.lineShape29.X1 = 879;
            this.lineShape29.X2 = 878;
            this.lineShape29.Y1 = 196;
            this.lineShape29.Y2 = 295;
            // 
            // lineShape28
            // 
            this.lineShape28.Name = "lineShape28";
            this.lineShape28.X1 = 763;
            this.lineShape28.X2 = 763;
            this.lineShape28.Y1 = 279;
            this.lineShape28.Y2 = 363;
            // 
            // lineShape27
            // 
            this.lineShape27.Name = "lineShape27";
            this.lineShape27.X1 = 763;
            this.lineShape27.X2 = 763;
            this.lineShape27.Y1 = 212;
            this.lineShape27.Y2 = 266;
            // 
            // lineShape26
            // 
            this.lineShape26.Name = "lineShape26";
            this.lineShape26.X1 = 735;
            this.lineShape26.X2 = 746;
            this.lineShape26.Y1 = 195;
            this.lineShape26.Y2 = 195;
            // 
            // rectangleShape11
            // 
            this.rectangleShape11.Location = new System.Drawing.Point(748, 181);
            this.rectangleShape11.Name = "rectangleShape11";
            this.rectangleShape11.Size = new System.Drawing.Size(30, 29);
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 779;
            this.lineShape1.X2 = 790;
            this.lineShape1.Y1 = 196;
            this.lineShape1.Y2 = 196;
            // 
            // rectangleShape26
            // 
            this.rectangleShape26.Location = new System.Drawing.Point(185, 377);
            this.rectangleShape26.Name = "rectangleShape26";
            this.rectangleShape26.Size = new System.Drawing.Size(124, 26);
            // 
            // rectangleShape25
            // 
            this.rectangleShape25.Location = new System.Drawing.Point(185, 335);
            this.rectangleShape25.Name = "rectangleShape25";
            this.rectangleShape25.Size = new System.Drawing.Size(122, 27);
            // 
            // rectangleShape24
            // 
            this.rectangleShape24.Location = new System.Drawing.Point(183, 288);
            this.rectangleShape24.Name = "rectangleShape24";
            this.rectangleShape24.Size = new System.Drawing.Size(132, 24);
            // 
            // lineShape52
            // 
            this.lineShape52.Name = "lineShape52";
            this.lineShape52.X1 = 166;
            this.lineShape52.X2 = 182;
            this.lineShape52.Y1 = 301;
            this.lineShape52.Y2 = 301;
            this.lineShape52.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // lineShape51
            // 
            this.lineShape51.Name = "lineShape51";
            this.lineShape51.X1 = 316;
            this.lineShape51.X2 = 332;
            this.lineShape51.Y1 = 301;
            this.lineShape51.Y2 = 301;
            this.lineShape51.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // rectangleShape10
            // 
            this.rectangleShape10.Location = new System.Drawing.Point(182, 260);
            this.rectangleShape10.Name = "rectangleShape10";
            this.rectangleShape10.Size = new System.Drawing.Size(132, 21);
            // 
            // lineShape25
            // 
            this.lineShape25.Name = "lineShape25";
            this.lineShape25.X1 = 165;
            this.lineShape25.X2 = 181;
            this.lineShape25.Y1 = 271;
            this.lineShape25.Y2 = 271;
            this.lineShape25.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // lineShape24
            // 
            this.lineShape24.Name = "lineShape24";
            this.lineShape24.X1 = 315;
            this.lineShape24.X2 = 331;
            this.lineShape24.Y1 = 270;
            this.lineShape24.Y2 = 270;
            this.lineShape24.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // rectangleShape9
            // 
            this.rectangleShape9.Location = new System.Drawing.Point(183, 229);
            this.rectangleShape9.Name = "rectangleShape9";
            this.rectangleShape9.Size = new System.Drawing.Size(132, 23);
            // 
            // lineShape23
            // 
            this.lineShape23.Name = "lineShape23";
            this.lineShape23.X1 = 166;
            this.lineShape23.X2 = 182;
            this.lineShape23.Y1 = 241;
            this.lineShape23.Y2 = 241;
            this.lineShape23.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // lineShape22
            // 
            this.lineShape22.Name = "lineShape22";
            this.lineShape22.X1 = 316;
            this.lineShape22.X2 = 332;
            this.lineShape22.Y1 = 241;
            this.lineShape22.Y2 = 241;
            this.lineShape22.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // rectangleShape8
            // 
            this.rectangleShape8.Location = new System.Drawing.Point(183, 199);
            this.rectangleShape8.Name = "rectangleShape8";
            this.rectangleShape8.Size = new System.Drawing.Size(132, 23);
            // 
            // lineShape21
            // 
            this.lineShape21.Name = "lineShape21";
            this.lineShape21.X1 = 166;
            this.lineShape21.X2 = 182;
            this.lineShape21.Y1 = 211;
            this.lineShape21.Y2 = 211;
            this.lineShape21.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // lineShape20
            // 
            this.lineShape20.Name = "lineShape20";
            this.lineShape20.X1 = 316;
            this.lineShape20.X2 = 332;
            this.lineShape20.Y1 = 211;
            this.lineShape20.Y2 = 211;
            this.lineShape20.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // rectangleShape7
            // 
            this.rectangleShape7.Location = new System.Drawing.Point(182, 170);
            this.rectangleShape7.Name = "rectangleShape7";
            this.rectangleShape7.Size = new System.Drawing.Size(134, 23);
            // 
            // lineShape19
            // 
            this.lineShape19.Name = "lineShape19";
            this.lineShape19.X1 = 166;
            this.lineShape19.X2 = 182;
            this.lineShape19.Y1 = 181;
            this.lineShape19.Y2 = 181;
            this.lineShape19.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // lineShape18
            // 
            this.lineShape18.Name = "lineShape18";
            this.lineShape18.X1 = 317;
            this.lineShape18.X2 = 333;
            this.lineShape18.Y1 = 182;
            this.lineShape18.Y2 = 182;
            this.lineShape18.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // rectangleShape6
            // 
            this.rectangleShape6.Location = new System.Drawing.Point(183, 140);
            this.rectangleShape6.Name = "rectangleShape6";
            this.rectangleShape6.Size = new System.Drawing.Size(132, 23);
            // 
            // lineShape17
            // 
            this.lineShape17.Name = "lineShape17";
            this.lineShape17.X1 = 166;
            this.lineShape17.X2 = 182;
            this.lineShape17.Y1 = 152;
            this.lineShape17.Y2 = 152;
            this.lineShape17.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // lineShape16
            // 
            this.lineShape16.Name = "lineShape16";
            this.lineShape16.X1 = 316;
            this.lineShape16.X2 = 332;
            this.lineShape16.Y1 = 152;
            this.lineShape16.Y2 = 152;
            this.lineShape16.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // rectangleShape5
            // 
            this.rectangleShape5.Location = new System.Drawing.Point(182, 111);
            this.rectangleShape5.Name = "rectangleShape5";
            this.rectangleShape5.Size = new System.Drawing.Size(132, 23);
            // 
            // lineShape15
            // 
            this.lineShape15.Name = "lineShape15";
            this.lineShape15.X1 = 165;
            this.lineShape15.X2 = 181;
            this.lineShape15.Y1 = 122;
            this.lineShape15.Y2 = 122;
            this.lineShape15.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // lineShape14
            // 
            this.lineShape14.Name = "lineShape14";
            this.lineShape14.X1 = 315;
            this.lineShape14.X2 = 331;
            this.lineShape14.Y1 = 122;
            this.lineShape14.Y2 = 122;
            this.lineShape14.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // lineShape13
            // 
            this.lineShape13.Name = "lineShape13";
            this.lineShape13.X1 = 315;
            this.lineShape13.X2 = 331;
            this.lineShape13.Y1 = 92;
            this.lineShape13.Y2 = 92;
            this.lineShape13.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // rectangleShape4
            // 
            this.rectangleShape4.Location = new System.Drawing.Point(182, 81);
            this.rectangleShape4.Name = "rectangleShape4";
            this.rectangleShape4.Size = new System.Drawing.Size(132, 23);
            // 
            // lineShape12
            // 
            this.lineShape12.Name = "lineShape12";
            this.lineShape12.X1 = 165;
            this.lineShape12.X2 = 181;
            this.lineShape12.Y1 = 92;
            this.lineShape12.Y2 = 92;
            this.lineShape12.Click += new System.EventHandler(this.lineShape10_Click);
            // 
            // lineShape11
            // 
            this.lineShape11.Name = "lineShape11";
            this.lineShape11.X1 = 345;
            this.lineShape11.X2 = 344;
            this.lineShape11.Y1 = 91;
            this.lineShape11.Y2 = 309;
            // 
            // lineShape10
            // 
            this.lineShape10.Name = "lineShape10";
            this.lineShape10.X1 = 154;
            this.lineShape10.X2 = 153;
            this.lineShape10.Y1 = 93;
            this.lineShape10.Y2 = 311;
            // 
            // lineShape9
            // 
            this.lineShape9.Name = "lineShape9";
            this.lineShape9.X1 = 96;
            this.lineShape9.X2 = 154;
            this.lineShape9.Y1 = 161;
            this.lineShape9.Y2 = 161;
            // 
            // lineShape8
            // 
            this.lineShape8.Name = "lineShape8";
            this.lineShape8.X1 = 95;
            this.lineShape8.X2 = 95;
            this.lineShape8.Y1 = 161;
            this.lineShape8.Y2 = 184;
            // 
            // lineShape7
            // 
            this.lineShape7.Name = "lineShape7";
            this.lineShape7.X1 = 9;
            this.lineShape7.X2 = 20;
            this.lineShape7.Y1 = 195;
            this.lineShape7.Y2 = 195;
            // 
            // lineShape6
            // 
            this.lineShape6.Name = "lineShape6";
            this.lineShape6.X1 = -3;
            this.lineShape6.X2 = 10;
            this.lineShape6.Y1 = 87;
            this.lineShape6.Y2 = 108;
            // 
            // lineShape5
            // 
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.X1 = 11;
            this.lineShape5.X2 = 19;
            this.lineShape5.Y1 = 107;
            this.lineShape5.Y2 = 90;
            // 
            // lineShape4
            // 
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 10;
            this.lineShape4.X2 = 9;
            this.lineShape4.Y1 = 85;
            this.lineShape4.Y2 = 194;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 95;
            this.lineShape3.X2 = 95;
            this.lineShape3.Y1 = 200;
            this.lineShape3.Y2 = 381;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 345;
            this.lineShape2.X2 = 365;
            this.lineShape2.Y1 = 199;
            this.lineShape2.Y2 = 199;
            // 
            // rectangleShape3
            // 
            this.rectangleShape3.Location = new System.Drawing.Point(447, 171);
            this.rectangleShape3.Name = "rectangleShape3";
            this.rectangleShape3.Size = new System.Drawing.Size(112, 43);
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.Location = new System.Drawing.Point(449, 349);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(112, 41);
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.Location = new System.Drawing.Point(673, 178);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(61, 34);
            // 
            // Heter1
            // 
            this.Heter1.BackColor = System.Drawing.Color.LightGray;
            this.Heter1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Heter1.BackgroundImage")));
            this.Heter1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Heter1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Heter1.Controls.Add(this.LO1_SignalLevel);
            this.Heter1.Controls.Add(this.LO1_Peleng);
            this.Heter1.Controls.Add(this.label34);
            this.Heter1.Controls.Add(this.label25);
            this.Heter1.Controls.Add(this.LOHeter1);
            this.Heter1.Controls.Add(this.panelATTLo1);
            this.Heter1.Controls.Add(this.panel14);
            this.Heter1.Controls.Add(this.LO1_PF4);
            this.Heter1.Controls.Add(this.LO1_PF3);
            this.Heter1.Controls.Add(this.LO1_PF2);
            this.Heter1.Controls.Add(this.LO1_PF1);
            this.Heter1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Heter1.Location = new System.Drawing.Point(4, 22);
            this.Heter1.Name = "Heter1";
            this.Heter1.Padding = new System.Windows.Forms.Padding(3);
            this.Heter1.Size = new System.Drawing.Size(1320, 497);
            this.Heter1.TabIndex = 1;
            this.Heter1.Text = "LO1";
            this.Heter1.Click += new System.EventHandler(this.Heter1_Click);
            // 
            // LO1_SignalLevel
            // 
            this.LO1_SignalLevel.AutoSize = true;
            this.LO1_SignalLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO1_SignalLevel.Location = new System.Drawing.Point(923, 282);
            this.LO1_SignalLevel.Name = "LO1_SignalLevel";
            this.LO1_SignalLevel.Size = new System.Drawing.Size(121, 15);
            this.LO1_SignalLevel.TabIndex = 55;
            this.LO1_SignalLevel.Text = "Уровень сигнала";
            // 
            // LO1_Peleng
            // 
            this.LO1_Peleng.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LO1_Peleng.Controls.Add(this.LO1_SignalLevel5);
            this.LO1_Peleng.Controls.Add(this.LO1_SignalLevel4);
            this.LO1_Peleng.Controls.Add(this.LO1_SignalLevel3);
            this.LO1_Peleng.Controls.Add(this.LO1_SignalLevel2);
            this.LO1_Peleng.Controls.Add(this.MinusLO1_URP5);
            this.LO1_Peleng.Controls.Add(this.LO1_URP5);
            this.LO1_Peleng.Controls.Add(this.PlusLO1_URP5);
            this.LO1_Peleng.Controls.Add(this.label43);
            this.LO1_Peleng.Controls.Add(this.MinusLO1_URP4);
            this.LO1_Peleng.Controls.Add(this.LO1_URP4);
            this.LO1_Peleng.Controls.Add(this.PlusLO1_URP4);
            this.LO1_Peleng.Controls.Add(this.label42);
            this.LO1_Peleng.Controls.Add(this.MinusLO1_URP3);
            this.LO1_Peleng.Controls.Add(this.LO1_URP3);
            this.LO1_Peleng.Controls.Add(this.PlusLO1_URP3);
            this.LO1_Peleng.Controls.Add(this.label41);
            this.LO1_Peleng.Controls.Add(this.MinusLO1_URP2);
            this.LO1_Peleng.Controls.Add(this.LO1_URP2);
            this.LO1_Peleng.Controls.Add(this.label39);
            this.LO1_Peleng.Controls.Add(this.PlusLO1_URP2);
            this.LO1_Peleng.Location = new System.Drawing.Point(780, 312);
            this.LO1_Peleng.Name = "LO1_Peleng";
            this.LO1_Peleng.Size = new System.Drawing.Size(305, 114);
            this.LO1_Peleng.TabIndex = 54;
            this.LO1_Peleng.Visible = false;
            // 
            // LO1_SignalLevel5
            // 
            this.LO1_SignalLevel5.AutoSize = true;
            this.LO1_SignalLevel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO1_SignalLevel5.Location = new System.Drawing.Point(141, 85);
            this.LO1_SignalLevel5.Name = "LO1_SignalLevel5";
            this.LO1_SignalLevel5.Size = new System.Drawing.Size(121, 15);
            this.LO1_SignalLevel5.TabIndex = 75;
            this.LO1_SignalLevel5.Text = "Уровень сигнала";
            // 
            // LO1_SignalLevel4
            // 
            this.LO1_SignalLevel4.AutoSize = true;
            this.LO1_SignalLevel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO1_SignalLevel4.Location = new System.Drawing.Point(141, 59);
            this.LO1_SignalLevel4.Name = "LO1_SignalLevel4";
            this.LO1_SignalLevel4.Size = new System.Drawing.Size(121, 15);
            this.LO1_SignalLevel4.TabIndex = 74;
            this.LO1_SignalLevel4.Text = "Уровень сигнала";
            // 
            // LO1_SignalLevel3
            // 
            this.LO1_SignalLevel3.AutoSize = true;
            this.LO1_SignalLevel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO1_SignalLevel3.Location = new System.Drawing.Point(141, 34);
            this.LO1_SignalLevel3.Name = "LO1_SignalLevel3";
            this.LO1_SignalLevel3.Size = new System.Drawing.Size(121, 15);
            this.LO1_SignalLevel3.TabIndex = 73;
            this.LO1_SignalLevel3.Text = "Уровень сигнала";
            // 
            // LO1_SignalLevel2
            // 
            this.LO1_SignalLevel2.AutoSize = true;
            this.LO1_SignalLevel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO1_SignalLevel2.Location = new System.Drawing.Point(141, 8);
            this.LO1_SignalLevel2.Name = "LO1_SignalLevel2";
            this.LO1_SignalLevel2.Size = new System.Drawing.Size(121, 15);
            this.LO1_SignalLevel2.TabIndex = 72;
            this.LO1_SignalLevel2.Text = "Уровень сигнала";
            // 
            // MinusLO1_URP5
            // 
            this.MinusLO1_URP5.BackgroundImage = global::RPU.Properties.Resources.bfa_minus_simple_aqua_48x48;
            this.MinusLO1_URP5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MinusLO1_URP5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinusLO1_URP5.ForeColor = System.Drawing.Color.Gainsboro;
            this.MinusLO1_URP5.Location = new System.Drawing.Point(0, 79);
            this.MinusLO1_URP5.Name = "MinusLO1_URP5";
            this.MinusLO1_URP5.Size = new System.Drawing.Size(26, 28);
            this.MinusLO1_URP5.TabIndex = 70;
            this.MinusLO1_URP5.UseVisualStyleBackColor = true;
            this.MinusLO1_URP5.Click += new System.EventHandler(this.MinusLO1_URP5_Click);
            // 
            // LO1_URP5
            // 
            this.LO1_URP5.Location = new System.Drawing.Point(26, 81);
            this.LO1_URP5.Name = "LO1_URP5";
            this.LO1_URP5.Size = new System.Drawing.Size(46, 20);
            this.LO1_URP5.TabIndex = 68;
            this.LO1_URP5.TextChanged += new System.EventHandler(this.LO1_URP5_TextChanged);
            this.LO1_URP5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LO1_URP5_KeyPress);
            this.LO1_URP5.KeyUp += new System.Windows.Forms.KeyEventHandler(this.LO1_URP5_KeyUp);
            // 
            // PlusLO1_URP5
            // 
            this.PlusLO1_URP5.BackgroundImage = global::RPU.Properties.Resources.bfa_plus_simple_aqua_48x48;
            this.PlusLO1_URP5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlusLO1_URP5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlusLO1_URP5.ForeColor = System.Drawing.Color.Gainsboro;
            this.PlusLO1_URP5.Location = new System.Drawing.Point(92, 79);
            this.PlusLO1_URP5.Name = "PlusLO1_URP5";
            this.PlusLO1_URP5.Size = new System.Drawing.Size(29, 26);
            this.PlusLO1_URP5.TabIndex = 71;
            this.PlusLO1_URP5.UseVisualStyleBackColor = true;
            this.PlusLO1_URP5.Click += new System.EventHandler(this.PlusLO1_URP5_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(74, 84);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(20, 13);
            this.label43.TabIndex = 69;
            this.label43.Text = "dB";
            // 
            // MinusLO1_URP4
            // 
            this.MinusLO1_URP4.BackgroundImage = global::RPU.Properties.Resources.bfa_minus_simple_aqua_48x48;
            this.MinusLO1_URP4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MinusLO1_URP4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinusLO1_URP4.ForeColor = System.Drawing.Color.Gainsboro;
            this.MinusLO1_URP4.Location = new System.Drawing.Point(0, 53);
            this.MinusLO1_URP4.Name = "MinusLO1_URP4";
            this.MinusLO1_URP4.Size = new System.Drawing.Size(26, 28);
            this.MinusLO1_URP4.TabIndex = 66;
            this.MinusLO1_URP4.UseVisualStyleBackColor = true;
            this.MinusLO1_URP4.Click += new System.EventHandler(this.MinusLO1_URP4_Click);
            // 
            // LO1_URP4
            // 
            this.LO1_URP4.Location = new System.Drawing.Point(26, 55);
            this.LO1_URP4.Name = "LO1_URP4";
            this.LO1_URP4.Size = new System.Drawing.Size(46, 20);
            this.LO1_URP4.TabIndex = 64;
            this.LO1_URP4.TextChanged += new System.EventHandler(this.LO1_URP4_TextChanged);
            this.LO1_URP4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LO1_URP4_KeyPress);
            this.LO1_URP4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.LO1_URP4_KeyUp);
            // 
            // PlusLO1_URP4
            // 
            this.PlusLO1_URP4.BackgroundImage = global::RPU.Properties.Resources.bfa_plus_simple_aqua_48x48;
            this.PlusLO1_URP4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlusLO1_URP4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlusLO1_URP4.ForeColor = System.Drawing.Color.Gainsboro;
            this.PlusLO1_URP4.Location = new System.Drawing.Point(92, 53);
            this.PlusLO1_URP4.Name = "PlusLO1_URP4";
            this.PlusLO1_URP4.Size = new System.Drawing.Size(29, 26);
            this.PlusLO1_URP4.TabIndex = 67;
            this.PlusLO1_URP4.UseVisualStyleBackColor = true;
            this.PlusLO1_URP4.Click += new System.EventHandler(this.PlusLO1_URP4_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(74, 58);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(20, 13);
            this.label42.TabIndex = 65;
            this.label42.Text = "dB";
            // 
            // MinusLO1_URP3
            // 
            this.MinusLO1_URP3.BackgroundImage = global::RPU.Properties.Resources.bfa_minus_simple_aqua_48x48;
            this.MinusLO1_URP3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MinusLO1_URP3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinusLO1_URP3.ForeColor = System.Drawing.Color.Gainsboro;
            this.MinusLO1_URP3.Location = new System.Drawing.Point(0, 27);
            this.MinusLO1_URP3.Name = "MinusLO1_URP3";
            this.MinusLO1_URP3.Size = new System.Drawing.Size(26, 28);
            this.MinusLO1_URP3.TabIndex = 62;
            this.MinusLO1_URP3.UseVisualStyleBackColor = true;
            this.MinusLO1_URP3.Click += new System.EventHandler(this.MinusLO1_URP3_Click);
            // 
            // LO1_URP3
            // 
            this.LO1_URP3.Location = new System.Drawing.Point(26, 29);
            this.LO1_URP3.Name = "LO1_URP3";
            this.LO1_URP3.Size = new System.Drawing.Size(46, 20);
            this.LO1_URP3.TabIndex = 60;
            this.LO1_URP3.TextChanged += new System.EventHandler(this.LO1_URP3_TextChanged);
            this.LO1_URP3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LO1_URP3_KeyPress);
            this.LO1_URP3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.LO1_URP3_KeyUp);
            // 
            // PlusLO1_URP3
            // 
            this.PlusLO1_URP3.BackgroundImage = global::RPU.Properties.Resources.bfa_plus_simple_aqua_48x48;
            this.PlusLO1_URP3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlusLO1_URP3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlusLO1_URP3.ForeColor = System.Drawing.Color.Gainsboro;
            this.PlusLO1_URP3.Location = new System.Drawing.Point(92, 27);
            this.PlusLO1_URP3.Name = "PlusLO1_URP3";
            this.PlusLO1_URP3.Size = new System.Drawing.Size(29, 26);
            this.PlusLO1_URP3.TabIndex = 63;
            this.PlusLO1_URP3.UseVisualStyleBackColor = true;
            this.PlusLO1_URP3.Click += new System.EventHandler(this.PlusLO1_URP3_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(74, 32);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(20, 13);
            this.label41.TabIndex = 61;
            this.label41.Text = "dB";
            // 
            // MinusLO1_URP2
            // 
            this.MinusLO1_URP2.BackgroundImage = global::RPU.Properties.Resources.bfa_minus_simple_aqua_48x48;
            this.MinusLO1_URP2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MinusLO1_URP2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinusLO1_URP2.ForeColor = System.Drawing.Color.Gainsboro;
            this.MinusLO1_URP2.Location = new System.Drawing.Point(0, 1);
            this.MinusLO1_URP2.Name = "MinusLO1_URP2";
            this.MinusLO1_URP2.Size = new System.Drawing.Size(26, 28);
            this.MinusLO1_URP2.TabIndex = 58;
            this.MinusLO1_URP2.UseVisualStyleBackColor = true;
            this.MinusLO1_URP2.Click += new System.EventHandler(this.MinusLO1_URP2_Click);
            // 
            // LO1_URP2
            // 
            this.LO1_URP2.Location = new System.Drawing.Point(26, 3);
            this.LO1_URP2.Name = "LO1_URP2";
            this.LO1_URP2.Size = new System.Drawing.Size(46, 20);
            this.LO1_URP2.TabIndex = 56;
            this.LO1_URP2.TextChanged += new System.EventHandler(this.LO1_URP2_TextChanged);
            this.LO1_URP2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LO1_URP2_KeyPress);
            this.LO1_URP2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.LO1_URP2_KeyUp);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(74, 6);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(20, 13);
            this.label39.TabIndex = 57;
            this.label39.Text = "dB";
            // 
            // PlusLO1_URP2
            // 
            this.PlusLO1_URP2.BackgroundImage = global::RPU.Properties.Resources.bfa_plus_simple_aqua_48x48;
            this.PlusLO1_URP2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlusLO1_URP2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlusLO1_URP2.ForeColor = System.Drawing.Color.Gainsboro;
            this.PlusLO1_URP2.Location = new System.Drawing.Point(92, 1);
            this.PlusLO1_URP2.Name = "PlusLO1_URP2";
            this.PlusLO1_URP2.Size = new System.Drawing.Size(29, 26);
            this.PlusLO1_URP2.TabIndex = 59;
            this.PlusLO1_URP2.UseVisualStyleBackColor = true;
            this.PlusLO1_URP2.Click += new System.EventHandler(this.PlusLO1_URP2_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label34.Location = new System.Drawing.Point(340, 303);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(66, 16);
            this.label34.TabIndex = 52;
            this.label34.Text = "Low LO1";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(336, 184);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(75, 16);
            this.label25.TabIndex = 51;
            this.label25.Text = "Hight LO1";
            // 
            // LOHeter1
            // 
            this.LOHeter1.Location = new System.Drawing.Point(340, 201);
            this.LOHeter1.Name = "LOHeter1";
            this.LOHeter1.Size = new System.Drawing.Size(64, 96);
            this.LOHeter1.SwitchStyle = NationalInstruments.UI.SwitchStyle.VerticalToggle3D;
            this.LOHeter1.TabIndex = 50;
            this.LOHeter1.StateChanged += new NationalInstruments.UI.ActionEventHandler(this.LOHeter1_StateChanged);
            // 
            // panelATTLo1
            // 
            this.panelATTLo1.BackColor = System.Drawing.Color.Gainsboro;
            this.panelATTLo1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelATTLo1.Controls.Add(this.panel18);
            this.panelATTLo1.Controls.Add(this.textBox6);
            this.panelATTLo1.Controls.Add(this.label24);
            this.panelATTLo1.Location = new System.Drawing.Point(780, 276);
            this.panelATTLo1.Name = "panelATTLo1";
            this.panelATTLo1.Size = new System.Drawing.Size(123, 29);
            this.panelATTLo1.TabIndex = 49;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Gainsboro;
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel18.Controls.Add(this.MinusLO1);
            this.panel18.Controls.Add(this.LO1_ATT);
            this.panel18.Controls.Add(this.PlusLO1);
            this.panel18.Controls.Add(this.label19);
            this.panel18.Location = new System.Drawing.Point(-2, -2);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(123, 29);
            this.panel18.TabIndex = 28;
            // 
            // MinusLO1
            // 
            this.MinusLO1.BackgroundImage = global::RPU.Properties.Resources.bfa_minus_simple_aqua_48x48;
            this.MinusLO1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MinusLO1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinusLO1.ForeColor = System.Drawing.Color.Gainsboro;
            this.MinusLO1.Location = new System.Drawing.Point(0, -1);
            this.MinusLO1.Name = "MinusLO1";
            this.MinusLO1.Size = new System.Drawing.Size(26, 28);
            this.MinusLO1.TabIndex = 49;
            this.MinusLO1.UseVisualStyleBackColor = true;
            this.MinusLO1.Click += new System.EventHandler(this.MinusLO1_Click);
            // 
            // LO1_ATT
            // 
            this.LO1_ATT.Location = new System.Drawing.Point(26, 1);
            this.LO1_ATT.Name = "LO1_ATT";
            this.LO1_ATT.Size = new System.Drawing.Size(46, 20);
            this.LO1_ATT.TabIndex = 22;
            this.LO1_ATT.TextChanged += new System.EventHandler(this.LO1_ATT_TextChanged);
            this.LO1_ATT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LO1_ATT_KeyPress);
            this.LO1_ATT.KeyUp += new System.Windows.Forms.KeyEventHandler(this.LO1_ATT_KeyUp);
            // 
            // PlusLO1
            // 
            this.PlusLO1.BackgroundImage = global::RPU.Properties.Resources.bfa_plus_simple_aqua_48x48;
            this.PlusLO1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlusLO1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlusLO1.ForeColor = System.Drawing.Color.Gainsboro;
            this.PlusLO1.Location = new System.Drawing.Point(92, -1);
            this.PlusLO1.Name = "PlusLO1";
            this.PlusLO1.Size = new System.Drawing.Size(29, 26);
            this.PlusLO1.TabIndex = 49;
            this.PlusLO1.UseVisualStyleBackColor = true;
            this.PlusLO1.Click += new System.EventHandler(this.PlusLO1_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(74, 4);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(20, 13);
            this.label19.TabIndex = 23;
            this.label19.Text = "dB";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(29, 3);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(46, 20);
            this.textBox6.TabIndex = 22;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(81, 6);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(20, 13);
            this.label24.TabIndex = 23;
            this.label24.Text = "dB";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Gainsboro;
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel14.Controls.Add(this.label18);
            this.panel14.Location = new System.Drawing.Point(799, 206);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(77, 21);
            this.panel14.TabIndex = 48;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(60, 15);
            this.label18.TabIndex = 26;
            this.label18.Text = "0...31dB";
            // 
            // LO1_PF4
            // 
            this.LO1_PF4.BackColor = System.Drawing.Color.LightGray;
            this.LO1_PF4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LO1_PF4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO1_PF4.Location = new System.Drawing.Point(585, 288);
            this.LO1_PF4.Name = "LO1_PF4";
            this.LO1_PF4.Size = new System.Drawing.Size(157, 31);
            this.LO1_PF4.TabIndex = 15;
            this.LO1_PF4.Text = "3300 - 3900 MHz";
            this.LO1_PF4.UseVisualStyleBackColor = false;
            // 
            // LO1_PF3
            // 
            this.LO1_PF3.BackColor = System.Drawing.Color.LightGray;
            this.LO1_PF3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LO1_PF3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO1_PF3.Location = new System.Drawing.Point(585, 251);
            this.LO1_PF3.Name = "LO1_PF3";
            this.LO1_PF3.Size = new System.Drawing.Size(157, 31);
            this.LO1_PF3.TabIndex = 14;
            this.LO1_PF3.Text = "2700 - 3500 MHz";
            this.LO1_PF3.UseVisualStyleBackColor = false;
            // 
            // LO1_PF2
            // 
            this.LO1_PF2.BackColor = System.Drawing.Color.LightGray;
            this.LO1_PF2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LO1_PF2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO1_PF2.Location = new System.Drawing.Point(587, 212);
            this.LO1_PF2.Name = "LO1_PF2";
            this.LO1_PF2.Size = new System.Drawing.Size(157, 31);
            this.LO1_PF2.TabIndex = 13;
            this.LO1_PF2.Text = "2000 - 2700 MHz";
            this.LO1_PF2.UseVisualStyleBackColor = false;
            // 
            // LO1_PF1
            // 
            this.LO1_PF1.BackColor = System.Drawing.Color.LightGray;
            this.LO1_PF1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LO1_PF1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO1_PF1.Location = new System.Drawing.Point(587, 175);
            this.LO1_PF1.Name = "LO1_PF1";
            this.LO1_PF1.Size = new System.Drawing.Size(157, 31);
            this.LO1_PF1.TabIndex = 12;
            this.LO1_PF1.Text = "950 - 2200 MHz";
            this.LO1_PF1.UseVisualStyleBackColor = false;
            // 
            // Heter2
            // 
            this.Heter2.BackColor = System.Drawing.Color.LightGray;
            this.Heter2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Heter2.BackgroundImage")));
            this.Heter2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Heter2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Heter2.Controls.Add(this.LO2_Peleng);
            this.Heter2.Controls.Add(this.LO2_SignalLevel);
            this.Heter2.Controls.Add(this.label37);
            this.Heter2.Controls.Add(this.label38);
            this.Heter2.Controls.Add(this.LOHeter2);
            this.Heter2.Controls.Add(this.Switch11);
            this.Heter2.Controls.Add(this.Switch10);
            this.Heter2.Controls.Add(this.LO2_PF2);
            this.Heter2.Controls.Add(this.LO2_PF1);
            this.Heter2.Location = new System.Drawing.Point(4, 22);
            this.Heter2.Name = "Heter2";
            this.Heter2.Size = new System.Drawing.Size(1320, 497);
            this.Heter2.TabIndex = 2;
            this.Heter2.Text = "LO2";
            this.Heter2.Click += new System.EventHandler(this.Heter2_Click);
            // 
            // LO2_Peleng
            // 
            this.LO2_Peleng.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LO2_Peleng.Controls.Add(this.LO2_SignalLevel5);
            this.LO2_Peleng.Controls.Add(this.LO2_SignalLevel4);
            this.LO2_Peleng.Controls.Add(this.LO2_SignalLevel3);
            this.LO2_Peleng.Controls.Add(this.LO2_SignalLevel2);
            this.LO2_Peleng.Location = new System.Drawing.Point(884, 262);
            this.LO2_Peleng.Name = "LO2_Peleng";
            this.LO2_Peleng.Size = new System.Drawing.Size(159, 136);
            this.LO2_Peleng.TabIndex = 57;
            this.LO2_Peleng.Visible = false;
            // 
            // LO2_SignalLevel5
            // 
            this.LO2_SignalLevel5.AutoSize = true;
            this.LO2_SignalLevel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO2_SignalLevel5.Location = new System.Drawing.Point(5, 106);
            this.LO2_SignalLevel5.Name = "LO2_SignalLevel5";
            this.LO2_SignalLevel5.Size = new System.Drawing.Size(121, 15);
            this.LO2_SignalLevel5.TabIndex = 60;
            this.LO2_SignalLevel5.Text = "Уровень сигнала";
            // 
            // LO2_SignalLevel4
            // 
            this.LO2_SignalLevel4.AutoSize = true;
            this.LO2_SignalLevel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO2_SignalLevel4.Location = new System.Drawing.Point(5, 73);
            this.LO2_SignalLevel4.Name = "LO2_SignalLevel4";
            this.LO2_SignalLevel4.Size = new System.Drawing.Size(121, 15);
            this.LO2_SignalLevel4.TabIndex = 59;
            this.LO2_SignalLevel4.Text = "Уровень сигнала";
            // 
            // LO2_SignalLevel3
            // 
            this.LO2_SignalLevel3.AutoSize = true;
            this.LO2_SignalLevel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO2_SignalLevel3.Location = new System.Drawing.Point(5, 42);
            this.LO2_SignalLevel3.Name = "LO2_SignalLevel3";
            this.LO2_SignalLevel3.Size = new System.Drawing.Size(121, 15);
            this.LO2_SignalLevel3.TabIndex = 58;
            this.LO2_SignalLevel3.Text = "Уровень сигнала";
            // 
            // LO2_SignalLevel2
            // 
            this.LO2_SignalLevel2.AutoSize = true;
            this.LO2_SignalLevel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO2_SignalLevel2.Location = new System.Drawing.Point(5, 13);
            this.LO2_SignalLevel2.Name = "LO2_SignalLevel2";
            this.LO2_SignalLevel2.Size = new System.Drawing.Size(121, 15);
            this.LO2_SignalLevel2.TabIndex = 57;
            this.LO2_SignalLevel2.Text = "Уровень сигнала";
            // 
            // LO2_SignalLevel
            // 
            this.LO2_SignalLevel.AutoSize = true;
            this.LO2_SignalLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO2_SignalLevel.Location = new System.Drawing.Point(889, 244);
            this.LO2_SignalLevel.Name = "LO2_SignalLevel";
            this.LO2_SignalLevel.Size = new System.Drawing.Size(121, 15);
            this.LO2_SignalLevel.TabIndex = 56;
            this.LO2_SignalLevel.Text = "Уровень сигнала";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(364, 306);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(66, 16);
            this.label37.TabIndex = 55;
            this.label37.Text = "Low LO2";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(360, 187);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(75, 16);
            this.label38.TabIndex = 54;
            this.label38.Text = "Hight LO2";
            // 
            // LOHeter2
            // 
            this.LOHeter2.Location = new System.Drawing.Point(364, 204);
            this.LOHeter2.Name = "LOHeter2";
            this.LOHeter2.Size = new System.Drawing.Size(64, 96);
            this.LOHeter2.SwitchStyle = NationalInstruments.UI.SwitchStyle.VerticalToggle3D;
            this.LOHeter2.TabIndex = 53;
            this.LOHeter2.StateChanged += new NationalInstruments.UI.ActionEventHandler(this.LOHeter2_StateChanged);
            // 
            // Switch11
            // 
            this.Switch11.BackgroundImage = global::RPU.Properties.Resources.ON2;
            this.Switch11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Switch11.Location = new System.Drawing.Point(787, 230);
            this.Switch11.Name = "Switch11";
            this.Switch11.Size = new System.Drawing.Size(75, 29);
            this.Switch11.TabIndex = 16;
            this.Switch11.TabStop = false;
            // 
            // Switch10
            // 
            this.Switch10.BackgroundImage = global::RPU.Properties.Resources.ON1;
            this.Switch10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Switch10.Location = new System.Drawing.Point(550, 226);
            this.Switch10.Name = "Switch10";
            this.Switch10.Size = new System.Drawing.Size(72, 38);
            this.Switch10.TabIndex = 15;
            this.Switch10.TabStop = false;
            // 
            // LO2_PF2
            // 
            this.LO2_PF2.BackColor = System.Drawing.Color.LightGray;
            this.LO2_PF2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LO2_PF2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO2_PF2.Location = new System.Drawing.Point(638, 251);
            this.LO2_PF2.Name = "LO2_PF2";
            this.LO2_PF2.Size = new System.Drawing.Size(132, 41);
            this.LO2_PF2.TabIndex = 14;
            this.LO2_PF2.Text = "1360 - 1640 MHz";
            this.LO2_PF2.UseVisualStyleBackColor = false;
            // 
            // LO2_PF1
            // 
            this.LO2_PF1.BackColor = System.Drawing.Color.LightGray;
            this.LO2_PF1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LO2_PF1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LO2_PF1.Location = new System.Drawing.Point(638, 204);
            this.LO2_PF1.Name = "LO2_PF1";
            this.LO2_PF1.Size = new System.Drawing.Size(132, 41);
            this.LO2_PF1.TabIndex = 13;
            this.LO2_PF1.Text = "470 - 880 MHz";
            this.LO2_PF1.UseVisualStyleBackColor = false;
            // 
            // Calibration
            // 
            this.Calibration.BackColor = System.Drawing.Color.LightGray;
            this.Calibration.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Calibration.Controls.Add(this.LO1_SecondNormEnd);
            this.Calibration.Controls.Add(this.label49);
            this.Calibration.Controls.Add(this.LO1_SecondNormStart);
            this.Calibration.Controls.Add(this.label50);
            this.Calibration.Controls.Add(this.label51);
            this.Calibration.Controls.Add(this.LO1_FirstNormEnd);
            this.Calibration.Controls.Add(this.label48);
            this.Calibration.Controls.Add(this.LO1_FirstNormStart);
            this.Calibration.Controls.Add(this.label47);
            this.Calibration.Controls.Add(this.FirstRangeLo1);
            this.Calibration.Controls.Add(this.panelCohLO1);
            this.Calibration.Controls.Add(this.panelIndLO1);
            this.Calibration.Controls.Add(this.ButCalibCoh);
            this.Calibration.Controls.Add(this.ButCalibInd);
            this.Calibration.Controls.Add(this.label44);
            this.Calibration.Controls.Add(this.label40);
            this.Calibration.Controls.Add(this.DataCoherent);
            this.Calibration.Controls.Add(this.DataIndependet);
            this.Calibration.Location = new System.Drawing.Point(4, 22);
            this.Calibration.Name = "Calibration";
            this.Calibration.Padding = new System.Windows.Forms.Padding(3);
            this.Calibration.Size = new System.Drawing.Size(1320, 497);
            this.Calibration.TabIndex = 3;
            this.Calibration.Text = "Calibration LO1";
            // 
            // LO1_SecondNormEnd
            // 
            this.LO1_SecondNormEnd.Location = new System.Drawing.Point(741, 246);
            this.LO1_SecondNormEnd.Name = "LO1_SecondNormEnd";
            this.LO1_SecondNormEnd.Size = new System.Drawing.Size(58, 20);
            this.LO1_SecondNormEnd.TabIndex = 23;
            this.LO1_SecondNormEnd.ValueChanged += new System.EventHandler(this.ChangedValue);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label49.Location = new System.Drawing.Point(714, 246);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(21, 18);
            this.label49.TabIndex = 22;
            this.label49.Text = "to";
            // 
            // LO1_SecondNormStart
            // 
            this.LO1_SecondNormStart.Location = new System.Drawing.Point(650, 246);
            this.LO1_SecondNormStart.Name = "LO1_SecondNormStart";
            this.LO1_SecondNormStart.Size = new System.Drawing.Size(58, 20);
            this.LO1_SecondNormStart.TabIndex = 21;
            this.LO1_SecondNormStart.ValueChanged += new System.EventHandler(this.ChangedValue);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label50.Location = new System.Drawing.Point(562, 246);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(85, 18);
            this.label50.TabIndex = 20;
            this.label50.Text = "Norm: from";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label51.Location = new System.Drawing.Point(570, 214);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(225, 20);
            this.label51.TabIndex = 19;
            this.label51.Text = "Frequency range: 1030...3010:";
            // 
            // LO1_FirstNormEnd
            // 
            this.LO1_FirstNormEnd.Location = new System.Drawing.Point(741, 148);
            this.LO1_FirstNormEnd.Name = "LO1_FirstNormEnd";
            this.LO1_FirstNormEnd.Size = new System.Drawing.Size(58, 20);
            this.LO1_FirstNormEnd.TabIndex = 18;
            this.LO1_FirstNormEnd.ValueChanged += new System.EventHandler(this.ChangedValue);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label48.Location = new System.Drawing.Point(714, 148);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(21, 18);
            this.label48.TabIndex = 17;
            this.label48.Text = "to";
            // 
            // LO1_FirstNormStart
            // 
            this.LO1_FirstNormStart.Location = new System.Drawing.Point(650, 148);
            this.LO1_FirstNormStart.Name = "LO1_FirstNormStart";
            this.LO1_FirstNormStart.Size = new System.Drawing.Size(58, 20);
            this.LO1_FirstNormStart.TabIndex = 16;
            this.LO1_FirstNormStart.ValueChanged += new System.EventHandler(this.ChangedValue);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label47.Location = new System.Drawing.Point(562, 148);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(85, 18);
            this.label47.TabIndex = 15;
            this.label47.Text = "Norm: from";
            // 
            // FirstRangeLo1
            // 
            this.FirstRangeLo1.AutoSize = true;
            this.FirstRangeLo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FirstRangeLo1.Location = new System.Drawing.Point(581, 120);
            this.FirstRangeLo1.Name = "FirstRangeLo1";
            this.FirstRangeLo1.Size = new System.Drawing.Size(211, 20);
            this.FirstRangeLo1.TabIndex = 14;
            this.FirstRangeLo1.Text = "Frequency range: 40....1030:";
            // 
            // panelCohLO1
            // 
            this.panelCohLO1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelCohLO1.Controls.Add(this.ReadCohLO1);
            this.panelCohLO1.Controls.Add(this.ButDelCoh);
            this.panelCohLO1.Controls.Add(this.ButSavCoh);
            this.panelCohLO1.Location = new System.Drawing.Point(1055, 23);
            this.panelCohLO1.Name = "panelCohLO1";
            this.panelCohLO1.Size = new System.Drawing.Size(245, 29);
            this.panelCohLO1.TabIndex = 13;
            // 
            // ReadCohLO1
            // 
            this.ReadCohLO1.Location = new System.Drawing.Point(3, 2);
            this.ReadCohLO1.Name = "ReadCohLO1";
            this.ReadCohLO1.Size = new System.Drawing.Size(75, 23);
            this.ReadCohLO1.TabIndex = 11;
            this.ReadCohLO1.Text = "Read";
            this.ReadCohLO1.UseVisualStyleBackColor = true;
            this.ReadCohLO1.Click += new System.EventHandler(this.ReadCohLO1_Click);
            // 
            // ButDelCoh
            // 
            this.ButDelCoh.Location = new System.Drawing.Point(82, 2);
            this.ButDelCoh.Name = "ButDelCoh";
            this.ButDelCoh.Size = new System.Drawing.Size(75, 23);
            this.ButDelCoh.TabIndex = 8;
            this.ButDelCoh.Text = "Delete";
            this.ButDelCoh.UseVisualStyleBackColor = true;
            this.ButDelCoh.Click += new System.EventHandler(this.ButDelCoh_Click);
            // 
            // ButSavCoh
            // 
            this.ButSavCoh.Location = new System.Drawing.Point(163, 2);
            this.ButSavCoh.Name = "ButSavCoh";
            this.ButSavCoh.Size = new System.Drawing.Size(75, 23);
            this.ButSavCoh.TabIndex = 9;
            this.ButSavCoh.Text = "Save";
            this.ButSavCoh.UseVisualStyleBackColor = true;
            this.ButSavCoh.Click += new System.EventHandler(this.ButSavCoh_Click);
            // 
            // panelIndLO1
            // 
            this.panelIndLO1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelIndLO1.Controls.Add(this.ReadIndLO1);
            this.panelIndLO1.Controls.Add(this.ButDelInd);
            this.panelIndLO1.Controls.Add(this.ButSavInd);
            this.panelIndLO1.Location = new System.Drawing.Point(316, 23);
            this.panelIndLO1.Name = "panelIndLO1";
            this.panelIndLO1.Size = new System.Drawing.Size(245, 29);
            this.panelIndLO1.TabIndex = 12;
            // 
            // ReadIndLO1
            // 
            this.ReadIndLO1.Location = new System.Drawing.Point(3, 3);
            this.ReadIndLO1.Name = "ReadIndLO1";
            this.ReadIndLO1.Size = new System.Drawing.Size(75, 23);
            this.ReadIndLO1.TabIndex = 10;
            this.ReadIndLO1.Text = "Read";
            this.ReadIndLO1.UseVisualStyleBackColor = true;
            this.ReadIndLO1.Click += new System.EventHandler(this.ReadIndLO1_Click);
            // 
            // ButDelInd
            // 
            this.ButDelInd.Location = new System.Drawing.Point(84, 3);
            this.ButDelInd.Name = "ButDelInd";
            this.ButDelInd.Size = new System.Drawing.Size(75, 23);
            this.ButDelInd.TabIndex = 5;
            this.ButDelInd.Text = "Delete";
            this.ButDelInd.UseVisualStyleBackColor = true;
            this.ButDelInd.Click += new System.EventHandler(this.ButDelInd_Click);
            // 
            // ButSavInd
            // 
            this.ButSavInd.Location = new System.Drawing.Point(159, 3);
            this.ButSavInd.Name = "ButSavInd";
            this.ButSavInd.Size = new System.Drawing.Size(75, 23);
            this.ButSavInd.TabIndex = 6;
            this.ButSavInd.Text = "Save";
            this.ButSavInd.UseVisualStyleBackColor = true;
            this.ButSavInd.Click += new System.EventHandler(this.ButSavInd_Click);
            // 
            // ButCalibCoh
            // 
            this.ButCalibCoh.Location = new System.Drawing.Point(974, 27);
            this.ButCalibCoh.Name = "ButCalibCoh";
            this.ButCalibCoh.Size = new System.Drawing.Size(75, 23);
            this.ButCalibCoh.TabIndex = 7;
            this.ButCalibCoh.Text = "Calibrate";
            this.ButCalibCoh.UseVisualStyleBackColor = true;
            this.ButCalibCoh.Click += new System.EventHandler(this.ButCalibCoh_Click);
            // 
            // ButCalibInd
            // 
            this.ButCalibInd.Location = new System.Drawing.Point(235, 27);
            this.ButCalibInd.Name = "ButCalibInd";
            this.ButCalibInd.Size = new System.Drawing.Size(75, 23);
            this.ButCalibInd.TabIndex = 4;
            this.ButCalibInd.Text = "Calibrate";
            this.ButCalibInd.UseVisualStyleBackColor = true;
            this.ButCalibInd.Click += new System.EventHandler(this.ButCalibInd_Click);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label44.Location = new System.Drawing.Point(800, 23);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(172, 25);
            this.label44.TabIndex = 3;
            this.label44.Text = "Coherent mode";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label40.Location = new System.Drawing.Point(12, 23);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(206, 25);
            this.label40.TabIndex = 2;
            this.label40.Text = "Independent mode";
            // 
            // DataCoherent
            // 
            this.DataCoherent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.DataCoherent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataCoherent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.DataCoherent.Location = new System.Drawing.Point(805, 55);
            this.DataCoherent.Name = "DataCoherent";
            this.DataCoherent.RowHeadersVisible = false;
            this.DataCoherent.Size = new System.Drawing.Size(495, 409);
            this.DataCoherent.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Band number";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 70;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Frequency";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 70;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "URP1";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 70;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "URP2";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 70;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "URP3";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 70;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "URP4";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 70;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "URP5";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 70;
            // 
            // DataIndependet
            // 
            this.DataIndependet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.DataIndependet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataIndependet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnNumber,
            this.ColumnFreq,
            this.ColumnURP1,
            this.ColumnURP2,
            this.ColumnURP3,
            this.ColumnURP4,
            this.ColumnURP5,
            this.ColumnURP6});
            this.DataIndependet.Location = new System.Drawing.Point(17, 55);
            this.DataIndependet.Name = "DataIndependet";
            this.DataIndependet.RowHeadersVisible = false;
            this.DataIndependet.Size = new System.Drawing.Size(544, 409);
            this.DataIndependet.TabIndex = 0;
            // 
            // ColumnNumber
            // 
            this.ColumnNumber.HeaderText = "Band number";
            this.ColumnNumber.Name = "ColumnNumber";
            this.ColumnNumber.Width = 50;
            // 
            // ColumnFreq
            // 
            this.ColumnFreq.HeaderText = "Frequency";
            this.ColumnFreq.Name = "ColumnFreq";
            this.ColumnFreq.Width = 70;
            // 
            // ColumnURP1
            // 
            this.ColumnURP1.HeaderText = "URP1";
            this.ColumnURP1.Name = "ColumnURP1";
            this.ColumnURP1.Width = 70;
            // 
            // ColumnURP2
            // 
            this.ColumnURP2.HeaderText = "URP2";
            this.ColumnURP2.Name = "ColumnURP2";
            this.ColumnURP2.Width = 70;
            // 
            // ColumnURP3
            // 
            this.ColumnURP3.HeaderText = "URP3";
            this.ColumnURP3.Name = "ColumnURP3";
            this.ColumnURP3.Width = 70;
            // 
            // ColumnURP4
            // 
            this.ColumnURP4.HeaderText = "URP4";
            this.ColumnURP4.Name = "ColumnURP4";
            this.ColumnURP4.Width = 70;
            // 
            // ColumnURP5
            // 
            this.ColumnURP5.HeaderText = "URP5";
            this.ColumnURP5.Name = "ColumnURP5";
            this.ColumnURP5.Width = 70;
            // 
            // ColumnURP6
            // 
            this.ColumnURP6.HeaderText = "URP6";
            this.ColumnURP6.Name = "ColumnURP6";
            this.ColumnURP6.Width = 70;
            // 
            // CalibrationLO2
            // 
            this.CalibrationLO2.BackColor = System.Drawing.Color.LightGray;
            this.CalibrationLO2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.CalibrationLO2.Controls.Add(this.panel2);
            this.CalibrationLO2.Controls.Add(this.button1);
            this.CalibrationLO2.Controls.Add(this.button2);
            this.CalibrationLO2.Controls.Add(this.ButCalibCohLO2);
            this.CalibrationLO2.Controls.Add(this.button4);
            this.CalibrationLO2.Controls.Add(this.button5);
            this.CalibrationLO2.Controls.Add(this.ButCalibIndLO2);
            this.CalibrationLO2.Controls.Add(this.label45);
            this.CalibrationLO2.Controls.Add(this.label46);
            this.CalibrationLO2.Controls.Add(this.DataCoherentLO2);
            this.CalibrationLO2.Controls.Add(this.DataIndependetLO2);
            this.CalibrationLO2.Location = new System.Drawing.Point(4, 22);
            this.CalibrationLO2.Name = "CalibrationLO2";
            this.CalibrationLO2.Padding = new System.Windows.Forms.Padding(3);
            this.CalibrationLO2.Size = new System.Drawing.Size(1320, 497);
            this.CalibrationLO2.TabIndex = 5;
            this.CalibrationLO2.Text = "Calibration LO2";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label54);
            this.panel2.Controls.Add(this.LO2_NormEnd);
            this.panel2.Controls.Add(this.label53);
            this.panel2.Controls.Add(this.label52);
            this.panel2.Controls.Add(this.LO2_NormStart);
            this.panel2.Location = new System.Drawing.Point(567, 172);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(234, 100);
            this.panel2.TabIndex = 25;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label54.Location = new System.Drawing.Point(14, 19);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(207, 20);
            this.label54.TabIndex = 20;
            this.label54.Text = "Frequency range: 40....3010";
            // 
            // LO2_NormEnd
            // 
            this.LO2_NormEnd.Location = new System.Drawing.Point(178, 48);
            this.LO2_NormEnd.Name = "LO2_NormEnd";
            this.LO2_NormEnd.Size = new System.Drawing.Size(48, 20);
            this.LO2_NormEnd.TabIndex = 24;
            this.LO2_NormEnd.ValueChanged += new System.EventHandler(this.ChangedValue);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label53.Location = new System.Drawing.Point(9, 47);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(85, 18);
            this.label53.TabIndex = 21;
            this.label53.Text = "Norm: from";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label52.Location = new System.Drawing.Point(151, 48);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(21, 18);
            this.label52.TabIndex = 23;
            this.label52.Text = "to";
            // 
            // LO2_NormStart
            // 
            this.LO2_NormStart.Location = new System.Drawing.Point(97, 47);
            this.LO2_NormStart.Name = "LO2_NormStart";
            this.LO2_NormStart.Size = new System.Drawing.Size(48, 20);
            this.LO2_NormStart.TabIndex = 22;
            this.LO2_NormStart.ValueChanged += new System.EventHandler(this.ChangedValue);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1226, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1145, 29);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ButCalibCohLO2
            // 
            this.ButCalibCohLO2.Location = new System.Drawing.Point(1064, 29);
            this.ButCalibCohLO2.Name = "ButCalibCohLO2";
            this.ButCalibCohLO2.Size = new System.Drawing.Size(75, 23);
            this.ButCalibCohLO2.TabIndex = 17;
            this.ButCalibCohLO2.Text = "Read";
            this.ButCalibCohLO2.UseVisualStyleBackColor = true;
            this.ButCalibCohLO2.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(486, 30);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 16;
            this.button4.Text = "Save";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(405, 30);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 15;
            this.button5.Text = "Delete";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // ButCalibIndLO2
            // 
            this.ButCalibIndLO2.Location = new System.Drawing.Point(324, 30);
            this.ButCalibIndLO2.Name = "ButCalibIndLO2";
            this.ButCalibIndLO2.Size = new System.Drawing.Size(75, 23);
            this.ButCalibIndLO2.TabIndex = 14;
            this.ButCalibIndLO2.Text = "Read";
            this.ButCalibIndLO2.UseVisualStyleBackColor = true;
            this.ButCalibIndLO2.Click += new System.EventHandler(this.button6_Click);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label45.Location = new System.Drawing.Point(802, 26);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(172, 25);
            this.label45.TabIndex = 13;
            this.label45.Text = "Coherent mode";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.Location = new System.Drawing.Point(12, 26);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(206, 25);
            this.label46.TabIndex = 12;
            this.label46.Text = "Independent mode";
            // 
            // DataCoherentLO2
            // 
            this.DataCoherentLO2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.DataCoherentLO2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataCoherentLO2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14});
            this.DataCoherentLO2.Location = new System.Drawing.Point(807, 58);
            this.DataCoherentLO2.Name = "DataCoherentLO2";
            this.DataCoherentLO2.RowHeadersVisible = false;
            this.DataCoherentLO2.Size = new System.Drawing.Size(495, 409);
            this.DataCoherentLO2.TabIndex = 11;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Band number";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 70;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Frequency";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 70;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "URP1";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 70;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "URP2";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 70;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "URP3";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 70;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "URP4";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 70;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "URP5";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 70;
            // 
            // DataIndependetLO2
            // 
            this.DataIndependetLO2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.DataIndependetLO2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataIndependetLO2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22});
            this.DataIndependetLO2.Location = new System.Drawing.Point(17, 58);
            this.DataIndependetLO2.Name = "DataIndependetLO2";
            this.DataIndependetLO2.RowHeadersVisible = false;
            this.DataIndependetLO2.Size = new System.Drawing.Size(544, 409);
            this.DataIndependetLO2.TabIndex = 10;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "Band number";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 50;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "Frequency";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Width = 70;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.HeaderText = "URP1";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.Width = 70;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "URP2";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Width = 70;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "URP3";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.Width = 70;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "URP4";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.Width = 70;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "URP5";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.Width = 70;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "URP6";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.Width = 70;
            // 
            // CommandTab
            // 
            this.CommandTab.BackColor = System.Drawing.Color.LightGray;
            this.CommandTab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.CommandTab.Controls.Add(this.textBox1);
            this.CommandTab.Controls.Add(this.LoadButton);
            this.CommandTab.Controls.Add(this.panel21);
            this.CommandTab.Controls.Add(this.ClearButton);
            this.CommandTab.Location = new System.Drawing.Point(4, 22);
            this.CommandTab.Name = "CommandTab";
            this.CommandTab.Padding = new System.Windows.Forms.Padding(3);
            this.CommandTab.Size = new System.Drawing.Size(1320, 497);
            this.CommandTab.TabIndex = 4;
            this.CommandTab.Text = "Command";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox1.BackColor = System.Drawing.Color.NavajoWhite;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(709, 43);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(486, 414);
            this.textBox1.TabIndex = 5;
            this.textBox1.Text = "";
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(93, 15);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(75, 23);
            this.LoadButton.TabIndex = 4;
            this.LoadButton.Text = "Load file";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // panel21
            // 
            this.panel21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel21.AutoScroll = true;
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel21.Location = new System.Drawing.Point(93, 44);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(529, 413);
            this.panel21.TabIndex = 2;
            // 
            // ClearButton
            // 
            this.ClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ClearButton.BackColor = System.Drawing.Color.Gainsboro;
            this.ClearButton.Location = new System.Drawing.Point(858, 463);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(196, 23);
            this.ClearButton.TabIndex = 1;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = false;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // timerThermometer
            // 
            this.timerThermometer.Interval = 5000;
            this.timerThermometer.Tick += new System.EventHandler(this.timerThermometer_Tick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.ComTab);
            this.tabControl1.Controls.Add(this.SpiTab);
            this.tabControl1.Location = new System.Drawing.Point(3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(204, 133);
            this.tabControl1.TabIndex = 75;
            // 
            // ComTab
            // 
            this.ComTab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ComTab.Controls.Add(this.ComPanel);
            this.ComTab.Location = new System.Drawing.Point(4, 22);
            this.ComTab.Name = "ComTab";
            this.ComTab.Padding = new System.Windows.Forms.Padding(3);
            this.ComTab.Size = new System.Drawing.Size(196, 107);
            this.ComTab.TabIndex = 0;
            this.ComTab.Text = "ComConnection";
            this.ComTab.UseVisualStyleBackColor = true;
            // 
            // SpiTab
            // 
            this.SpiTab.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SpiTab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SpiTab.Controls.Add(this.SPIPanel);
            this.SpiTab.Location = new System.Drawing.Point(4, 22);
            this.SpiTab.Name = "SpiTab";
            this.SpiTab.Padding = new System.Windows.Forms.Padding(3);
            this.SpiTab.Size = new System.Drawing.Size(196, 107);
            this.SpiTab.TabIndex = 1;
            this.SpiTab.Text = "SPIConnection";
            // 
            // SPIPanel
            // 
            this.SPIPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SPIPanel.Controls.Add(this.OpenSpi);
            this.SPIPanel.Location = new System.Drawing.Point(1, 1);
            this.SPIPanel.Name = "SPIPanel";
            this.SPIPanel.Size = new System.Drawing.Size(190, 101);
            this.SPIPanel.TabIndex = 75;
            // 
            // OpenSpi
            // 
            this.OpenSpi.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenSpi.Location = new System.Drawing.Point(44, 41);
            this.OpenSpi.Name = "OpenSpi";
            this.OpenSpi.Size = new System.Drawing.Size(103, 23);
            this.OpenSpi.TabIndex = 5;
            this.OpenSpi.Text = "Open";
            this.OpenSpi.UseVisualStyleBackColor = true;
            this.OpenSpi.Click += new System.EventHandler(this.OpenSpi_Click);
            // 
            // SaveSettings
            // 
            this.SaveSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SaveSettings.BackColor = System.Drawing.Color.LightGray;
            this.SaveSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SaveSettings.ForeColor = System.Drawing.Color.Blue;
            this.SaveSettings.Location = new System.Drawing.Point(648, 40);
            this.SaveSettings.Name = "SaveSettings";
            this.SaveSettings.Size = new System.Drawing.Size(187, 31);
            this.SaveSettings.TabIndex = 75;
            this.SaveSettings.Text = "SAVE";
            this.SaveSettings.UseVisualStyleBackColor = false;
            this.SaveSettings.Visible = false;
            this.SaveSettings.Click += new System.EventHandler(this.SaveSettings_Click);
            // 
            // FormRPU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1549, 541);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.SchemeControl);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(1565, 580);
            this.Name = "FormRPU";
            this.Text = "RPU";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ComPanel.ResumeLayout(false);
            this.ComPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.panel19.ResumeLayout(false);
            this.SchemeControl.ResumeLayout(false);
            this.Scheme.ResumeLayout(false);
            this.Scheme.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.PCH_Peleng.ResumeLayout(false);
            this.PCH_Peleng.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Thermometer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Filter10)).EndInit();
            this.panelUNN.ResumeLayout(false);
            this.panelUNN.PerformLayout();
            this.panelATT1.ResumeLayout(false);
            this.panelATT1.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LedATT4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedATT3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedATT2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LedATT1)).EndInit();
            this.panelATT2.ResumeLayout(false);
            this.panelATT2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Switch9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch1)).EndInit();
            this.Heter1.ResumeLayout(false);
            this.Heter1.PerformLayout();
            this.LO1_Peleng.ResumeLayout(false);
            this.LO1_Peleng.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LOHeter1)).EndInit();
            this.panelATTLo1.ResumeLayout(false);
            this.panelATTLo1.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.Heter2.ResumeLayout(false);
            this.Heter2.PerformLayout();
            this.LO2_Peleng.ResumeLayout(false);
            this.LO2_Peleng.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LOHeter2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Switch10)).EndInit();
            this.Calibration.ResumeLayout(false);
            this.Calibration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LO1_SecondNormEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LO1_SecondNormStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LO1_FirstNormEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LO1_FirstNormStart)).EndInit();
            this.panelCohLO1.ResumeLayout(false);
            this.panelIndLO1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataCoherent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataIndependet)).EndInit();
            this.CalibrationLO2.ResumeLayout(false);
            this.CalibrationLO2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LO2_NormEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LO2_NormStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataCoherentLO2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataIndependetLO2)).EndInit();
            this.CommandTab.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ComTab.ResumeLayout(false);
            this.SpiTab.ResumeLayout(false);
            this.SPIPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ComPanel;
        private System.Windows.Forms.ComboBox ComPortName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SpeedPortBox;
        private System.Windows.Forms.Button OpenPort;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl SchemeControl;
        private System.Windows.Forms.TabPage Heter1;
        private System.Windows.Forms.TabPage Heter2;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox LO1_ATT;
        private System.Windows.Forms.Button LO1_PF4;
        private System.Windows.Forms.Button LO1_PF3;
        private System.Windows.Forms.Button LO1_PF2;
        private System.Windows.Forms.Button LO1_PF1;
        private System.Windows.Forms.Button LO2_PF2;
        private System.Windows.Forms.Button LO2_PF1;
        private System.Windows.Forms.TextBox BandNumber;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panelATTLo1;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button MinusLO1;
        private System.Windows.Forms.Button PlusLO1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button PlusBand;
        private System.Windows.Forms.Button MinusBand;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label FreqLabel;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.PictureBox Switch10;
        private System.Windows.Forms.PictureBox Switch11;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label25;
        private NationalInstruments.UI.WindowsForms.Switch LOHeter1;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private NationalInstruments.UI.WindowsForms.Switch LOHeter2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Request;
        private System.Windows.Forms.DataGridViewTextBoxColumn Answer;
        private System.Windows.Forms.CheckedListBox NumberURP;
        private System.Windows.Forms.Label LO1_SignalLevel;
        private System.Windows.Forms.Panel LO1_Peleng;
        private System.Windows.Forms.Label LO1_SignalLevel5;
        private System.Windows.Forms.Label LO1_SignalLevel4;
        private System.Windows.Forms.Label LO1_SignalLevel3;
        private System.Windows.Forms.Label LO1_SignalLevel2;
        private System.Windows.Forms.Button MinusLO1_URP5;
        private System.Windows.Forms.TextBox LO1_URP5;
        private System.Windows.Forms.Button PlusLO1_URP5;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button MinusLO1_URP4;
        private System.Windows.Forms.TextBox LO1_URP4;
        private System.Windows.Forms.Button PlusLO1_URP4;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button MinusLO1_URP3;
        private System.Windows.Forms.TextBox LO1_URP3;
        private System.Windows.Forms.Button PlusLO1_URP3;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button MinusLO1_URP2;
        private System.Windows.Forms.TextBox LO1_URP2;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button PlusLO1_URP2;
        private System.Windows.Forms.Panel LO2_Peleng;
        private System.Windows.Forms.Label LO2_SignalLevel5;
        private System.Windows.Forms.Label LO2_SignalLevel4;
        private System.Windows.Forms.Label LO2_SignalLevel3;
        private System.Windows.Forms.Label LO2_SignalLevel2;
        private System.Windows.Forms.Label LO2_SignalLevel;
        private System.Windows.Forms.TabPage Calibration;
        private System.Windows.Forms.Button ButSavInd;
        private System.Windows.Forms.Button ButDelInd;
        private System.Windows.Forms.Button ButCalibInd;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.DataGridView DataCoherent;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridView DataIndependet;
        private System.Windows.Forms.Button ButSavCoh;
        private System.Windows.Forms.Button ButDelCoh;
        private System.Windows.Forms.Button ButCalibCoh;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFreq;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnURP1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnURP2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnURP3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnURP4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnURP5;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnURP6;
        private System.Windows.Forms.SaveFileDialog saveIndependentMode;
        private System.Windows.Forms.SaveFileDialog saveCoherentMode;
        private System.Windows.Forms.Timer timerThermometer;
        private System.Windows.Forms.TabPage CommandTab;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.RichTextBox textBox1;
        private System.Windows.Forms.CheckBox Temperatur;
        private System.Windows.Forms.TabPage CalibrationLO2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button ButCalibCohLO2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button ButCalibIndLO2;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.DataGridView DataCoherentLO2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridView DataIndependetLO2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.Button ReadCohLO1;
        private System.Windows.Forms.Button ReadIndLO1;
        private System.Windows.Forms.TabPage Scheme;
        private System.Windows.Forms.Panel PCH_Peleng;
        private System.Windows.Forms.Label Level_PCH1;
        private NationalInstruments.UI.WindowsForms.Thermometer Thermometer;
        private NationalInstruments.UI.WindowsForms.Led Filter8;
        private NationalInstruments.UI.WindowsForms.Led Filter7;
        private NationalInstruments.UI.WindowsForms.Led Filter6;
        private NationalInstruments.UI.WindowsForms.Led Filter5;
        private NationalInstruments.UI.WindowsForms.Led Filter4;
        private NationalInstruments.UI.WindowsForms.Led Filter3;
        private NationalInstruments.UI.WindowsForms.Led Filter2;
        private NationalInstruments.UI.WindowsForms.Led Filter1;
        private NationalInstruments.UI.WindowsForms.Led Filter9;
        private System.Windows.Forms.Label label36;
        private NationalInstruments.UI.WindowsForms.Led Filter10;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panelUNN;
        private System.Windows.Forms.Button MinusUFF;
        private System.Windows.Forms.TextBox UNNBox;
        private System.Windows.Forms.Button PlusUUF;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panelATT1;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button MinusATT1;
        private System.Windows.Forms.Button PlusATT1;
        private System.Windows.Forms.TextBox ATTBox1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label5;
        private NationalInstruments.UI.WindowsForms.Led LedATT4;
        private NationalInstruments.UI.WindowsForms.Led LedATT3;
        private NationalInstruments.UI.WindowsForms.Led LedATT2;
        private NationalInstruments.UI.WindowsForms.Led LedATT1;
        private System.Windows.Forms.Button ATT4;
        private System.Windows.Forms.Button ATT3;
        private System.Windows.Forms.Button ATT2;
        private System.Windows.Forms.Button ATT1;
        private System.Windows.Forms.Panel panelATT2;
        private System.Windows.Forms.Button MinusATT2;
        private System.Windows.Forms.Button PlusATT2;
        private System.Windows.Forms.TextBox ATTBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox Switch9;
        private System.Windows.Forms.PictureBox Switch7;
        private System.Windows.Forms.PictureBox Switch8;
        private System.Windows.Forms.PictureBox Switch6;
        private System.Windows.Forms.PictureBox Switch5;
        private System.Windows.Forms.PictureBox Switch4;
        private System.Windows.Forms.PictureBox Switch3;
        private System.Windows.Forms.PictureBox Switch2;
        private System.Windows.Forms.PictureBox Switch1;
        private System.Windows.Forms.Button LO2;
        private System.Windows.Forms.Button LO1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape26;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape25;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape24;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape52;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape51;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape10;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape25;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape24;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape9;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape23;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape22;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape8;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape21;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape20;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape7;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape19;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape18;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape6;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape17;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape16;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape5;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape15;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape14;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape13;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape12;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape11;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape10;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape9;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape8;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape7;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape3;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private System.Windows.Forms.Label Level_PCH5;
        private System.Windows.Forms.Label Level_PCH4;
        private System.Windows.Forms.Label Level_PCH3;
        private System.Windows.Forms.Label Level_PCH2;
        private System.Windows.Forms.Panel panel20;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape56;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape55;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape54;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape53;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape50;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape49;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape17;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape16;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape15;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape14;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape13;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape12;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape48;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape47;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape46;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape45;
        private Microsoft.VisualBasic.PowerPacks.OvalShape ovalShape1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape44;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape43;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape42;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape41;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape40;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape39;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape38;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape37;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape36;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape35;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape34;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape33;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape32;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape31;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape30;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape29;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape28;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape27;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape26;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape11;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Panel panelCohLO1;
        private System.Windows.Forms.Panel panelIndLO1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage ComTab;
        private System.Windows.Forms.TabPage SpiTab;
        private System.Windows.Forms.Button OpenSpi;
        private System.Windows.Forms.Panel SPIPanel;
        private System.Windows.Forms.NumericUpDown LO1_SecondNormEnd;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.NumericUpDown LO1_SecondNormStart;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.NumericUpDown LO1_FirstNormEnd;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.NumericUpDown LO1_FirstNormStart;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label FirstRangeLo1;
        private System.Windows.Forms.NumericUpDown LO2_NormEnd;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.NumericUpDown LO2_NormStart;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button SaveSettings;
    }
}

